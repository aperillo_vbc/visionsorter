#!/bin/bash
set -e

# sudo apt-get install qtmultimedia5-dev libqt5multimedia5-plugins libpulse-dev

if [ "$1" == "clean" ]
then
    make clean -C ./controller
    make clean
    rm Makefile
    rm VisionSorter
    exit 0
fi

if [ ! -f /etc/ld.so.conf.d/99local.conf ]
then
    echo "updating ld path with /etc/local/lib for shraed libraries"
    sudo echo "/usr/local/lib" >> /etc/ld.so.conf.d/99local.conf
    sudo cp ./controller/camera/lib/libtoupcam.so /usr/local/lib
    sudo ldconfig
fi

if grep --quiet OPENNI2_INCLUDE ~/.profile; then
    echo "OPENNI2 Include paths are already loaded"
else
    echo "export OPENNI2_INCLUDE=`pwd`/OpenNI2/Include" >> ~/.profile
fi

if grep --quiet OPENNI2_REDIST ~/.profile; then
    echo "OPENNI2 Redist paths are already loaded"
else
    echo "export OPENNI2_REDIST=`pwd`" >> ~/.profile
fi

if grep --quiet "export.*PYTHONPATH.*\." ~/.profile; then
    echo "Python path already configured."
else
    echo "Configuring Python path"
    echo 'export PYTHONPATH="$PYTHONPATH:."' >> ~/.profile
fi

if [ ! -f classify.py ]
then
    echo "Linking the classifier."
    ln -s controller/classifier/classify.py .
else
    echo "The classifer is already linked."
fi

echo "building controller"
make -C ./controller

echo "qmake on ui"
if [ "$1" == "release" ]
then
    qmake CONFIG+=release ui/VisionSorter.pro
else
    qmake CONFIG+=debug ui/VisionSorter.pro
fi

echo "building ui"
make

exit 0
