#ifndef ASTRACAM_H
#define ASTRACAM_H

#include <openni/OpenNI.h>
#include <memory>
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>

class AstraCam;

typedef std::shared_ptr<AstraCam> AstraCamPtr;

class AstraCam {
public:
    AstraCam();
    ~AstraCam();
    void getColorFrame(openni::VideoFrameRef *frame);
    const void* getColorFrameData();
    void getDepthFrame(openni::VideoFrameRef *frame);
    const void* getDepthFrameData();
    void init();
    inline bool deviceIsValid(){return device.isValid();}
    inline bool colorStreamIsValid(){return color.isValid();}
    inline bool depthStreamIsValid(){return device.isValid();}
    cv::Mat getDepthMat8();
protected:
    openni::Device device;
    openni::VideoStream depth, color;
};

#endif
