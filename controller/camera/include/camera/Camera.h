#ifndef T_CAMERA_H
#define T_CAMERA_H

#include "daq/DAQclass.h"
#include <pylon/PylonIncludes.h>
#include <iomanip>
#include <cstdio>
#include <unistd.h>
#include <string>
#include <limits>
#include <vector>
#include <chrono>
#include <ctime>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <opencv2/opencv.hpp>

// Namespace for using pylon objects.
using namespace Pylon;

// Settings for using Basler USB cameras.
#include <pylon/usb/BaslerUsbInstantCamera.h>
using namespace Basler_UsbCameraParams;

#ifndef __BITMAPINFOHEADER_DEFINED__
#define __BITMAPINFOHEADER_DEFINED__
typedef struct {
    unsigned        biSize;
    int             biWidth;
    int             biHeight;
    unsigned short  biPlanes;
    unsigned short  biBitCount;
    unsigned        biCompression;
    unsigned        biSizeImage;
    int             biXPelsPerMeter;
    int             biYPelsPerMeter;
    unsigned        biClrUsed;
    unsigned        biClrImportant;
} BITMAPINFOHEADER;
#endif

namespace baslerCam {

    class Camera;

    typedef std::shared_ptr<Camera> CameraPtr;

    typedef unsigned int uint;
    typedef unsigned short ushort;

    typedef struct{
        unsigned int status;
        void * data;
    } Ctx;

    typedef int LONG;
    typedef unsigned char BYTE;
    typedef unsigned int DWORD;
    typedef unsigned short WORD;

    struct CameraSettings {
        CameraSettings();
        void print(std::ostream &out) const;
        int tint;
        int colorTemp;
        int hue;
        uint exposureTime; 
        ushort gain;
        uint eSize;
        bool vFlip;
        bool hFlip;
        bool hwTrigger;
	int triggerLine;
    };

    typedef struct BITMAPFILEHEADER
    {
        BITMAPFILEHEADER(const BITMAPINFOHEADER &pHeader);
        /* BITMAPFILEHEADER(); */
        void print(std::ostream &out) const;
        WORD    bfType; // 2  /* Magic identifier */
        DWORD   bfSize; // 4  /* File size in bytes */
        WORD    bfReserved1; // 2
        WORD    bfReserved2; // 2
        DWORD   bfOffBits; // 4 /* Offset to image data, bytes */
    } __attribute__((packed)) BITMAPFILEHEADER;

    typedef struct BITMAPINFOHEADER_PACKED
    {
        BITMAPINFOHEADER_PACKED(const BITMAPINFOHEADER &pHeader);
        /* BITMAPINFOHEADER_PACKED(); */
        void print(std::ostream &out) const;
        DWORD    biSize; // 4 /* Header size in bytes */
        LONG     biWidth; // 4 /* Width of image */
        LONG     biHeight; // 4 /* Height of image */
        WORD     biPlanes; // 2 /* Number of colour planes */
        WORD     biBitCount; // 2 /* Bits per pixel */
        DWORD    biCompress; // 4 /* Compression type */
        DWORD    biSizeImage; // 4 /* Image size in bytes */
        LONG     biXPelsPerMeter; // 4
        LONG     biYPelsPerMeter; // 4 /* Pixels per meter */
        DWORD    biClrUsed; // 4 /* Number of colours */
        DWORD    biClrImportant; // 4 /* Important colours */
    } __attribute__((packed)) BITMAPINFOHEADER_PACKED;

    class Camera{
    public:
        Camera(Pylon::CBaslerUsbInstantCamera *cam, const CameraSettings & cameraSettings, std::string id, DAQPtr daq);
        ~Camera();

        void reset(const CameraSettings &cameraSettings);

        inline const CameraSettings &getCameraSettings(){return camSets_;}
        inline std::string getID(){
            return id_;
        }

        inline void setHFlip(int flip){
            camSets_.hFlip = flip;
	    cam_->ReverseX.SetValue(camSets_.hFlip);
        }
        inline void setVFlip(int flip){
            camSets_.vFlip = flip;
	    cam_->ReverseY.SetValue(camSets_.vFlip);
        }

        int startGrab();
        int stopGrab();
        void triggerImagePrefix(const std::string &filePrefix, std::chrono::system_clock::time_point &triggerTime);
        void triggerImage(const std::string &fileName, std::chrono::system_clock::time_point &triggerTime);
        void triggerImage(const std::string &fileName);
        cv::Mat trigger_block_bgr();
        cv::Mat trigger_block_rgb();
        void trigger_block_gpu_rgb(cv::cuda::GpuMat &d_img);
        void trigger_block_gpu_bgr(cv::cuda::GpuMat &d_img);
        int saveImageBmp(const std::string &fileName, const void* pData, const BITMAPINFOHEADER* pHeader);
        static void printHeader(std::ostream &out, const BITMAPINFOHEADER &pHeader);
        static std::string attachTimeStamp(std::string &filePrefix, const std::chrono::system_clock::time_point &triggerTime);
        static std::string timeStamp(std::string &filePrefix, std::chrono::system_clock::time_point &triggerTime);
        static std::pair<double, double> findOrigin(cv::Mat &img, double size, cv::Point2f &originDist, cv::Point2f &originPix);

        void rotateAndSwapRB(const BITMAPINFOHEADER_PACKED &bm_info, unsigned char *oData);
        inline void setExposureTime(unsigned int expoTime){
            camSets_.exposureTime = expoTime;
	    cam_->ExposureTime.SetValue(camSets_.exposureTime);
        }
        inline void setGain(unsigned short gain){
            double gain_mindb, gain_maxdb, gain_reqdb, gain_reqsetdb;
		gain_mindb = cam_->Gain.GetMin();
		gain_maxdb = cam_->Gain.GetMax();
		gain_reqdb = (gain-100)*(gain_maxdb/200);

	    	camSets_.gain = (gain_reqdb < gain_mindb) ? 100 : gain;
	    	camSets_.gain = (gain_reqdb > gain_maxdb) ? 300 : gain;
		gain_reqsetdb = (camSets_.gain-100)*(gain_maxdb/200);
		cam_->Gain.SetValue(gain_reqsetdb);
        }

        inline void setColorTemp(int temp, int tint){
//            Toupcam_put_TempTint(cam_, temp, tint);
            camSets_.tint = tint;
            camSets_.colorTemp = temp;
        }

        inline void setHue(int hue){
//            Toupcam_put_Hue(cam_, hue);
            camSets_.hue = hue;
        }

        CameraPtr cameraFactory();
        inline std::chrono::milliseconds getTotalTime(){
            return std::chrono::duration_cast<std::chrono::milliseconds>(return_time - trigger_time);
        }
        inline std::chrono::milliseconds getCallBackTime(){
            return std::chrono::duration_cast<std::chrono::milliseconds>(callback_time - trigger_time);
        }
        inline std::chrono::system_clock::time_point getTriggerTime(){return trigger_time;}
        inline std::string getTriggerTimeString(const std::string prefix = "data/"){
            std::stringstream timeStr;
            timeStr << prefix << id_ << "_";
            auto triggerTime = getTriggerTime();
            auto now_c = std::chrono::system_clock::to_time_t(triggerTime);
            timeStr << std::put_time(std::localtime(&now_c), "%H-%M-%S-");
            timeStr << std::setfill('0') << std::setw(3);
            auto fraction = triggerTime - std::chrono::time_point_cast<std::chrono::seconds>(triggerTime);
            timeStr << std::chrono::duration_cast<std::chrono::milliseconds>(fraction).count();
            timeStr << std::put_time(std::localtime(&now_c), "_%m-%d-%y");
            timeStr << ".bmp";
            return timeStr.str();
        }
        
	// For acA1300-200uc 
	inline cv::Size getImageSize() const{
            if(camSets_.eSize == 0)
                return cv::Size(1922, 2560);
            else if(camSets_.eSize == 1)
                return cv::Size(1024, 1280);
            else
                return cv::Size(480, 640);
        }

        // For 18 MP
	/*inline cv::Size getImageSize() const{
            if(camSets_.eSize == 0)
                return cv::Size(3684, 4912);
            else if(camSets_.eSize == 1)
                return cv::Size(1842, 2456);
            else
                return cv::Size(922, 1228);
        }*/

        void setRectify(std::vector<float> intrins, std::vector<float> distCoeffs);
        void setRectify(cv::Mat intrins, cv::Mat distCoeffs, cv::Size imgSize);
    protected:
        int openCamera();
        std::string id_;
        int reset();

        CameraSettings camSets_;
	CBaslerUsbInstantCamera *cam_;
        /* BITMAPFILEHEADER bmp_head; */
        /* BITMAPINFOHEADER_PACKED  bmp_info; */
        Ctx pCtx_;
        bool currentOpenCamera;
        int imageCounter_;
        std::string fileName_;

        bool capture_complete;
        std::mutex captureLock;
        std::condition_variable capture_cv;

        const BITMAPINFOHEADER* oHeader_;
        void *oData_;

        bool rectify_;
        cv::Mat intrins_, distCoeffs_;
        cv::Mat map1_, map2_;

        std::chrono::system_clock::time_point callback_time;
        std::chrono::system_clock::time_point trigger_time;
        std::chrono::system_clock::time_point return_time;
        
	DAQPtr daq_;

        // cv::Mat currentImage;
    };
}

#endif
