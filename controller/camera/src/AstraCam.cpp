#ifndef VISION_ASTRA_CAM_H
#define VISION_ASTRA_CAM_H

#include "camera/AstraCam.h"
#include <iostream>

#define DEPTH_SCALE 255./65535.

void AstraCam::init()
{
    openni::Status status = openni::STATUS_OK;
    const char* deviceURI = openni::ANY_DEVICE;
    // junk
    status = openni::OpenNI::initialize();
    status = device.open(deviceURI);
    if(status != openni::STATUS_OK)
    {
        openni::OpenNI::shutdown();
        std::string msg;
        if(status == openni::STATUS_NO_DEVICE)
            msg = std::string("[AstraCam] No camera was found.");
        else
            msg = std::string("[AstraCam] The camera failed to load.");
        throw std::runtime_error(msg);
    }


    status = depth.create(device, openni::SENSOR_DEPTH);
    if (status == openni::STATUS_OK)
    {
        status = depth.start();
        depth.setMirroringEnabled(false);
        if (status != openni::STATUS_OK)
        {
            throw std::runtime_error(std::string("[AstraCam] Couldn't start depth stream: ") + openni::OpenNI::getExtendedError());
            depth.destroy();
        }
    }
    else
    {
        throw std::runtime_error(std::string("[AstraCam] Couldn't find depth stream: ") + openni::OpenNI::getExtendedError());
    }

    status = color.create(device, openni::SENSOR_COLOR);
    if (status == openni::STATUS_OK)
    {
        status = color.start();
        color.setMirroringEnabled(false);
        if (status != openni::STATUS_OK)
        {
            throw std::runtime_error(std::string("[AstraCam] Couldn't start color stream: ") + openni::OpenNI::getExtendedError());
            color.destroy();
        }
    }
    else
    {
        throw std::runtime_error(std::string("[AstraCam] Couldn't find color stream: ") + openni::OpenNI::getExtendedError());
    }

    if (!depth.isValid() || !color.isValid())
    {
        openni::OpenNI::shutdown();
        throw std::runtime_error("[AstraCam] No valid streams.");
    }

    device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
}

AstraCam::AstraCam(){}

AstraCam::~AstraCam()
{
    depth.stop();
    color.stop();
    depth.destroy();
    color.destroy();
    device.close();
    openni::OpenNI::shutdown();
}

void AstraCam::getColorFrame(openni::VideoFrameRef *frame)
{
    color.readFrame(frame);
}

const void* AstraCam::getColorFrameData()
{
    if(!color.isValid())
    {
        return nullptr;
    }
    openni::VideoFrameRef frame;
    openni::Status status = color.readFrame(&frame);
    if(status != openni::STATUS_OK)
        return nullptr;
    else
        return frame.getData();
}

void AstraCam::getDepthFrame(openni::VideoFrameRef *frame)
{
    depth.readFrame(frame);
}

cv::Mat AstraCam::getDepthMat8()
{
    cv::Mat img;
    openni::VideoFrameRef frame;
    depth.readFrame(&frame);
    void *buffer = const_cast<void *>(frame.getData());
    img.create(frame.getHeight(), frame.getWidth(), CV_16UC1);
    memcpy(img.data, buffer, frame.getDataSize());//*sizeof(uint16_t));
    // cv::Mat imgRet;
    // double min, max;
    // minMaxIdx(img, &min, &max);
    // cv::convertScaleAbs(img, imgRet, 255./max);
    img.convertTo(img, CV_8UC1);
    // cv::bitwise_not(img, img);
    return img;
}

const void* AstraCam::getDepthFrameData()
{
    if(!depth.isValid())
    {
        return nullptr;
    }
    openni::VideoFrameRef frame;
    openni::Status status = depth.readFrame(&frame);
    if(status != openni::STATUS_OK)
        return nullptr;
    else
        return frame.getData();
}

#endif
