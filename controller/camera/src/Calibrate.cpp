#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <exception>
#include <string>

bool runCalibrationAndSave(cv::Size patSize, cv::Mat &camMat, cv::Mat &distCoeffs,
                           std::vector<std::vector<Point2f>> &imagePoints);

bool runCalibrationAndSave(cv::Size patSize, cv::Mat &camMat, cv::Mat &distCoeffs,
                           std::vector<std::vector<Point2f>> &imagePoints);

bool runCalibration(cv::Size& patSize, cv::Mat& camMat, cv::Mat& distCoeffs,
                    std::vector<std::vector<Point2f> > imagePoints,
                    std::vector<Mat>& rvecs, std::vector<cv::Mat>& tvecs,
                    std::vector<float>& reprojErrs, double& totalAvgErr);

double computeReprojErrors(const std::vector<std::vector<Point3f>>& objectPoints,
                           const std::vector<std::vector<Point2f>>& imagePoints,
                           const std::vector<Mat>&rvecs,
                           const std::vector<Mat>&tvecs,
                           const cv::Mat& camMat, const cv::Mat& distCoeffs,
                           std::vector<float>& perViewErrors);

void showSmall(const std::string &str, const cv::Mat &mat)
{
    cv::Mat small;
    cv::resize(mat, small, Size(), 0.4, 0.4);
    cv::imshow(str, small);
}

void writeParams(const std::string &camFile, cv::Mat& camMat, cv::Mat& distCoeffs, int width, int height, double error)
{
    cv::FileStorage in(camFile, cv::FileStorage::WRITE);
    fs << "cam_intrinsic" << camMat;
    fs << "cam_distCoeffs" << distCoeffs;
    fs << "cam_width" << width;
    fs << "cam_height" << height;
    fs << "avg_reprojection_error" << error;
}

void calibrate(FileStorage &fs, const std::string &camName, size_t numImages)
{
    Size patSize(BOARD_WIDTH, BOARD_HEIGHT);
    std::vector<std::vector<Point2f>> imagePoints;
    Mat camMat, distCoeffs;
    for(size_t i = 0; i < numImages; ++i)
    {
        std::vector<Point2f> corners;
        Mat color = imread(camName + std::string("_")
                                  + std::to_string(i+1)
                                  + ".jpg", IMREAD_COLOR);
        // Mat c_temp = color.clone();
        // color = c_temp(Rect(200, 0, 2100, 1842));
        Mat bw = imread(camName + std::string("_")
                                 + std::to_string(i+1)
                                 + ".jpg", IMREAD_GRAYSCALE);
        // Mat bw_temp = bw.clone();
        // bw = bw_temp(Rect(200, 0, 2100, 1842));
        // showSmall(std::string("CROP_") + std::to_string(i), color);

        bool foundCorners = findChessboardCorners(bw, patSize, corners,
                              CALIB_CB_ADAPTIVE_THRESH
                              + CALIB_CB_NORMALIZE_IMAGE);

        if(foundCorners)
        {
            cornerSubPix(bw, corners, Size(11, 11), Size(-1, -1),
                         TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
            imagePoints.push_back(corners);
            // drawChessboardCorners(color, patSize, Mat(corners), foundCorners);
            // showSmall(std::to_string(i), color);
            runCalibrationAndSave(patSize, camMat, distCoeffs, imagePoints);
        }
    }

    writeParams(fs, camName, camMat, distCoeffs);
}

int main()
{
    FileStorage fs("camera_params", FileStorage::WRITE);
    calibrate(fs, "R", 5);
    calibrate(fs, "F", 5);
    fs.release();

    FileStorage in("camera_params", FileStorage::READ);
    // for(size_t i = 0; i < 2; ++i)
    // {
    //                         + std::to_string(i+1)
    //                         + ".jpg", IMREAD_COLOR);
    //     Mat temp = color.clone();
    //     // Mat temp = color(Rect(200, 0, 2100, 1842));
        Mat intrinsic, distCoeffs;
        in["F"]["cam_intrinsic"] >> intrinsic;
        in["F"]["cam_distCoeffs"] >> distCoeffs;
        Mat color = imread("F_2.jpg");
        Mat temp = color.clone();
        undistort(temp, color, intrinsic, distCoeffs);
        showSmall(std::string("F_2") + "_undistored", color);
        imwrite("undistored_F.jpg",color);
    // }
    // Mat intrinsic, distCoeffs;
    // in["R"]["cam_intrinsic"] >> intrinsic;
    // in["R"]["cam_distCoeffs"] >> distCoeffs;
    // Mat color = imread("R_2.jpg");
    // Mat temp = color.clone();
    // undistort(temp, color, intrinsic, distCoeffs);
    // showSmall(std::string("R_2") + "_undistored", color);
    // imwrite("undistored_R.jpg",color);



    // for(size_t i = 0; i < 5; ++i)
    // {
    //     Mat r_color = imread(std::string("RN_")
    //                          + std::to_string(i+1)
    //                          + ".jpg", IMREAD_COLOR);
    //     Mat r_temp = r_color.clone();
    //     undistort(r_temp, r_color, camMat, distCoeffs);
    //     showSmall(std::string("distored_") + std::to_string(i), r_temp);
    //     showSmall(std::string("undistored_") + std::to_string(i), r_color);
    // }
    // std::cout << "Valid boards found: " << imagePoints.size() << std::endl;
    std::cout << "done" << std::endl;
    waitKey(0);
    return 0;
}

bool runCalibrationAndSave(Size patSize, Mat &camMat, Mat &distCoeffs,
                           std::vector<std::vector<Point2f>> &imagePoints)
{
    std::vector<Mat> rvecs, tvecs;
    std::vector<float> reprojErrs;
    double totalAvgErr = 0;
    return runCalibration(patSize, camMat, distCoeffs,
                          imagePoints,
                          rvecs, tvecs, reprojErrs, totalAvgErr);
}

bool runCalibration(Size& patSize, Mat& camMat, Mat& distCoeffs,
                    std::vector<std::vector<Point2f> > imagePoints,
                    std::vector<Mat>& rvecs, std::vector<Mat>& tvecs,
                    std::vector<float>& reprojErrs, double& totalAvgErr)
{
    camMat = Mat::eye(3, 3, CV_64F);
    camMat.at<double>(0,0) = 1;
    camMat.at<double>(1,1) = 1;
    distCoeffs = Mat::zeros(8, 1, CV_64F);
    std::vector<std::vector<Point3f>> objectPoints;
    std::vector<Point3f> corners;
    for(int y = 0; y < BOARD_HEIGHT; ++y)
        for(int x = 0; x < BOARD_WIDTH; ++x)
            corners.push_back(Point3f(BOARD_SIZE_IN * x,
                                      BOARD_SIZE_IN * y, 0));
    objectPoints.push_back(corners);
    objectPoints.resize(imagePoints.size(), objectPoints[0]);
    double rms = calibrateCamera(objectPoints, imagePoints, patSize,
                                 camMat, distCoeffs, rvecs, tvecs);//,
                                 // CV_CALIB_FIX_K4|CV_CALIB_FIX_K5 );
    std::cout << "RMS: " << rms << std::endl;
    totalAvgErr = computeReprojErrors(objectPoints, imagePoints,
                                      rvecs, tvecs, camMat, distCoeffs,
                                      reprojErrs);
    return checkRange(camMat) && checkRange(distCoeffs);
}

double computeReprojErrors(const std::vector<std::vector<Point3f>>& objectPoints,
                           const std::vector<std::vector<Point2f>>& imagePoints,
                           const std::vector<Mat>&rvecs,
                           const std::vector<Mat>&tvecs,
                           const Mat& camMat, const Mat& distCoeffs,
                           std::vector<float>& perViewErrors)
{
    std::vector<Point2f> imagePoints_temp;
    size_t totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for(size_t i = 0; i < objectPoints.size(); ++i )
    {
        projectPoints(objectPoints[i], rvecs[i], tvecs[i], camMat, distCoeffs, imagePoints_temp);
        err = norm(imagePoints[i], imagePoints_temp, NORM_L2);

        size_t n = objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err*err/n);
        totalErr        += err*err;
        totalPoints     += n;
    }

    return std::sqrt(totalErr/totalPoints);
}
