#include "camera/Camera.h"
#include <iostream>
#include <memory>
#include <cstring>
#include <sstream>
#include <opencv2/aruco.hpp>
#include <thread>

#if CV_MAJOR_VERSION >= 3
#include <opencv2/cudaimgproc.hpp>
#endif

baslerCam::BITMAPFILEHEADER::BITMAPFILEHEADER(const BITMAPINFOHEADER &pHeader) :
    bfType(0x4d42),
		bfReserved1(0),
    bfReserved2(0)
{
    bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER_PACKED) + pHeader.biSizeImage;
		bfOffBits = bfSize - pHeader.biSizeImage;
}

baslerCam::BITMAPINFOHEADER_PACKED::BITMAPINFOHEADER_PACKED(const BITMAPINFOHEADER &pHeader)
{
    biSize = pHeader.biSize;
    biWidth = pHeader.biWidth;
    biHeight = pHeader.biHeight;
    biPlanes = pHeader.biPlanes;
    biBitCount = pHeader.biBitCount;
    biCompress = pHeader.biCompression;
    biSizeImage = pHeader.biSizeImage;
    biXPelsPerMeter = pHeader.biXPelsPerMeter;
    biYPelsPerMeter = pHeader.biYPelsPerMeter;
    biClrUsed = pHeader.biClrUsed;
    biClrImportant = pHeader.biClrImportant;
}

baslerCam::CameraSettings::CameraSettings() :
    tint(800),
    colorTemp(6503),
    hue(0),
    exposureTime(1.4 * 1e3),
    gain(100),
    eSize(1),
    hFlip(false),
    vFlip(false),
    hwTrigger(false),
    triggerLine(0)
{
}

void baslerCam::CameraSettings::print(std::ostream &out) const
{
    out << "tint: " << tint << std::endl;
    out << "color temperature: " << colorTemp << std::endl;
    out << "hue: " << hue << std::endl;
    out << "exposure time: " << exposureTime << std::endl;
    out << "gain: " << gain << std::endl;
    out << "eSize: " << eSize << std::endl;
}

baslerCam::Camera::Camera(Pylon::CBaslerUsbInstantCamera *cam, const CameraSettings & cameraSettings, std::string id, DAQPtr daq) :
    cam_(cam),
    id_(id),
    daq_(daq),
    currentOpenCamera(false),
    imageCounter_(0)
{
    rectify_ = false;
    capture_complete = false;
    reset(cameraSettings);
    if(cameraSettings.hwTrigger)
	daq_->setSolenoid(camSets_.triggerLine, true);
}


void baslerCam::Camera::setRectify(std::vector<float> intrins, std::vector<float> distCoeffs)
{
    cv::Mat matIntr = cv::Mat(3, 3, CV_32FC1, intrins.data()).clone();
    cv::Mat matDist = cv::Mat(distCoeffs, true);
    setRectify(matIntr, matDist, getImageSize());
}

void baslerCam::Camera::setRectify(cv::Mat intrins, cv::Mat distCoeffs, cv::Size imgSize)
{
    intrins_ = intrins;
    distCoeffs_ = distCoeffs;
    cv::initUndistortRectifyMap(
        intrins, distCoeffs, cv::Mat(),
        cv::getOptimalNewCameraMatrix(intrins, distCoeffs,
                                      imgSize, 1, imgSize, 0),
        imgSize, CV_32FC1, map1_, map2_);
    rectify_ = true;
}

baslerCam::Camera::~Camera()
{
    if(currentOpenCamera)
    {
	try{
	   if(cam_->IsOpen())
    	   {
        	cam_->Close();
        	std::cout << "**************closed cam***********" << std::endl;
	   }
	   else
        	std::cout << "camera has been closed for unknown reason" << std::endl;
    	}
	catch (const Pylon::GenericException & e)
    	{
    		std::cerr << "Failed close cam. Reason: "
  		<< e.GetDescription() << std::endl;
    	}
    }
}

int baslerCam::Camera::openCamera()
{
    try{
    	if(currentOpenCamera)
    	{
	   if(cam_->IsOpen())
    	   {
          	cam_->Close();
          	std::cout << "**************closed cam on reset***********" << std::endl;
	   }
    	}

    // open cam with default configuration
        std::cout << "opening cam" << std::endl;
        cam_->Open();
    }
    catch(const Pylon::GenericException &e){
        std::cout << "Failed to open cam" << std::endl;
        std::cout << e.what() << std::endl;
        return -1;
    } 
    currentOpenCamera = true;
    return 0;
}

void baslerCam::Camera::reset(const CameraSettings &cameraSettings)
{
   try{
   	// Register the standard configuration event handler for enabling software triggering.
        // The software trigger configuration handler replaces the default configuration
        // as all currently registered configuration handlers are removed by setting the registration mode to RegistrationMode_ReplaceAll.
        cam_->RegisterConfiguration( new Pylon::CSoftwareTriggerConfiguration, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_Delete);
   	camSets_ = cameraSettings;
    	openCamera();
    	reset();
   }
   catch(const Pylon::GenericException &e){
        std::cout << "Failed to configure" << std::endl;
        std::cout << e.what() << std::endl;
        cam_->Close();
    	currentOpenCamera = false;
   }

}

int baslerCam::Camera::reset()
{
   try{
	std::cout << "Loading features file" << std::endl;
	const char Filename[] = "cameraFeatures.pfs";
	//Pylon::CFeaturePersistence::Load(Filename, &(cam_->GetNodeMap()), true);
        // tensorflow expects RGB format
        GenApi::CEnumerationPtr(cam_->GetNodeMap().GetNode("PixelFormat"))->FromString("RGB8");
        // Reconfigure the camera to use continuous acquisition.
//        Pylon::CAcquireContinuousConfiguration().OnOpened(*cam_);
	// Set the exposure time to  μs
	cam_->ExposureMode.SetValue( ExposureMode_Timed );
	cam_->ExposureTime.SetValue(camSets_.exposureTime);
	cam_->GainSelector.SetValue(GainSelector_All);

        double gain_mindb, gain_maxdb, gain_reqdb, gain_reqsetdb;
	gain_mindb = cam_->Gain.GetMin();
	gain_maxdb = cam_->Gain.GetMax();
	gain_reqdb = (camSets_.gain-100)*(gain_maxdb/200);

    	camSets_.gain = (gain_reqdb < gain_mindb) ? 100 :camSets_.gain;
    	camSets_.gain = (gain_reqdb > gain_maxdb) ? 300 : camSets_.gain;
	gain_reqsetdb = (camSets_.gain-100)*(gain_maxdb/200);
	cam_->Gain.SetValue(gain_reqsetdb);

	// reverse X
	cam_->ReverseX.SetValue(camSets_.hFlip);
	// reverse Y
	cam_->ReverseY.SetValue(camSets_.vFlip);
   }
   catch(const Pylon::GenericException &e){
        std::cout << "Failed to load camera settings" << std::endl;
        std::cout << e.what() << std::endl;
        cam_->Close();
    	currentOpenCamera = false;
   	return -1;
   }

    return 0;
}

void baslerCam::Camera::rotateAndSwapRB(const BITMAPINFOHEADER_PACKED &bm_info, unsigned char *oData)
{
    unsigned char temp;

    // Swap R and B to go from either RGB to BGR or BGR to RGB
    for(int i = 0; i < bm_info.biSizeImage; i+=3)
    {
        temp = oData[i];
        oData[i] = oData[i+2];
        oData[i+2] = temp;
    }
}

int baslerCam::Camera::saveImageBmp(const std::string &fileName, const void* pData, const BITMAPINFOHEADER* pHeader){
    FILE *fp = fopen(fileName.c_str(), "wb");
    if (fp) {
        BITMAPFILEHEADER bmp_head(*pHeader);
        BITMAPINFOHEADER_PACKED bmp_info(*pHeader);

        fwrite(&bmp_head.bfType, 1, sizeof(WORD), fp);
        fwrite(&bmp_head.bfSize, 1, sizeof(DWORD), fp);
        fwrite(&bmp_head.bfReserved1, 1, sizeof(WORD), fp);
        fwrite(&bmp_head.bfReserved1, 1, sizeof(WORD), fp);
        fwrite(&bmp_head.bfOffBits, 1, sizeof(DWORD), fp);

        fwrite(&bmp_info.biSize, 1, sizeof(DWORD), fp);
        fwrite(&bmp_info.biWidth, 1, sizeof(LONG), fp);
        fwrite(&bmp_info.biHeight, 1, sizeof(LONG), fp);
        fwrite(&bmp_info.biPlanes, 1, sizeof(WORD), fp);
        fwrite(&bmp_info.biBitCount, 1, sizeof(WORD), fp);
        fwrite(&bmp_info.biCompress, 1, sizeof(DWORD), fp);
        fwrite(&bmp_info.biSizeImage, 1, sizeof(DWORD), fp);
        fwrite(&bmp_info.biXPelsPerMeter, 1, sizeof(LONG), fp);
        fwrite(&bmp_info.biYPelsPerMeter, 1, sizeof(LONG), fp);
        fwrite(&bmp_info.biClrUsed, 1, sizeof(LONG), fp);
        fwrite(&bmp_info.biClrImportant, 1, sizeof(LONG), fp);

        unsigned char* oData;
        if(oData == nullptr)
            oData = (unsigned char *)malloc(bmp_info.biSizeImage);
        std::memcpy(oData, pData, bmp_info.biSizeImage);
        rotateAndSwapRB(bmp_info, oData);
        fwrite(oData, 1, bmp_info.biSizeImage, fp);
        free(oData);
        fclose(fp);
        return 1;
    }
    return 0;
}

void baslerCam::Camera::printHeader(std::ostream &out, const BITMAPINFOHEADER &pHeader)
{
    out << "----- Header info ------- " << std::endl;
    out << "size: " << pHeader.biSize << std::endl;
    out << "width: " << pHeader.biWidth << std::endl;
    out << "height: " << pHeader.biHeight << std::endl;
    out << "planes: " << pHeader.biPlanes << std::endl;
    out << "bit count: " << pHeader.biBitCount << std::endl;
    out << "compression: " << pHeader.biCompression << std::endl;
    out << "image size: " << pHeader.biSizeImage << std::endl;
    out << "meters per pixel, x: " << pHeader.biXPelsPerMeter << std::endl;
    out << "meters per pixel, y: " << pHeader.biYPelsPerMeter << std::endl;
    out << "colors used: " << pHeader.biClrUsed << std::endl;
    out << "important colors: " << pHeader.biClrImportant << std::endl;
    out << "-------------------------" << std::endl;
}

void baslerCam::BITMAPFILEHEADER::print(std::ostream &out) const
{
    out << "----- Header file ------- " << std::endl;
    out << "type: " << bfType << std::endl;
    out << "size: " << bfSize << std::endl;
    out << "reserved 1: " << bfReserved1 << std::endl;
    out << "reserved 2: " << bfReserved2 << std::endl;
    out << "OffBits: " << bfOffBits << std::endl;
    out << "------------------------- " << std::endl;
}


int baslerCam::Camera::startGrab()
{
    try{
        cam_->StartGrabbing(Pylon::GrabStrategy_OneByOne);
    }
    catch (const Pylon::GenericException & e)
    {
    	std::cerr << "Failed startGrab. Reason: "
  	<< e.GetDescription() << std::endl;
    }

    return 0;
}


int baslerCam::Camera::stopGrab()
{
    try{
	cam_->StopGrabbing();
    }
    catch (const Pylon::GenericException & e)
    {
    	std::cerr << "Failed stopGrab. Reason: "
  	<< e.GetDescription() << std::endl;
    }
    return 0;
}

void baslerCam::Camera::triggerImagePrefix(const std::string &filePrefix, std::chrono::system_clock::time_point &triggerTime)
{
    std::stringstream timeStr;
    timeStr << filePrefix + "_";
    triggerTime = std::chrono::system_clock::now();
    trigger_time = triggerTime;
    auto now_c = std::chrono::system_clock::to_time_t(triggerTime);
    timeStr << std::put_time(std::localtime(&now_c), "%H:%M:%S:");
    timeStr << std::setfill('0') << std::setw(3);
    auto fraction = triggerTime - std::chrono::time_point_cast<std::chrono::seconds>(triggerTime);
    timeStr << std::chrono::duration_cast<std::chrono::milliseconds>(fraction).count();
    timeStr << std::put_time(std::localtime(&now_c), "_%m-%d-%y");
    timeStr << ".bmp";

    fileName_ = timeStr.str();; // this could cause a race condition

//    Toupcam_Trigger(cam_, 1);
}

void baslerCam::Camera::triggerImage(const std::string &fileName, std::chrono::system_clock::time_point &triggerTime)
{
    fileName_ = fileName;
    triggerTime = std::chrono::system_clock::now();
//    Toupcam_Trigger(cam_, 1);
}

void baslerCam::Camera::triggerImage(const std::string &fileName)
{
    fileName_ = fileName;
//    Toupcam_Trigger(cam_, 1);
}


cv::Mat baslerCam::Camera::trigger_block_rgb()
{
    // This smart pointer will receive the grab result data.
    Pylon::CGrabResultPtr image;
    try{
    	if ( cam_->WaitForFrameTriggerReady( 1000, Pylon::TimeoutHandling_ThrowException))
    	{
	  trigger_time = std::chrono::system_clock::now();
    	  if(camSets_.hwTrigger){
	  	daq_->setSolenoid(camSets_.triggerLine, false);
        	std::this_thread::sleep_for(std::chrono::microseconds(10));
	  	daq_->setSolenoid(camSets_.triggerLine, true);
	  }	
          else{
          	cam_->ExecuteSoftwareTrigger();
	  }
    	}

    	// Queues a buffer for grabbing and waits for the grab to finish.
    	cam_->RetrieveResult( 2000, image, Pylon::TimeoutHandling_ThrowException);
    }
    catch (const Pylon::GenericException & e)
    {
    	std::cerr << "Failed trigger/retrieve. Reason: "
  	<< e.GetDescription() << std::endl;
        throw e;
    }

    return_time = std::chrono::system_clock::now();


    if(image->GrabSucceeded()){
    	cv::Mat img(image->GetHeight(), image->GetWidth(), CV_8UC3, image->GetBuffer());

//    cv::Mat img(oHeader_->biHeight, oHeader_->biWidth, CV_8UC3, oData_);
    	if(rectify_)
           cv::remap(img, img, map1_, map2_, cv::INTER_CUBIC);

    	cv::Mat img1 = img.clone();
    	return img1;
    }else{
    	return cv::Mat(); 
    }
}

cv::Mat baslerCam::Camera::trigger_block_bgr()
{
    cv::Mat img = trigger_block_rgb();
    
#if CV_MAJOR_VERSION != 4 && CV_MAJOR_VERSION != 3

        cv::cvtColor(img, img, CV_RGB2BGR);
        if(rectify_)
        cv::remap(img, img, map1_, map2_, cv::INTER_CUBIC);
#else
        cv::cvtColor(img, img, cv::COLOR_RGB2BGR);
        if(rectify_)
        cv::remap(img, img, map1_, map2_, cv::INTER_CUBIC);
#endif

    return img;
}

void baslerCam::Camera::trigger_block_gpu_rgb(cv::cuda::GpuMat &d_img)
{
    cv::Mat h_img = trigger_block_rgb();
    cv::cuda::setDevice(0);
    cv::cuda::GpuMat d_img2;
    d_img2.upload(h_img);
}

void baslerCam::Camera::trigger_block_gpu_bgr(cv::cuda::GpuMat &d_img)
{
    trigger_block_gpu_rgb(d_img);
    
#if CV_MAJOR_VERSION != 4 && CV_MAJOR_VERSION != 3

     cv::cuda::cvtColor(d_img, d_img, CV_RGB2BGR); 
#else
     cv::cuda::cvtColor(d_img, d_img, cv::COLOR_RGB2BGR);
#endif    
}

std::string baslerCam::Camera::timeStamp(std::string &filePrefix, std::chrono::system_clock::time_point &triggerTime)
{
    std::stringstream timeStr;
    timeStr << filePrefix + "_";
    triggerTime = std::chrono::system_clock::now();
    auto now_c = std::chrono::system_clock::to_time_t(triggerTime);
    timeStr << std::put_time(std::localtime(&now_c), "%H-%M-%S-");
    timeStr << std::setfill('0') << std::setw(3);
    auto fraction = triggerTime - std::chrono::time_point_cast<std::chrono::seconds>(triggerTime);
    timeStr << std::chrono::duration_cast<std::chrono::milliseconds>(fraction).count();
    timeStr << std::put_time(std::localtime(&now_c), "_%m-%d-%y");
    timeStr << ".bmp";
    return timeStr.str();
}

std::string baslerCam::Camera::attachTimeStamp(std::string &filePrefix, const std::chrono::system_clock::time_point &triggerTime)
{
    std::stringstream timeStr;
    timeStr << filePrefix + "_";
    auto now_c = std::chrono::system_clock::to_time_t(triggerTime);
    timeStr << std::put_time(std::localtime(&now_c), "%H-%M-%S-");
    timeStr << std::setfill('0') << std::setw(3);
    auto fraction = triggerTime - std::chrono::time_point_cast<std::chrono::seconds>(triggerTime);
    timeStr << std::chrono::duration_cast<std::chrono::milliseconds>(fraction).count();
    timeStr << std::put_time(std::localtime(&now_c), "_%m-%d-%y");
    timeStr << ".bmp";
    return timeStr.str();
}

std::pair<double, double> baslerCam::Camera::findOrigin(cv::Mat &img, double size, cv::Point2f &originDist, cv::Point2f &originPix)
{
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    std::vector<int> ids;
    std::vector<std::vector<cv::Point2f> > markers;
    cv::aruco::detectMarkers(img, dictionary, markers, ids);
    // cv::imwrite("position.jpg", img);
    std::cout << "id size is "<<ids.size() << std::endl;
    std::cout << "size is "<<size << std::endl;
    if(ids.size() > 1)
        throw std::runtime_error("[FindOrigin] Too many fiducial markers, please only have the fiducial in view.");
    if(ids.size() == 0)
        throw std::runtime_error("[FindOrigin] No fiducial marker could be found. Did you put the marker under the camera and are the lights on?");

    cv::aruco::drawDetectedMarkers(img, markers, ids);
    originPix = cv::Point2f(0, std::numeric_limits<double>::max());
    auto &marker = markers[0];
    // top right
    for(auto &pt : marker)
        if(pt.x > originPix.x && pt.y < originPix.y)
            originPix = pt;
    cv::circle(img,cv::Point(originPix.x,originPix.y),10.0,cv::Scalar(0,255,0),5,8); //visualizing the origin on fiducial - Isha
    cv::circle(img,cv::Point(0,0),10.0,cv::Scalar(255,0,255),5,8); //visualizing image origin (pink circle quater) - Isha
    double height = 0, width = 0;
    for(auto &pt : marker)
    {
        if(std::abs(originPix.x - pt.x) > width)
            width = std::abs(originPix.x - pt.x);
        if(std::abs(originPix.y - pt.y) > height)
            height = std::abs(originPix.y - pt.y);
    }
    double inPerPixX = size / width;
    double inPerPixY = size / height;
    originDist = originPix;
    originDist.x *= inPerPixX;
    originDist.y *= inPerPixY;

    return std::make_pair(inPerPixX, inPerPixY);
}
