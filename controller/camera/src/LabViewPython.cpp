// uhvPython.cpp : Defines the exported functions for the DLL application.

#include "stdafx.h"
#include <stdio.h>
#include <Windows.h>
#include <wchar.h>
#include "string"
#include "Python.h"
#include "ndarrayobject.h"
#include "frameobject.h"
#include <mutex>
#include <pylon/PylonIncludes.h>
#include <queue>

static Pylon::CInstantCamera *cam1 = nullptr;
static Pylon::CInstantCamera *cam2 = nullptr;
static std::mutex gilLock;

// keep the smartpointers for image around for use in snap and sort
static std::queue<Pylon::CGrabResultPtr> imagePtrs1;
static std::queue<Pylon::CGrabResultPtr> imagePtrs2;
static std::mutex queueLock1;
static std::mutex queueLock2;


extern "C" {

// globals
	static PyObject *pFunc1 = NULL;
	static PyObject *pFunc2 = NULL;

	static char save_error_type[2048], save_error_info[2048], save_error_trace[2048];
	static wchar_t temp[8192];
	static char tracebuff[8192];
	static wchar_t msgBuffer[8192];

	void PyErrorHandler(void);

__declspec(dllexport) int init()
	{
		PyObject *sys=NULL;
		PyObject *path=NULL;
		PyObject *pModule=NULL;
		PyObject *pName = NULL;
		char * func1 = "colorsort";
		char * func2 = "sort";
		const char *scriptDirectoryName = "c:\\UHVSorter";
//		wchar_t newPath[] = L"'.';C:\\Program Files\\Python35\\python35.zip;C:\\Program Files\\Python35\\DLLs;C:\\Program Files\\Python35\\lib;C:\\Program Files\\Python35;C:\\Program Files\\Python35\\lib\\site-packages;C:\\Program Files\\Python35\\lib\\site-packages\\Sphinx-1.4.6-py3.5.egg;C:\\Program Files\\Python35\\lib\\site-packages\\win32;C:\\Program Files\\Python35\\lib\\site-packages\\win32\\lib;C:\\Program Files\\Python35\\lib\\site-packages\\Pythonwin;C:\\Program Files\\Python35\\lib\\site-packages\\setuptools-27.2.0-py3.5.egg;C:\\Program Files\\Python35\\Lib\\site-packages\\pypylon-0.0.1-py3.5-win-amd64.egg";

		wchar_t newPath[] = L"'.';C:\\Program Files\\Python35\\python35.zip;C:\\Program Files\\Python35\\DLLs;C:\\Program Files\\Python35\\lib;C:\\Program Files\\Python35;C:\\Program Files\\Python35\\lib\\site-packages;C:\\Program Files\\Python35\\Lib\\site-packages\\keras-1.2.0-py3.5.egg;C:\\Program Files\\Python35\\Lib\\site-packages\\pypylon-0.0.1-py3.5-win-amd64.egg";
		wchar_t * searchPath = Py_GetPath();

		Py_SetPath(newPath);

		Py_Initialize();
		import_array();
		Pylon::PylonInitialize();

		sys = PyImport_ImportModule("sys");
		path = PyObject_GetAttrString(sys, "path");
		int err = PyList_Append(path, PyUnicode_FromString(scriptDirectoryName));

		wchar_t * checkPath = Py_GetPath();

		pName = PyUnicode_FromString("sort");
		
		pModule = PyImport_Import(pName);

		Py_DECREF(path);
		Py_DECREF(pName);

		if (PyErr_Occurred())
		{
			err = -2;
			PyErrorHandler();
			goto error;
		}
		
		if (pModule != NULL)
		{
			/* pFunc1 and pFunc2 is used outside the init code */
			pFunc1 = PyObject_GetAttrString(pModule, func1);
			if(pFunc1 == NULL)
			{
				PyErrorHandler();
				err = -3;
			}
			
			pFunc2 = PyObject_GetAttrString(pModule, func2);
			if (pFunc2 == NULL)
			{
				PyErrorHandler();
				err = -4;
			}

			Py_DECREF(pModule);
		}
		else {
			PyErrorHandler();
			err = -1;
		}
	error:
		return err;
	}

__declspec(dllexport) int uninit()
{
	PyObject *pValue = NULL;
	PyObject *pArgs = NULL;


	Py_XDECREF(pFunc1);
	Py_XDECREF(pFunc2);

	Py_Finalize();
  if(cam1 != nullptr && cam1->IsOpen())
  {
      cam1->Close();
      delete cam1;
  }
  if(cam2 != nullptr && cam2->IsOpen())
  {
      cam2->Close();
      delete cam2;
  }
  Pylon::PylonTerminate();
  return 0;
}

__declspec(dllexport) int snap(int lane)
{
	auto cam = cam1;
	if (lane == 1) {
		cam = cam1;
	}
	else {
		cam = cam2;
	}

	Pylon::CGrabResultPtr image;
	try {
		cam->GrabOne(40, image);
	}
	catch (const TimeoutException &e)
	{
		//        std::cout << "failed to grab image" << std::endl;
		//        std::cout << e.what() << std::endl;
		return -3;
	}
	if (lane == 1) {
		queueLock1.lock();
		imagePtrs1.push(image);
		queueLock1.unlock();
	}
	else {
		queueLock2.lock();
		imagePtrs2.push(image);
		queueLock2.unlock();
	}

	return 0;
}

__declspec(dllexport) int sort(char *path, double *prob0, double *prob1, int lane, uint8_t *imgbuffer)
{
//    auto cam = cam1;
//    if(lane == 1){
//        cam = cam1;
//    }
//    else{
//        cam = cam2;
//    }

//    Pylon::CGrabResultPtr image;
//    try{
//        cam->GrabOne(40, image);
//    }
//    catch(const TimeoutException &e)
//    {
//        std::cout << "failed to grab image" << std::endl;
//        std::cout << e.what() << std::endl;
//       return -3;
//    }
	
    PyObject *pValue = NULL;
    PyObject *pArgs = NULL;
    int result = 0;
	Pylon::CGrabResultPtr image;
	if (lane == 1) {
		queueLock1.lock();
		image = imagePtrs1.front();
	}
	else {
		queueLock2.lock();
		image = imagePtrs2.front();
	}

	memcpy(imgbuffer, image->GetBuffer(), 640*480*3);   // for brass sorter
//	memcpy(imgbuffer, image->GetBuffer(), 1280 * 1024 * 3); // for Zorba Ft. Wayne

    if (pFunc2 && PyCallable_Check(pFunc2))
    {
		gilLock.lock();
		npy_intp dims[3];
        // dims[0] = 1;
        dims[0] = image->GetHeight();
        dims[1] = image->GetWidth();
        dims[2] = 3;
        PyObject *numpy_array = PyArray_SimpleNewFromData(
            3,
            dims,
            NPY_UBYTE,
            (uint8_t *)(image->GetBuffer()));

		if (lane == 1) {
			imagePtrs1.pop();
			queueLock1.unlock();
		}
		else {
			imagePtrs2.pop();
			queueLock2.unlock();
		}

	
        pArgs = PyTuple_New(1);
        PyTuple_SetItem(pArgs, 0, numpy_array);

        pValue = PyObject_CallObject(pFunc2, pArgs);

		
        Py_DECREF(pArgs);
        if (pValue != NULL)
        {
            if (PyList_Size(pValue) >= 1)
            {
                PyObject * PyValue;
                PyObject * ascii_mystring;

                PyValue = PyList_GetItem(pValue, 0);
                result = PyLong_AsLong(PyValue);

                PyValue = PyList_GetItem(pValue, 1);
                ascii_mystring = PyUnicode_AsASCIIString(PyValue);
                strcpy(path, PyBytes_AsString(ascii_mystring));
                Py_DECREF(ascii_mystring);

                PyValue = PyList_GetItem(pValue, 2);
                if (PyFloat_Check(PyValue))
                *prob0 = PyFloat_AsDouble(PyValue);

                PyValue = PyList_GetItem(pValue, 3);
                if (PyFloat_Check(PyValue))
                *prob1 = PyFloat_AsDouble(PyValue);
            }
            Py_DECREF(pValue);
			gilLock.unlock();
			return result;
        }
        else {
            if(PyErr_Occurred())
                PyErrorHandler();
			gilLock.unlock();
			return -1;
        }
    }
    else
    {
        return -2;
    }
}

__declspec(dllexport) int colorsort(int *prediction, int lane, uint8_t *imgbuffer)
{
	auto cam = cam1;
	if (lane == 1) {
		cam = cam1;
	}
	else {
		cam = cam2;
	}

	Pylon::CGrabResultPtr image;
	try {
		cam->GrabOne(40, image);
	}
	catch (const TimeoutException &e)
	{
		//        std::cout << "failed to grab image" << std::endl;
		//        std::cout << e.what() << std::endl;
		return -3;
	}

	memcpy(imgbuffer, image->GetBuffer(), 640 * 480 * 3);

	PyObject *pValue = NULL;
	PyObject *pArgs = NULL;
	int result = 0;

	if (pFunc1 && PyCallable_Check(pFunc1))
	{
		npy_intp dims[3];
		// dims[0] = 1;
		dims[0] = image->GetHeight();
		dims[1] = image->GetWidth();
		dims[2] = 3;
		PyObject *numpy_array = PyArray_SimpleNewFromData(
			3,
			dims,
			NPY_UBYTE,
			(uint8_t *)image->GetBuffer());

		pArgs = PyTuple_New(1);
		PyTuple_SetItem(pArgs, 0, numpy_array);

		gilLock.lock();
		pValue = PyObject_CallObject(pFunc1, pArgs);
		gilLock.unlock();

		Py_DECREF(pArgs);
		if (pValue != NULL)
		{
			if (PyList_Size(pValue) >= 1)
			{
				PyObject * PyValue;

				PyValue = PyList_GetItem(pValue, 0);
				*prediction = PyLong_AsLong(PyValue);
			}
			Py_DECREF(pValue);
			return result;
		}
		else {
			if (PyErr_Occurred())
				PyErrorHandler();
			return -1;
		}
	}
	else
	{
		return -2;
	}
}

__declspec(dllexport) int create_cam(int serial, int laneNumber)
{
	const char Filename[] = "C:\\UHVSorter\\cameraFeatures.pfs";

	Pylon::CTlFactory& TlFactory = Pylon::CTlFactory::GetInstance();
    Pylon::DeviceInfoList_t lstDevices;
    TlFactory.EnumerateDevices( lstDevices );
    Pylon::IPylonDevice* device = nullptr;
    bool camFound = false;
    // find the available cameras
    if ( ! lstDevices.empty() ) {
        Pylon::DeviceInfoList_t::const_iterator it;
        for ( it = lstDevices.begin(); it != lstDevices.end(); ++it )
        {
            std::cout << it->GetSerialNumber() << std::endl;
            if(std::stoi(it->GetSerialNumber().c_str()) == serial)
            {
                device = TlFactory.CreateDevice(*it);
                camFound = true;
                break;
            }
        }
    }
    else
    {
//        std::cerr << "No devices found!" << std::endl;
        return -1;
    }
    if(!camFound)
    {
//        std::cerr << "Camera " << serial << " not found."<< std::endl;
        return -2;
    }

    Pylon::CInstantCamera *cam = new Pylon::CInstantCamera(device);
    // determine which camera is being set
    if(laneNumber == 1)
        cam1 = cam;
    else if(laneNumber == 2)
        cam2 = cam;
    else return 1;
    // check if camera is open
    if(cam->IsOpen())
    {
//        std::cout << "The selected device: " << cam->GetDeviceInfo().GetSerialNumber() << "(serial) is already open." << std::endl;
        cam->Close();
        return -3;
    }

	cam->Open();

//	std::cout << "Saving features file" << std::endl;
//	Pylon::CFeaturePersistence::Save(Filename, &(cam->GetNodeMap()));

    // load the settings
    try{
//		std::cout << "Loading features file" << std::endl;

		Pylon::CFeaturePersistence::Load(Filename, &(cam->GetNodeMap()), true);
        // tensorflow expects RGB format
        GenApi::CEnumerationPtr(cam->GetNodeMap().GetNode("PixelFormat"))->FromString("RGB8");
    }
    catch(const GenericException &e){
//        std::cout << "Failed to load camera settings" << std::endl;
//        std::cout << e.what() << std::endl;
        cam->Close();
        return -4;
    } 

	return 1;	// success
}


// __declspec(dllexport) int create_cam(int a)
// {
// 	PyObject *pArgs = NULL;
// 	PyObject *pValue= NULL;
// 	int result = 0;

// 	if (pFunc1 && PyCallable_Check(pFunc1))
// 	{
// 		pArgs = PyTuple_New(1);
// 		PyTuple_SetItem(pArgs, 0, PyLong_FromLong(a));

// 		pValue = PyObject_CallObject(pFunc1, pArgs);
// 		Py_DECREF(pArgs);
// 		if (pValue != NULL)
// 		{
// 			if (PyList_Size(pValue) >= 1)
// 			{
// 				PyObject * PyValue;
				
// 				PyValue = PyList_GetItem(pValue, 0);
// 				result = PyLong_AsLong(PyValue);
// 				return result;
// 			}
// 		}
// 		else {
// 			return -1;
// 		}
// 	}
// 	return -2;
// }


int getPythonTraceback(PyObject *type, PyObject *value, PyObject *traceback)
{
	// Python equivilant:
	// import traceback, sys
	// return "".join(traceback.format_exception(sys.exc_type,
	//    sys.exc_value, sys.exc_traceback))

//	PyObject *type, *value, *traceback;
	PyObject *tracebackModule;
	wchar_t *chrRetval;
	Py_ssize_t numChars;

//	PyErr_Fetch(&type, &value, &traceback);

	tracebackModule = PyImport_ImportModule("traceback");
	if (tracebackModule != NULL)
	{
		PyObject *tbList, *emptyString, *strRetval;

		tbList = PyObject_CallMethod(
			tracebackModule,
			"format_exception",
			"OOO",
			type,
			value == NULL ? Py_None : value,
			traceback == NULL ? Py_None : traceback);

		emptyString = PyUnicode_FromString("");
		strRetval = PyObject_CallMethod(emptyString, "join",
			"O", tbList);

		chrRetval = PyUnicode_AsWideCharString(strRetval, &numChars);
		wcscpy_s(msgBuffer, 4096, chrRetval);

		wcstombs(tracebuff, msgBuffer, 4096);

		Py_DECREF(tbList);
		Py_DECREF(emptyString);
		Py_DECREF(strRetval);
		Py_DECREF(tracebackModule);
	}
	else
	{
		wcscpy_s(msgBuffer, 4096, L"Unable to import traceback module.");
	}

//	Py_DECREF(type);
//	Py_XDECREF(value);
//	Py_XDECREF(traceback);

	return true;
}

void PyErrorHandler(void)
{
	char * temp1 = NULL;

	/* process Python-related errors */
	/* call after Python API raises an exception */

	PyObject *errobj, *errdata, *errtraceback, *pystring;
//	printf("%s\n", msgFromC);

	/* get latest python exception info */
	PyErr_Fetch(&errobj, &errdata, &errtraceback);
	getPythonTraceback(errobj, errdata, errtraceback);

	pystring = NULL;
	if ((errobj != NULL) && ((pystring = PyObject_Str(errobj)) != NULL))
	{
		if (PyUnicode_Check(pystring))
		{
			PyUnicode_AsWideChar(pystring, temp, 1024);
			wcstombs(save_error_type, temp, 1024);
		}
	}
	else
		strcpy_s(save_error_type, "<unknown exception type>");
	Py_XDECREF(pystring);

	pystring = NULL;
	if ((errdata != NULL) && ((pystring = PyObject_Str(errdata)) != NULL))
	{
		if(PyUnicode_Check(pystring))
		{
			PyUnicode_AsWideChar(pystring, temp, 1024);
			wcstombs(save_error_info, temp, 1024);
		}
	}
	else
		strcpy_s(save_error_info, "<unknown exception data>");
	Py_XDECREF(pystring);

	pystring = NULL;
	if ((errtraceback != NULL) && ((pystring = PyObject_Str(errtraceback)) != NULL))
	{
		if (PyUnicode_Check(pystring))
		{
			PyUnicode_AsWideChar(pystring, temp, 2048);
			wcstombs(save_error_trace, temp, 2048);
		}
	}
	else
		strcpy_s(save_error_type, "<unknown exception trace>");
	Py_XDECREF(pystring);

	
	//	printf("%s\n%s\n", save_error_type, save_error_info);
	Py_XDECREF(errobj);
	Py_XDECREF(errdata);         /* caller owns all 3 */
	Py_XDECREF(errtraceback);    /* already NULL'd out */
}

__declspec(dllexport) void Pyerror(char *msgType, char *msgInfo, char *msgTrace)
	{	
		strcpy(msgType, save_error_type);
		strcpy(msgInfo, save_error_info);
		strcpy(msgTrace, tracebuff);
		
}

}
