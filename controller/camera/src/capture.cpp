#include <iostream>
#include <sstream>
#include "camera/Camera.h"

using namespace baslerCam;

int main(int argc, char *argv[])
{
    auto cams = Camera::getAvailableCameras();
    if(!cams.size())
    {
        std::cout << "No cameras attached" << std::endl;
        return 1;
    }

    if(argc < 3)
    {
        std::cout << "Error, please include arguments: " << std::endl;
        std::cout << "cam1ExposureTime cam2ExposureTime colorTemp tint fps" << std::endl;
        std::cout << "for now do not exceed 7 FPS" << std::endl;
        return 1;
    }

    std::cout << cams.size() << std::endl;
    CameraSettings cam1Sets;
    CameraSettings cam2Sets;
    // camSets.exposureTime = 334e3;
    cam1Sets.exposureTime = std::atof(argv[1]);
    // camSets.colorTemp = 4047;
    cam1Sets.colorTemp = std::atof(argv[3]);
    // camSets.tint = 1066;
    cam1Sets.tint = std::atof(argv[4]);
    // camSets.tint = 853;

    cam2Sets.exposureTime = std::atof(argv[2]);
    cam2Sets.colorTemp = std::atof(argv[3]);
    cam2Sets.tint = std::atof(argv[4]);

    Camera cam1(cams[0], cam1Sets, true);
    Camera cam2(cams[1], cam2Sets, true);
    auto & classCam1 = cam1.getCameraSettings();
    auto & classCam2 = cam2.getCameraSettings();

    double fps = std::atof(argv[5]);
    cam1.startPush();
    cam2.startPush();
    cam1.startCounting();
    cam2.startCounting();

    cam1.getCameraSettings().print(std::cout);
    std::cout << std::endl;
    cam2.getCameraSettings().print(std::cout);

    std::chrono::system_clock::time_point triggerTime;
    // std::string name = std::string("images/") + std::to_string(classCam1.colorTemp) + "_" + std::to_string(classCam1.tint) + "_" + std::to_string(classCam1.hue) + ".bmp";
    // std::cout << name << std::endl;

    double time = 1800;

    std::chrono::duration<double> totalTime(time);
    std::chrono::duration<double> currentTime(0);
    auto start = std::chrono::system_clock::now();
    int count = 0;
    while(currentTime < totalTime)
    {
        auto now = std::chrono::system_clock::now();
        auto milliSec = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000; 
	auto now_c = std::chrono::system_clock::to_time_t(now);
        std::stringstream dateTime;
        dateTime << std::put_time(std::localtime(&now_c), "%H:%M:%S");
	dateTime << ":" << std::setfill('0') << std::setw(3) << milliSec.count();
	dateTime << std::put_time(std::localtime(&now_c), "_%m-%d-%y");
        
        cam1.triggerImage(std::string("cam1/cam1_") + dateTime.str() +".bmp", triggerTime);
        cam2.triggerImage(std::string("cam2/cam2_") + dateTime.str() +".bmp", triggerTime);
        usleep(1/fps * 1e6);
        currentTime = std::chrono::system_clock::now() - start;
        count++;
    }

    usleep(1e6);

    std::cout << "images: " << cam1.getImageCounter() << std::endl;
    std::cout << "fps: " << (double)cam1.getImageCounter() / time << std::endl;
    std::cout << "images: " << cam2.getImageCounter() << std::endl;
    std::cout << "fps: " << (double)cam2.getImageCounter() / time << std::endl;
    return 0;
}
