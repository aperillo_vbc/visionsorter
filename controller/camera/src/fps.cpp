#include <iostream>
#include "camera/Camera.h"

using namespace toupCam;

int main(int argc, char *argv[])
{
    auto cams = Camera::getAvailableCameras();
    if(!cams.size())
    {
        std::cout << "No cameras attached" << std::endl;
        return 1;
    }

    if(argc < 3)
    {
        std::cout << "Error, please include arguments: " << std::endl;
        std::cout << "exposureTime colorTemp tint fps" << std::endl;
        std::cout << "for now do not exceed 7 FPS" << std::endl;
        return 1;
    }

    std::cout << cams.size() << std::endl;
    CameraSettings camSets;
    // camSets.exposureTime = 334e3;
    camSets.exposureTime = std::atof(argv[1]);
    // camSets.colorTemp = 4047;
    camSets.colorTemp = std::atof(argv[2]);
    // camSets.tint = 1066;
    camSets.tint = std::atof(argv[3]);
    // camSets.tint = 853;
    Camera cam1(cams[0], camSets, true);
    Camera cam2(cams[1], camSets, true);
    auto & classCam = cam1.getCameraSettings();

    double fps = std::atof(argv[4]);
    cam1.startPush();
    cam2.startPush();
    cam1.startCounting();
    cam2.startCounting();

    cam1.getCameraSettings().print(std::cout);


    std::chrono::system_clock::time_point triggerTime;
    std::string name = std::string("images/") + std::to_string(classCam.colorTemp) + "_" + std::to_string(classCam.tint) + "_" + std::to_string(classCam.hue) + ".bmp";
    std::cout << name << std::endl;

    double time = 5;

    std::chrono::duration<double> totalTime(time);
    std::chrono::duration<double> currentTime(0);
    auto start = std::chrono::system_clock::now();
    int count = 0;
    while(currentTime < totalTime)
    {
        cam1.triggerImage(std::string("images/cam1_") + std::to_string(count)+".bmp", triggerTime);
        cam2.triggerImage(std::string("images/cam2_") + std::to_string(count)+".bmp", triggerTime);
        usleep(1/fps * 1e6);
        currentTime = std::chrono::system_clock::now() - start;
        count++;
    }

    usleep(1e6);

    std::cout << "images: " << cam1.getImageCounter() << std::endl;
    std::cout << "fps: " << (double)cam1.getImageCounter() / time << std::endl;
    std::cout << "images: " << cam2.getImageCounter() << std::endl;
    std::cout << "fps: " << (double)cam2.getImageCounter() / time << std::endl;
    return 0;
}
