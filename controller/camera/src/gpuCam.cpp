#include <iostream>
#include <sstream>
#include "camera/Camera.h"
#include "Model.h"
#include "Recipes.h"
#include "daq/DAQclass.h"

using namespace baslerCam;

void showSmall(const std::string &str, const cv::Mat &mat)
{
    cv::Mat small;
    cv::resize(mat, small, cv::Size(), 0.4, 0.4);
    cv::imshow(str, small);
}

int main(int argc, char *argv[])
{
    Pylon::PylonAutoInitTerm autoInitTerm;  // PylonInitialize() will be called now

    DAQPtr daq = DAQPtr(new DAQ);
    ModelPtr model = ModelPtr(new Model);
    RecipesPtr recipes;
    std::exception_ptr eptr;

    recipes = RecipesPtr(new Recipes(model));

    std::vector<baslerCam::CameraPtr> cameras;

    cameras = recipes->getCameras(daq);

    if(!cameras.size())
    {
        std::cout << "No cameras attached" << std::endl;
        return 1;
    }

    std::cout << "Number of cams found: " << cameras.size() << std::endl;
 
    cameras[0]->getCameraSettings().print(std::cout);

    cameras[0]->startGrab();
    cv::cuda::GpuMat image;
    cv::Mat image2;
//    cameras[0]->trigger_block_gpu_rgb(image);
//    image.download(image2);
    
    image2 = cameras[0]->trigger_block_rgb();
    showSmall("gpu", image2);
    cv::waitKey(0);
    return 0;
}
