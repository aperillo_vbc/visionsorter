#include <iostream>
#include <sstream>
#include "camera/Camera.h"

using namespace toupCam;

int main(int argc, char *argv[])
{
    auto cams = Camera::getAvailableCameras();
    if(!cams.size())
    {
        std::cout << "No cameras attached" << std::endl;
        return 1;
    }

    std::cout << cams.size() << std::endl;
    CameraSettings cam1Sets;
    cam1Sets.eSize = 1;
    // cam1Sets.exposureTime = 85e3;
    cam1Sets.exposureTime = 2e3;
    cam1Sets.colorTemp = 4600;
    cam1Sets.tint = 947;

    // cam1Sets.exposureTime = std::atof(argv[1]);
    // cam1Sets.colorTemp = std::atof(argv[3]);
    // cam1Sets.tint = std::atof(argv[4]);

    Camera cam1(cams[0], cam1Sets, true);
    auto & classCam1 = cam1.getCameraSettings();

    cam1.startPush(true);
    cam1.getCameraSettings().print(std::cout);

    cv::cuda::GpuMat image;
    cv::Mat image2;
    int avg = 0;
    int num_trials = 30;
    for(size_t i = 0; i < num_trials; ++i)
    {
        auto start = std::chrono::system_clock::now();
        cv::Mat temp = cam1.trigger_block_bgr();
        image.upload(temp);
        cv::cuda::cvtColor(image, image, CV_RGB2BGR);
        image.download(image2);
        // cv::cvtColor(temp, image2, CV_RGB2BGR);
        if(i==0)
            continue;
        // image = temp.clone();
        auto end = std::chrono::system_clock::now();
        std::cout << "time to trigger, capture, and return: "
                  << cam1.getTotalTime().count() << " ms" << std::endl;
        std::cout << "total time with copy: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << " ms" << std::endl;
        std::cout << "time from trigger to callback: "
                  << cam1.getCallBackTime().count() << " ms" << std::endl;
        avg += cam1.getTotalTime().count();
    }
    double time = (double)avg/(double)num_trials;
    std::cout << "avg latency: " << time << ", avg fps: " << 1/(time*1e-3) << std::endl;

    // imshow("image", image2);
    cv::waitKey(0);

    return 0;
}
