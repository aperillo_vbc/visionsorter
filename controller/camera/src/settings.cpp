#include <iostream>
#include "camera/Camera.h"

using namespace toupCam;

int main()
{
    auto cams = Camera::getAvailableCameras();
    if(!cams.size())
    {
        std::cout << "No cameras attached" << std::endl;
        return 1;
    }

    std::cout << cams.size() << std::endl;
    CameraSettings camSets;
    camSets.exposureTime = 334e3;
    camSets.colorTemp = 4047;
    camSets.tint = 1066;
    // camSets.tint = 853;
    Camera cam1(cams[0], camSets, true);
    auto & classCam = cam1.getCameraSettings();

    double fps = 1;
    cam1.startPush();
    cam1.startCounting();
    cam1.getCameraSettings().print(std::cout);
    // cam1.getFileHeader().print(std::cout);


    std::chrono::system_clock::time_point triggerTime;
    std::string name = std::string("images/") + std::to_string(classCam.colorTemp) + "_" + std::to_string(classCam.tint) + "_" + std::to_string(classCam.hue) + ".bmp";
    std::cout << name << std::endl;
    cam1.triggerImage(name, triggerTime);
    usleep(1./fps * 1e6);

    return 0;
}
