import sys
import numpy as np
sys.argv = ["classify.py"]
import os
import tensorflow as tf
import keras.backend.tensorflow_backend as KTF
import scipy.misc
#import cv2

# def get_session(gpu_fraction=0.65):
#     num_threads = os.environ.get('OMP_NUM_THREADS')
#     gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)

#     if num_threads:
#         return tf.Session(config=tf.ConfigProto(
#             gpu_options=gpu_options, intra_op_parallelism_threads=num_threads))
#     else:
#         return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


gpu_options = tf.GPUOptions(visible_device_list="0")
#print("reached")
config = tf.ConfigProto(gpu_options=gpu_options)
session = tf.Session(config=config)
KTF.set_session(session)

from keras.models import load_model
import datetime
import time

model = None

def init_model(model_path):
    global model
    model = load_model(model_path)

def classify(images):
    # tic = time.time()
   # print("shape: ", images.shape, images.shape[0])
    result = model.predict(images, batch_size = images.shape[0])
    # with open("file.txt" , "a") as file:

#    print(result)
 # cv2.imwrite('img.jpg', images)
#    for index, image in enumerate(images):
#        fileName =  "xyz/py_" + datetime.datetime.now().strftime('%I-%M-%S-%f%p_%d-%b-%Y') + ".jpeg"
#        scipy.misc.imsave(fileName, image)
        # file.write("{}     {} \n".format(result[index],fileName))

   #toc = time.time()
    #print("time to classiy: ",toc-tic)
    #for index, image in enumerate(images):
     #fileName = None
     #if result[index][0] > result[index][1]:
      #fileName = "data/py_" + datetime.datetime.now().strftime('%I-%M-%S-%f%p_%d-%b-%Y') + ".jpeg"
     #else:
      #fileName = "/mnt/data/wrought/wrought_" + datetime.datetime.now().strftime('%I-%M-%S-%f%p_%d-%b-%Y') + "_" + str(result[index, 0]) + "_" + str(result[index, 1]) + ".jpeg"
      #scipy.misc.imsave(fileName, image)
    #toc = time.time()
    #print(result)
    return result
