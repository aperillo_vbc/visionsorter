#ifndef VISION_CLASSIFICATION_H
#define VISION_CLASSIFICATION_H

#include <vector>
#include <string>
#include <memory>
#include <chrono>
#include <iostream>
#include <opencv2/opencv.hpp>

#if CV_MAJOR_VERSION >= 3
#include <opencv2/cudaimgproc.hpp>
#endif

struct PyErrorMsg
{
    PyErrorMsg(bool success = false) :
        success(success),
        error_trace(""),
        error_type(""),
        error_info("")
    {}
    inline std::string getString()
    {
        if(error_type == "")
            return error_trace + error_info + "\n";
        else
            return error_trace + error_type + std::string("\n") + error_info + "\n";
    }
    bool success;
    std::string error_trace, error_type, error_info;
};

inline void printErrorMsg(const PyErrorMsg &msg)
{
    std::cout << msg.error_trace;
    std::cout << msg.error_type << std::endl;
    std::cout << msg.error_info << std::endl;
}


struct SubImg{
    SubImg(){}
    SubImg(cv::Mat _img, cv::Rect2i _rectP)
        : img(_img), rectP(_rectP)
    {}
    SubImg(cv::Mat _img,
           const cv::Rect2i &_rectP,
           const std::chrono::system_clock::time_point &_timePt)
        : img(_img), rectP(_rectP), timePt(_timePt)
    {}
    SubImg(cv::Mat _img,
           const cv::Rect2i &_rectP,
           const std::chrono::system_clock::time_point &_timePt,
           const std::string &_camSID)
        : img(_img), rectP(_rectP), timePt(_timePt), camSID(_camSID)
    {}
    inline double xD(){return rectD.x;}
    inline double yD(){return rectD.y;}
    inline double widthD(){return rectD.width;}
    inline double heightD(){return rectD.height;}
    inline int xP(){return rectP.x;}
    inline int yP(){return rectP.y;}
    inline int widthP(){return rectP.width;}
    inline int heightP(){return rectP.height;}
    cv::Mat img;
    cv::Rect2d rectD;
    cv::Rect2i rectP;
    double probClass0, probClass1, probClass2;
    std::vector<double> classes;
    std::chrono::system_clock::time_point timePt;
    std::string camSID;
};

inline void saveSubImg(const SubImg &subimg, int index = 0)
{
    cv::Mat img;
    subimg.img.convertTo(img, CV_8UC3, 255.);

#if CV_MAJOR_VERSION != 4 && CV_MAJOR_VERSION != 3

        cv::cvtColor(img, img, CV_RGB2BGR);
#else
        cv::cvtColor(img, img, cv::COLOR_RGB2BGR);
#endif

    std::stringstream timeStr;
    timeStr << std::string("data/") << subimg.camSID << "_";
    auto triggerTime = std::chrono::system_clock::now();
    auto now_c = std::chrono::system_clock::to_time_t(triggerTime);
    timeStr << std::put_time(std::localtime(&now_c), "%H-%M-%S-");
    timeStr << std::setfill('0') << std::setw(3);
    auto fraction = triggerTime - std::chrono::time_point_cast<std::chrono::seconds>(triggerTime);
    timeStr << std::chrono::duration_cast<std::chrono::milliseconds>(fraction).count();
    timeStr << std::put_time(std::localtime(&now_c), "_%m-%d-%y") << "_";
    for(auto &prob : subimg.classes)
    {
        timeStr << std::setprecision(2) << prob << "_";
    }
    timeStr << "_" << index;
    timeStr << ".bmp";
    cv::imwrite(timeStr.str(), img);
}

inline void printSubImg(std::ostream &out, SubImg &subimg)
{
    out << "rectD: " << subimg.xD() << ", " << subimg.yD()
        << " width, height: " << subimg.widthD() << ", " << subimg.heightD() << std::endl;
    out << "rectP: " << subimg.xP() << ", " << subimg.yP()
        << " width, height: " << subimg.widthP() << ", " << subimg.heightP() << std::endl;
    for(auto &prob : subimg.classes)
        out << prob << " ";
    out << std::endl;
}


typedef SubImg Scrap;

class Classifications{
public:
    inline void push_back(const Scrap &scrap){classes.push_back(scrap);}
    inline std::vector<Scrap> & getClasses(){return classes;}
    inline size_t size(){return classes.size();}
    Scrap& operator[](int i){return classes[i];}
    std::chrono::system_clock::time_point timeStamp;
private:
    std::vector<Scrap> classes;
};

void uninit_classifier(void);
void finalize_classifier(void);
int init_classifier(const std::string &model_path);
void classify(cv::cuda::GpuMat &d_img, const std::vector<cv::Rect> &pieces,  Classifications &classes);
void classify(std::vector<SubImg> &subimgs);

#endif
