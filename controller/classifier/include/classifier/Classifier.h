#ifndef VISION_CLASSIFIER_H
#define VISION_CLASSIFIER_H

#include <vector>
#include <string>
#include <memory>
#include <chrono>
#include "Python.h"
#include <opencv2/opencv.hpp>

class Classifier;

typedef std::shared_ptr<Classifier> ClassifierPtr;

struct PyErrorMsg{
    PyErrorMsg(bool success = false) :
        success(success),
        error_type(""),
        error_info(""),
        error_trace("")
    {}
    bool success;
    std::string error_type, error_info, error_trace;
};

void printErrorMsg(const PyErrorMsg &msg);

struct Scrap{
    cv::Rect2d rectD, rectP;
    inline double xD(){return rectD.x;}
    inline double yD(){return rectD.y;}
    inline double widthD(){return rectD.width;}
    inline double heightD(){return rectD.height;}
    inline double xP(){return rectP.x;}
    inline double yP(){return rectP.y;}
    inline double widthP(){return rectP.width;}
    inline double heightP(){return rectP.height;}
    double probClass0, probClass1;
};

inline void printScrap(std::ostream &out, Scrap &scrap)
{
    out << "rect: " << scrap.xD() << ", " << scrap.yD()
        << " width,height: " << scrap.widthD() << ", " << scrap.heightD() << std::endl;
}

class Classifications{
public:
    inline void push_back(const Scrap &scrap){classes.push_back(scrap);}
    inline std::vector<Scrap> & getClasses(){return classes;}
    inline size_t size(){return classes.size();}
    Scrap& operator[](int i){return classes[i];}
    std::chrono::system_clock::time_point timeStamp;
private:
    std::vector<Scrap> classes;
};


class Classifier
{
public:
    Classifier(const std::string &model_path);
    ~Classifier();
    PyErrorMsg init();
    PyErrorMsg init_model();
    int classify(cv::cuda::GpuMat &d_img, const std::vector<cv::Rect> &pieces,  Classifications &classes);
    inline void setModelPath(const std::string &model_path){model_path_=model_path;}
    inline std::string getModelPath() const {return model_path_;}
protected:
    int init_py();
    PyErrorMsg PyErrorHandler();
    std::string model_path_;
    size_t img_size_;
    cv::Size img_rect_size_;
    double scale_factor_;
    PyObject *py_classify_module;
    PyObject *py_classify_func;
    PyObject *py_init_func;
    PyErrorMsg error_msg;
};

#endif
