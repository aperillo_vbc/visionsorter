#include "classifier/Classification.h"
#include <vector>
#include <string>
#include <cstring>
#include "Python.h"
#include "numpy/ndarrayobject.h"

//#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>

static bool py_initialized_ = false;

static std::string model_path_;
static size_t img_size_;
static cv::Size img_rect_size_;
static double scale_factor_;

static PyObject *py_classify_module;
static PyObject *py_classify_func;
static PyObject *py_init_func;

static PyErrorMsg PyErrorHandler();

int init_classifier(const std::string &model_path)
{
    py_classify_module = NULL;
    py_classify_func = NULL;
    py_init_func = NULL;
    model_path_ = model_path;
    img_size_ = 299 * 299 * 3;
    img_rect_size_ = cv::Size(299, 299);
    scale_factor_ = 1. / 255.;

    if(!py_initialized_)
    {
        Py_Initialize();
        import_array();
        py_initialized_ = true;
    }
    PyObject *mod_name = PyUnicode_FromString("classify");
    if(PyErr_Occurred())
    {
        // PyErr_Print();
        throw std::runtime_error(PyErrorHandler().getString());
    }
    py_classify_module = PyImport_Import(mod_name);
    Py_DECREF(mod_name);
    if(PyErr_Occurred())
        throw std::runtime_error(PyErrorHandler().getString());
        //calling the classify function from python file classify.py
    py_classify_func = PyObject_GetAttrString(py_classify_module,
                                              "classify");
    if(PyErr_Occurred())
        throw std::runtime_error(PyErrorHandler().getString());
          //calling the init_model function from python file classify.py
    py_init_func = PyObject_GetAttrString(py_classify_module,
                                          "init_model");
    if(PyErr_Occurred())
        throw std::runtime_error(PyErrorHandler().getString());

    if(!py_init_func || !PyCallable_Check(py_init_func))
        throw std::runtime_error("The python initialization function is not callable, this error should not be reached.");
    std::cout << model_path_ << std::endl;
    PyObject *model_name = PyUnicode_FromString(model_path.c_str());
    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, model_name);
    PyObject *pValue = PyObject_CallObject(py_init_func, pArgs);
    if(pValue)
        Py_DECREF(pValue);
    Py_DECREF(pArgs);
    if(PyErr_Occurred())
        throw std::runtime_error(PyErrorHandler().getString());

    return 0;
}

void uninit_classifier(void)
{
    Py_XDECREF(py_classify_module);
    Py_XDECREF(py_classify_func);
    Py_XDECREF(py_init_func);
}

void finalize_classifier(void)
{
    uninit_classifier();
    if(py_initialized_)
        Py_Finalize();
}

PyErrorMsg PyErrorHandler()
{
    PyErrorMsg msg(false);
    std::string &error_info = msg.error_info;
    error_info = "An exception occured in python code.";
    std::string &error_trace = msg.error_trace;
    std::string &error_type = msg.error_type;

    /* process Python-related errors */
    /* call after Python API raises an exception */

    PyObject *type, *value, *traceback, *pystring;

    /* get latest python exception info */
    std::string err_msg;
    PyErr_Fetch(&type, &value, &traceback);
    pystring = NULL;
    if ((type != NULL) && ((pystring = PyObject_Str(type)) != NULL))
        error_type = std::string(PyUnicode_AsUTF8(pystring));
    else
        error_type = std::string("<unknown exception type>");
    Py_XDECREF(pystring);

    pystring = NULL;
    if ((value != NULL) && ((pystring = PyObject_Str(value)) != NULL))
        error_info = std::string(PyUnicode_AsUTF8(pystring));
    else
        error_info = std::string("<unknown exception data>");
    Py_XDECREF(pystring);

    PyObject *traceback_mod = PyImport_ImportModule("traceback");
    if (traceback_mod != NULL)
    {
        PyErr_NormalizeException(&type, &value, &traceback);

        if(traceback != NULL)
            PyException_SetTraceback(value, traceback);
        else
        {
            error_trace = std::string("Unable to retrieve trace.\n");
            return msg;
        }
        PyObject * method = PyObject_GetAttrString(traceback_mod, "format_tb");
        if(method != NULL && PyCallable_Check(method))
        {
            PyObject *list = PyObject_CallFunctionObjArgs(method, traceback, Py_None, NULL);
            if(list == NULL)
            {
                error_trace = std::string("Unable to format trace.");
            }
            else
            {
                PyObject *delim = PyUnicode_FromString("");
                PyObject *str = PyUnicode_Join(delim, list);
                error_trace = std::string(PyUnicode_AsUTF8(str));
                Py_DECREF(list);
                Py_DECREF(delim);
                Py_DECREF(str);
            }
        }

        Py_DECREF(traceback_mod);
    }
    else
    {
        error_trace = std::string("Unable to import traceback module.");
        return msg;
    }
//std::cout<<"classfication test py handler function\n";

    Py_XDECREF(type);
    Py_XDECREF(value);
    Py_XDECREF(traceback);
    return msg;
}

void classify(cv::cuda::GpuMat &d_img, const std::vector<cv::Rect> &pieces,  Classifications &classes)
{
    cv::cuda::GpuMat d_img_mod;
    d_img.convertTo(d_img_mod,  CV_32FC3, scale_factor_);
    float *data = new float[pieces.size()*img_size_];
    float *data_start = data;

    for(size_t i = 0; i < pieces.size(); ++i)
    {
        cv::cuda::GpuMat d_img_roi(d_img_mod, pieces[i]);
        cv::cuda::resize(d_img_roi, d_img_roi, img_rect_size_, 0, 0, cv::INTER_NEAREST);
        cv::Mat temp(img_rect_size_, CV_32FC3, data_start);
        d_img_roi.download(temp);
        data_start += img_size_;
    }

    npy_intp dims[4];
    dims[0] = pieces.size();
    dims[1] = img_rect_size_.height;
    dims[2] = img_rect_size_.width;
    dims[3] = 3;

    PyObject *numpy_array = PyArray_SimpleNewFromData(
        4,
        dims,
        NPY_FLOAT,
        data);

    if(!py_classify_func || !PyCallable_Check(py_classify_func))
    {
        delete data;
        std::cout << "The function is not callable." << std::endl;
        return;
    }
    PyObject *args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, numpy_array);
    PyObject *val = PyObject_CallObject(py_classify_func, args);
    if(PyErr_Occurred())
    {
        Py_XDECREF(val);
        Py_XDECREF(args);
        throw std::runtime_error(PyErrorHandler().getString());
    }

    for(size_t i = 0; i < pieces.size(); ++i)
    {
        classes[i].probClass0 = *((float*)PyArray_GETPTR2(val, i, 0));
        classes[i].probClass1 = *((float*)PyArray_GETPTR2(val, i, 1));
 //       classes[i].probClass2 = *((float*)PyArray_GETPTR2(val, i, 2));
        std::cout << "prob0: " << classes[i].probClass0 << "  prob1: " << classes[i].probClass1 
//	<< "  prob2: " << classes[i].probClass2
 	<< std::endl;
    }
//std::cout<<"classfication test classify function\n";
    Py_XDECREF(val);
    Py_XDECREF(args);
    delete data;
}

// the classify function below is called from the vision loop
void classify(std::vector<SubImg> &subimgs)
{
    float *data = new float[subimgs.size()*img_size_];
    float *data_start = data;

    // sub images ara the images which are segmented out when the vision loop is running

    for(auto &subimg : subimgs)
    {
        std::memcpy(data_start, subimg.img.data,sizeof(float)*img_size_);
        data_start += img_size_;
    }

    npy_intp dims[4];
    dims[0] = subimgs.size();
    dims[1] = img_rect_size_.height;
    dims[2] = img_rect_size_.width;
    dims[3] = 3;

    PyObject *numpy_array = PyArray_SimpleNewFromData(
        4,
        dims,
        NPY_FLOAT,
        data);
//std::cout<<"classfication test\n";
    if(!py_classify_func || !PyCallable_Check(py_classify_func))
    {
        delete data;
        std::cout << "The function is not callable." << std::endl;
        return;
    }
    PyObject *args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, numpy_array);
    PyObject *val = PyObject_CallObject(py_classify_func, args);
    if(PyErr_Occurred())
    {
        Py_XDECREF(val);
        Py_XDECREF(args);
        throw std::runtime_error(PyErrorHandler().getString());
    }
//
    for(size_t i = 0; i < subimgs.size(); ++i)
    {
        subimgs[i].classes.resize(0);
        for(size_t j = 0; j < 2; ++j)
        {
             subimgs[i].classes.push_back(*((float*)PyArray_GETPTR2(val, i, j)));
         //  subimgs[i].probClass0 = *((float*)PyArray_GETPTR2(val, i, 0));
            //subimgs[i].probClass1 = *((float*)PyArray_GETPTR2(val, i, 1));
//std::cout<<*((float*)PyArray_GETPTR2(val, i, j))<<std::endl;
        }
         std::cout << "prob0: " << subimgs[i].classes[0] << " prob1: " << subimgs[i].classes[1] 
//	<< " prob2: " << subimgs[i].classes[2]
 	<< std::endl;
    //     subimgs[i].probClass0 = *((float*)PyArray_GETPTR2(val, i, 0));
    //     subimgs[i].probClass1 = *((float*)PyArray_GETPTR2(val, i, 1));
    }

    Py_XDECREF(val);
    Py_XDECREF(args);
    delete data;
}
