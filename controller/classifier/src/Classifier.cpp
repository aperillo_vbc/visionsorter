#include "classifier/Classifier.h"
#include <opencv2/core/core.hpp>
#include <vector>
#include <string>
#include <cstring>
#include "Python.h"
#include <opencv2/opencv.hpp>
#include "numpy/ndarrayobject.h"
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>

Classifier::Classifier(const std::string &model_path)
{
    img_size_ = 299 * 299 * 3;
    img_rect_size_ = cv::Size(299,299);
    scale_factor_ = 1./255.;
    model_path_ = model_path;
    init_py();
}

int Classifier::init_py()
{
    Py_Initialize();
    import_array();
}

Classifier::~Classifier()
{
    if(py_classify_func != nullptr)
        Py_XDECREF(py_classify_func);
    if(py_classify_func != nullptr)
        Py_XDECREF(py_init_func);
    if(py_classify_module != nullptr)
        Py_DECREF(py_classify_module);
    Py_Finalize();
}

PyErrorMsg Classifier::init_model()
{
    if(!py_init_func || !PyCallable_Check(py_init_func))
        return PyErrorMsg();
    PyObject *name = PyUnicode_FromString(model_path_.c_str());
    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, name);
    PyObject *pValue = PyObject_CallObject(py_init_func, pArgs);
    Py_DECREF(name);
    Py_DECREF(pValue);
    Py_DECREF(pArgs);
    if(PyErr_Occurred())
        return PyErrorHandler();
    else
        return PyErrorMsg(true);
}

PyErrorMsg Classifier::init()
{
    PyObject *py_classify_module_name = PyUnicode_FromString("classify");
    py_classify_module = PyImport_Import(py_classify_module_name);
    if(PyErr_Occurred())
    {
        Py_DECREF(py_classify_module_name);
        py_classify_func = nullptr;
        py_init_func = nullptr;
        return PyErrorHandler();
    }

    py_classify_func = PyObject_GetAttrString(py_classify_module,
                                                "classify");
    py_init_func = PyObject_GetAttrString(py_classify_module,
                                            "init_model");
    PyErrorMsg msg = init_model();
    Py_DECREF(py_classify_module_name);

    return msg;
}

int Classifier::classify(cv::cuda::GpuMat &d_img, const std::vector<cv::Rect> &pieces,  Classifications &classes)
{
    // cv::cuda::GpuMat d_img_mod;
    // d_img.convertTo(d_img_mod,  CV_32FC3, scale_factor_);
    // float *data = new float[26*299*299*3];
    // float count = 0;
    // for(size_t i = 0; i < 26*299*299*3; ++i)
    //     data[i] = count++;
    // std::cout << img_size_ * pieces.size() << std::endl;
    // float *data_start = data;

    // for(size_t i = 0; i < pieces.size(); ++i)
    // {
    //     cv::cuda::GpuMat d_img_roi(d_img_mod, pieces[i]);
    //     cv::cuda::resize(d_img_roi, d_img_roi, img_rect_size_, 0, 0, cv::INTER_LINEAR);
    //     cv::Mat temp(img_rect_size_, CV_32FC3, data_start);
    //     d_img_roi.download(temp);
    //     data_start += img_size_;
    // }

    // std::cout << pieces.size() << " " << img_rect_size_.height << " " << img_rect_size_.width << " 3" << std::endl;
    // dims[0] = pieces.size();
    // dims[1] = img_rect_size_.height;
    // dims[2] = img_rect_size_.width;
    // dims[3] = 3;
    float *data = new float[26*299*299*3];
    float count = 0;
    for(size_t i = 0; i < 26*299*299*3; ++i)
        data[i] = count++;
    npy_intp dims[4];
    dims[0] = 26;
    dims[1] = 299;
    dims[2] = 299;
    dims[3] = 3;
    PyObject *numpy_array = PyArray_SimpleNewFromData(
        4,
        dims,
        NPY_FLOAT,
        data);

    if(PyErr_Occurred())
    {
        PyErr_Print();
        return 1;
    }
    if(!py_classify_func || !PyCallable_Check(py_classify_func))
        return 1;
    PyObject *args = PyTuple_New(1);
    PyTuple_SetItem(args, 0, numpy_array);
    PyObject *val = PyObject_CallObject(py_classify_func, args);
    if(PyErr_Occurred())
    {
        printErrorMsg(PyErrorHandler());
        if(args)
            Py_DECREF(args);
        if(val)
            Py_DECREF(val);
        return 1;
    }

    PyObject *probability = NULL;
    for(size_t i = 0; i < pieces.size(); ++i)
    {
        std::cout << i << std::endl;
        probability = PyList_GetItem(val, i);
        classes[i].probClass0 = PyFloat_AsDouble(PyList_GetItem(probability, 0));
        classes[i].probClass1 = PyFloat_AsDouble(PyList_GetItem(probability, 1));
    }

    Py_DECREF(probability);
    Py_DECREF(args);
    Py_DECREF(val);
}

PyErrorMsg Classifier::PyErrorHandler()
{
    PyErrorMsg msg(true);
    msg.success = false;
    std::string &error_info = msg.error_info;
    error_info = "An exception occured in python code.";
    std::string &error_trace = msg.error_trace;
    std::string &error_type = msg.error_type;

    /* process Python-related errors */
    /* call after Python API raises an exception */

    PyObject *type, *value, *traceback, *pystring;

    /* get latest python exception info */
    std::string err_msg;
    PyErr_Fetch(&type, &value, &traceback);
    pystring = NULL;
    if ((type != NULL) && ((pystring = PyObject_Str(type)) != NULL))
        error_type = std::string(PyUnicode_AsUTF8(pystring));
    else
        error_type = std::string("<unknown exception type>");
    Py_XDECREF(pystring);

    pystring = NULL;
    if ((value != NULL) && ((pystring = PyObject_Str(value)) != NULL))
        error_info = std::string(PyUnicode_AsUTF8(pystring));
    else
        error_info = std::string("<unknown exception data>");
    Py_XDECREF(pystring);

    // pystring = NULL;
    // if ((traceback != NULL) && ((pystring = PyObject_Str(traceback)) != NULL))
    //     error_trace = std::string(PyUnicode_AsUTF8(pystring));
    // else
    //     error_trace = std::string("<unknown exception trace>");
    // Py_XDECREF(pystring);
    // printErrorMsg(msg);
    PyObject *traceback_mod = PyImport_ImportModule("traceback");
    if (traceback_mod != NULL)
    {
        PyErr_NormalizeException(&type, &value, &traceback);

        if(traceback != NULL)
            PyException_SetTraceback(value, traceback);
        else
        {
            error_trace = std::string("Unable to retrieve trace.");
            msg.success = false;
            return msg;
        }
        PyObject * method = PyObject_GetAttrString(traceback_mod, "format_tb");
        if(method != NULL && PyCallable_Check(method))
        {
            PyObject *list = PyObject_CallFunctionObjArgs(method, traceback, Py_None, NULL);
            // PyObject *list = PyObject_CallFunctionObjArgs(method, traceback, NULL);
            // PyObject *list = PyObject_CallFunctionObjArgs(method, traceback, NULL);
            // PyObject *list = PyObject_CallFunctionObjArgs(
            //     method,
            //     type,
            //     value,
            //     traceback,
            //     NULL);
            // PyObject *list = PyObject_CallFunctionObjArgs(
            //     traceback_mod,
            //     "format_exception",
            //     "OOO",
            //     type,
            //     value == NULL ? Py_None : value,
            //     traceback == NULL ? Py_None : traceback);
            if(list == NULL)
            {
                error_trace = std::string("Unable to format trace.");
                msg.success = false;
            }
            else
            {
                PyObject *delim = PyUnicode_FromString("");
                PyObject *str = PyUnicode_Join(delim, list);
                error_trace = std::string(PyUnicode_AsUTF8(str));
                Py_DECREF(list);
                Py_DECREF(delim);
                Py_DECREF(str);
            }
        }

        Py_DECREF(traceback_mod);
    }
    else
    {
        error_trace = std::string("Unable to import traceback module.");
        msg.success = false;
        return msg;
    }

    Py_XDECREF(type);
    Py_XDECREF(value);
    Py_XDECREF(traceback);
    return msg;
}

void printErrorMsg(const PyErrorMsg &msg)
{
    std::cout << msg.error_type << std::endl;
    std::cout << msg.error_info << std::endl;
    std::cout << msg.error_trace << std::endl;
}
