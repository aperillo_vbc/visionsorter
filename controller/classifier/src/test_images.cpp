#include "classifier/Classification.h"

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        std::cout << "Error, please include the image file to process and number of runs: test_image.py <filename> <# of runs>" << std::endl;
        return 1;
    }

    try
    {
        init_classifier("models/V3_cast_wrought_01-27PM_08-Oct-2017_29-0.007-0.9975.hdf5");
    }
    catch(const std::runtime_error &e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }

    cv::Mat h_img_orig = cv::imread(argv[1]);

    for(size_t k = 0; k < 10; ++k)
    {
        cv::Mat h_img = h_img_orig.clone();
        cv::cuda::GpuMat d_img;
        d_img.upload(h_img);
        auto start_time = std::chrono::system_clock::now();

        std::vector<cv::Rect> pieces;
        cv::cuda::GpuMat d_img_thresh;
        cv::cuda::cvtColor(d_img, d_img_thresh, CV_BGR2GRAY);
        int pixThresh = 100;
        double thresh = cv::cuda::threshold(d_img_thresh, d_img_thresh, pixThresh, 255, cv::THRESH_BINARY);
        int dilationSize = 5;
        cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT,
                                                    cv::Size(
                                                        2 * dilationSize + 1,
                                                        2 * dilationSize + 1));
        cv::Ptr<cv::cuda::Filter> dilateFilter = cv::cuda::createMorphologyFilter(
            cv::MORPH_DILATE,
            CV_8UC1,
            element);
        dilateFilter->apply(d_img_thresh, d_img_thresh);
        cv::Mat h_img_bw;
        d_img_thresh.download(h_img_bw);
        std::vector<std::vector<cv::Point>> contours;
        cv::findContours(h_img_bw, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
        double minArea = 1;
        double maxArea = 33;
        double conv = 0.000124865;
        for (size_t i = 0; i < contours.size(); i++)
        {
            cv::drawContours(h_img, contours, i, cv::Scalar(0, 0, 255), 2);
            cv::Rect bRect = boundingRect(cv::Mat(contours[i]));
            if(bRect.area() * conv > minArea
               && bRect.area() * conv < maxArea)
            {
                cv::rectangle(h_img, bRect.tl(), bRect.br(), cv::Scalar(255, 0, 0), 2, 8, 0);
                pieces.push_back(bRect);
            }
        }
        // cv::imwrite("segmented.jpeg", h_img);

        Classifications classes;
        for(auto &rect : pieces)
        {
            Scrap scrap;
            scrap.rectP = cv::Rect2d(rect.x, rect.y, rect.width, rect.height);
            classes.push_back(scrap);
        }
        try
        {
            classify(d_img, pieces, classes);
        }
        catch(const std::runtime_error &e)
        {
            std::cout << e.what() << std::endl;
        }
        std::cout << "total time: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()-start_time).count() << " (ms)" << std::endl;
        // for(auto &s : classes.getClasses())
        // {
        //     std::cout << s.probClass0 << " " << s.probClass1 << std::endl;
        // }
    }
    uninit_classifier();

    return 0;
}
