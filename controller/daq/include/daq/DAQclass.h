// DAQ Interface, no threads required at this level
// The DAQ interface object will be passed around in a shared pointer to the
// classes which need to access its member functions
#ifndef DAQCLASS_H
#define DAQCLASS_H

#include "USB20xclass.h"
//#include "USB1024LSclass.h"
#include "PCIDIO24Hclass.h"
#include <memory>

class DAQ {

public:
	DAQ();
	~DAQ();

	bool daqDeviceReady();

	// A IN
	double getCabinetTemperature();		// F? C?
	double get24VDC();					// V
	double get5VDC();					// V
	double getCPAPressure();			// psi
  	double getInductive1();
  	double getInductive2();
	// Since the belt speed requires the belt to revolve a couple times,
	// we may want this function to return a -1 or 0 when it does not believe the "value is reasonable",
	// this could be based on proximity to the desired belt speed
  	double getConveyorSpeed();		// V
	bool isDoorIlockSafe();				// true means that the interlocks are in a state safe for operation, i.e., closed
	bool isOtherIlockSafe();			// true means that the interlocks are in a state safe for operation, i.e., closed
  	double getLevelSensor();		// used on Eriez sys #10

	// A OUT
	// Would be nice if we can regress this at
	// some point from a number of measured input speeds and the actual measured speed
	// A second order polynomial would probably be within +- 0.02 ft/min
	void setConveyorSpeed(double speed); 	// V
	void setCreepConveyorSpeed(double speed); 	// V
	void setInclineConveyorSpeed(double speed); 	// V
	void setHD75Speed(double speed);	// V

	void enableRelayPower();		// AOUT2 used to enable relays after everything running-avoid glitches

	// D IN
  	bool getInductive1_D();
  	bool getInductive2_D();
	bool readBit(int port, int pin);
	bool waitForLaserTrigger();

	// D OUT
	void setConveyor(bool on); // true is on, false is off
	void setAC(bool on); // true is on, false is off
	void setFeed(bool on); // true is on, false is off
	void setAlarm(bool on); // true is on, false is off
	void setSpareContactor(bool on); // true is on, false is off
	void setVisionLamps(bool on); // true is on, false is off
	void setCreepFeeder(bool on);
	bool getCreepFeeder(){ return creepFeederOn; }

	// functions to use CTR on 20x boards
	void resetCTR(int device);
	unsigned int  readCTR(int device);

	// Special ------
	// fireSolenoid - This should be blocking, i.e., there is a need to call clock_nanosleep so
	// that the solenoid is kept on for the time specified
	// there will be a separate control layer above the DAQ that threads the solenoid calls,
	// consumes timestamps to fire the solenoids, and otherwise ensures that the correct solenoids are fired
	void fireSolenoid(int solenoid, double time); 	// solenoid number (1-24 are valid values), seconds 								// to keep the solenoid open

	void setSolenoid(int solenoid, bool on);	// just set solenoid on/off, move the sleep
							// up a layer to the nozzle thread
    // accounts for the strange wiring
    void setSolenoidMod(int solenoid, bool on);
private:
	bool deviceInitialized;
	USB20x *mcc20x_1;
	PCIDIO24H *pcidio24H;
//	USB20x *mcc20x_2;
//	USB1024LS *mcc1024LS_1;
//	USB1024LS *mcc1024LS_2;
	bool creepFeederOn;
};

typedef std::shared_ptr<DAQ> DAQPtr;

#endif 		// DAQCLASS_H
