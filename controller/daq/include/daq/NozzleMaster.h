
#ifndef NOZZLEMASTER_H
#define NOZZLEMASTER_H

#include <vector>
#include <chrono>
#include <ctime>
#include <thread>
#include <iostream>
#include "NozzleThread.h"
#include "DAQclass.h"
#include <memory>

class NozzleMaster {
public:
    NozzleMaster(DAQPtr board, int numSolenoids);
    ~NozzleMaster();

    void nozzleFireTime(int nozzleIndex, const NozMsg &msg);
    void setSolenoid(int index, bool on);
private:
    DAQPtr _board;
    int _numSolenoids;
    std::vector<std::thread> nozzleThreads;
    std::vector<nozzleThread *> whichNozzle;
};

typedef std::shared_ptr<NozzleMaster> NozzleMasterPtr;

#endif		// NOZZLEMASTER_H
