#ifndef NOZZLETHREAD_H
#define NOZZLETHREAD_H

#include <vector>
#include <deque>
#include <chrono>
#include <ctime>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "DAQclass.h"

using namespace std;

struct NozMsg{
    std::chrono::system_clock::time_point time;
    std::chrono::microseconds duration;
};

class nozzleThread {
public:
    nozzleThread(DAQPtr board, int solenoid);
    ~nozzleThread();

    void * runLoop(void *arg);
    void addFireTime(const NozMsg &msg);

    std::thread loopThread();
    void stopLoop();

    void setSolenoid(bool on){
        _board->setSolenoid(_solenoid, on);
    }

private:
    DAQPtr _board;
    int _solenoid;
    bool runFlag;
    std::chrono::system_clock::time_point latestTime;
    std::chrono::microseconds latestDuration;
    std::deque<NozMsg> fireTimes;
    struct timespec nozzleOnTime;
    std::mutex qLock;
    std::condition_variable q_cv;
};


#endif		// NOZZLETHREAD_H
