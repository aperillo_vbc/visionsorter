// this class wraps the calls to the public driver written by Warren J. Jasper
#ifndef PCIDIO24HCLASS_H
#define PCIDIO24HCLASS_H

#include <sys/ioctl.h>
#include "pci-dio24H.h"

class PCIDIO24H {
public:

	PCIDIO24H();
	~PCIDIO24H();
	
	bool deviceReady();
	unsigned short setBit(int port, int pin, bool on);
	bool readBit(int port, int pin);
	void setSolenoid(int solenoid, bool on);	// just set solenoid on/off

private:
	bool devicePresent;	
	char DevNameIO[20];
	int  Board;
  	int fd_A, fd_B, fd_C;  
	char digPortABits;
	char digPortBBits;
	char digPortCBits;
};


#endif		// PCIDIO24HCLASS_H

