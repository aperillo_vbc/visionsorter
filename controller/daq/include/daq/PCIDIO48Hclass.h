// this class wraps the calls to the public driver written by Warren J. Jasper
#ifndef PCIDIO48HCLASS_H
#define PCIDIO48HCLASS_H

#include <sys/ioctl.h>
#include "pci-dio48H.h"

class PCIDIO48H {
public:

	PCIDIO48H();
	~PCIDIO48H();
	
	bool deviceReady();
	unsigned short setBit(int port, int pin, bool on);
	bool readBit(int port, int pin);
	void setSolenoid(int solenoid, bool on);	// just set solenoid on/off

private:
	bool devicePresent;	
	char DevNameIO[20];
	int  Board;
  	int fd_0A, fd_0B, fd_0C, fd_1A, fd_1B, fd_1C;
	char digPort0ABits;
	char digPort0BBits;
	char digPort0CBits;
	char digPort1ABits;
	char digPort1BBits;
	char digPort1CBits;
};


#endif		// PCIDIO24CLASS_H

