// this class wraps the calls to the public driver written by Warren J. Jasper
#ifndef PCIDIO96HCLASS_H
#define PCIDIO96HCLASS_H

#include <sys/ioctl.h>
#include "pci-dio96H.h"

class PCIDIO96H {
public:

	PCIDIO96H();
	~PCIDIO96H();
	
	bool deviceReady();
	unsigned short setBit(int port, int pin, bool on);
	bool readBit(int port, int pin);
	void setSolenoid(int solenoid, bool on);	// just set solenoid on/off

private:
	bool devicePresent;	
	char DevNameIO[20];
	int  Board;
  	int fd_0A, fd_0B, fd_0C, fd_1A, fd_1B, fd_1C;
	int fd_2A, fd_2B, fd_2C, fd_3A, fd_3B, fd_3C;
	char digPort0ABits;
	char digPort0BBits;
	char digPort0CBits;
	char digPort1ABits;
	char digPort1BBits;
	char digPort1CBits;
	char digPort2ABits;
	char digPort2BBits;
	char digPort2CBits;
	char digPort3ABits;
	char digPort3BBits;
	char digPort3CBits;
};


#endif		// PCIDIO96CLASS_H

