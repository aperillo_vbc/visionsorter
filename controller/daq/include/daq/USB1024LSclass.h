// this class wraps the calls to the public driver written by Warren J. Jasper
#ifndef USB1024LSCLASS_H
#define USB1024LSCLASS_H


#include <pthread.h>
#include "libusb/pmd.h"
#include "libusb/usb-1024LS.h"

class USB1024LS
{
public:
	USB1024LS(int serialNumber);
	~USB1024LS();
	
	bool deviceReady();
	void setSolenoid(int solenoid, bool on);	// just set solenoid on/off

private:
	::hid_device* hid;
	bool devicePresent;
};

#endif 		// USB1024LSCLASS_H

