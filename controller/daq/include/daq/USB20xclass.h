// this class wraps the calls to the public driver written by Warren J. Jasper
#ifndef USB20XCLASS_H
#define USB20XCLASS_H


#include "libusb/pmd.h"
#include "libusb/usb-20X.h"

class USB20x {
public:

	USB20x(int serialNum);
	~USB20x();
	
	enum bit{PORTAB0=0x1, PORTAB1=0x2, PORTAB2=0x4, PORTAB3=0x8, PORTAB4=0x10, PORTAB5=0x20, 			PORTAB6=0x40, PORTAB7=0x80};
	
	bool deviceReady();
	void setBit( bit whichBit, bool on);
	bool readBit( bit whichBit);

	void voltageOut(int channel, double volts);
	double voltageIn(int channel);
	void resetCTR();
	unsigned int readCTR();
private:
	::libusb_device_handle *udev;
	bool devicePresent;	
	int digPortBits;
	float table_AIN[NCHAN_USB20X][2];
};


#endif		// USB20XCLASS_H

