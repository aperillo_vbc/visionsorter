// implementation of DAQclass
#include <stdio.h>
#include <iostream>
#include "daq/DAQclass.h"


	DAQ::DAQ()
	{
		deviceInitialized = false;
		int ret = libusb_init(NULL);
		if (ret < 0){
 			printf("libusb init failed\n");
		}else{
//			ret = hid_init();
//			if (ret <0 )
//				printf("hid_init failed\n");
//			else {
//				mcc20x_1 = new USB20x(0x01DCADFF);	// standalone
				mcc20x_1 = new USB20x(0x01D202F6);	// Dave's board
				pcidio24H = new PCIDIO24H();
				deviceInitialized = mcc20x_1->deviceReady() &&
						    pcidio24H->deviceReady();
				if(!deviceInitialized) printf("Not all DAQ devices initialized!\n");
//			}
		}
	}

	DAQ::~DAQ()
	{
		delete mcc20x_1;
		delete pcidio24H;
		libusb_exit(NULL);
//		hid_exit();

	}

	bool DAQ::daqDeviceReady(){ return deviceInitialized; }
//
	// A IN
	double DAQ::getCabinetTemperature() {	// F? C?
		if(deviceInitialized){
			return mcc20x_1->voltageIn(1);
		}
	}

	double DAQ::get24VDC() {				// V  voltage divider returns 4.8 volts when 24V present
		if(deviceInitialized){
			return (mcc20x_1->voltageIn(6)*(24.0/5));
		}
	}

	double DAQ::get5VDC() {					// V
		if(deviceInitialized){
			return mcc20x_1->voltageIn(7);
		}
	}

	double DAQ::getCPAPressure() {				// psi 4.8 volts equals 100 psi
		if(deviceInitialized){
			return (mcc20x_1->voltageIn(0)*(100.0/5));
		}
	}

//	double DAQ::getConveyorSpeed(){		// V
//		if(deviceInitialized){
//			return mcc20x_1->voltageIn(4);
//		}
//	}

	double DAQ::getConveyorSpeed(){		// V
		return 0;
	}

	double DAQ::getInductive1(){
		if(deviceInitialized)
    			return mcc20x_1->voltageIn(3);
	}

    	double DAQ::getInductive2(){
		return mcc20x_1->voltageIn(5);
      	}

	bool DAQ::isDoorIlockSafe(){			// true means that the interlocks are in a state 								// safe for operation, i.e., closed
		if(deviceInitialized){
			return ((mcc20x_1->voltageIn(4) > 4.5)? true: false);
		}
		else
			return false;
	}

	bool DAQ::isOtherIlockSafe(){			// true means that the interlocks are in a state 								// safe for operation, i.e., closed
		if(deviceInitialized){
			return ((mcc20x_1->voltageIn(2) > 4.5)? true: false);
		}
		else
			return false;
	}

    	double DAQ::getLevelSensor(){
		return mcc20x_1->voltageIn(0);
      	}

	// A OUT
	void DAQ::setConveyorSpeed(double speed){	// V
		if(deviceInitialized) mcc20x_1->voltageOut(0, speed);
	}

    	// AOUT2 used to control HD-75 speed
	void DAQ::setHD75Speed(double speed){	// V
//		if(deviceInitialized) mcc20x_1->voltageOut(1, speed);
	}

	// A OUT
	void DAQ::setCreepConveyorSpeed(double speed){	// V
//		if(deviceInitialized) mcc20x_2->voltageOut(0, speed);
	}

	// A OUT
	void DAQ::setInclineConveyorSpeed(double speed){	// V
//		if(deviceInitialized) mcc20x_2->voltageOut(1, speed);
	}

	//enable relays after everything running-avoid glitches
	void DAQ::enableRelayPower(){
	}

	// D IN
 	bool DAQ::getInductive1_D(){
//		if(deviceInitialized)
//			return mcc202.readBit(USB20x::PORTAB6);
//		else
			return false;
	}

	bool DAQ::readBit(int port, int pin){
	if(deviceInitialized)
			return pcidio24H->readBit(port, pin);
		else
			return false;
	}

	bool DAQ::getInductive2_D(){
//		if(deviceInitialized)
//			return mcc202.readBit(USB20x::PORTAB7);
//		else
			return false;
	}

	bool DAQ::waitForLaserTrigger(){
		bool trigger=false;		
		do{
		  if(getInductive2() >= 4.0) trigger = true;		
		//	trigger = readBit(2, 7);
		// std::cout << "trigger: " << trigger << std::endl;
		}while(!trigger);
		return true;	
	}

	// D OUT
	void DAQ::setConveyor(bool on) {			// true is on, false is off
		if(deviceInitialized) mcc20x_1->setBit(USB20x::PORTAB0, on);
	}
	void DAQ::setAC(bool on) {					// true is on, false is off
		if(deviceInitialized) mcc20x_1->setBit(USB20x::PORTAB3, on);
	}

	void DAQ::setFeed(bool on) {				// true is on, false is off
		if(deviceInitialized){
			mcc20x_1->setBit(USB20x::PORTAB2, on);
			creepFeederOn = on;
			if(!creepFeederOn) setCreepFeeder(false);
		}
	}

	void DAQ::setAlarm(bool on) {				// true is on, false is off
		if(deviceInitialized) mcc20x_1->setBit(USB20x::PORTAB5, on);
	}

	void DAQ::setSpareContactor(bool on) {		// true is on, false is off
		if(deviceInitialized) mcc20x_1->setBit(USB20x::PORTAB6, on);
	}

	void DAQ::setVisionLamps(bool on) {			// true is on, false is off
		if(deviceInitialized) mcc20x_1->setBit(USB20x::PORTAB1, on);
	}

	void DAQ::setCreepFeeder(bool on) {			// true is on, false is off
		if(deviceInitialized) mcc20x_1->setBit(USB20x::PORTAB3, on);
	}

	void DAQ::fireSolenoid(int solenoid, double time) {} // solenoid number (1-24 are valid values), // seconds to keep the solenoid open

	 void DAQ::setSolenoid(int solenoid, bool on){
	 	if(deviceInitialized){
	 		pcidio24H->setSolenoid(solenoid, on);
	 	}
	 }




// 0-23
    	void DAQ::setSolenoidMod(int solenoid, bool on){	// this code make no sense, not sure if stephen added for some use
        	// int correctSolenoid = solenoid + 1;
        	int block = solenoid/8 + 1;
        	int correctSolenoid = block*8 - solenoid + (block-1)*8;
        	if(deviceInitialized) setSolenoid(correctSolenoid, on);
	}

// CTR
	void DAQ::resetCTR(int device){
		if(deviceInitialized){
			if (device == 1)
				mcc20x_1->resetCTR();
//			else
//				mcc20x_2->resetCTR();
		}
	}

	unsigned int  DAQ::readCTR(int device){
		if(deviceInitialized){
			if (device == 1)
				return mcc20x_1->readCTR();
//			else
//				return mcc20x_2->readCTR();
		}
	}
