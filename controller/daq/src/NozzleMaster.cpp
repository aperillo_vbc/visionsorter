#include <iostream>
#include <time.h>
#include <math.h>
#include "daq/NozzleMaster.h"
#include <cassert>

NozzleMaster::NozzleMaster(DAQPtr board, int numSolenoids):
_board(board),
_numSolenoids(numSolenoids)
{
    int mod = 8;
    // int cur = 0;
    // for(size_t j = 0; j < 3; ++j)
    // {
    //     mod = 8 * (j + 1);
    //     cur = mod - 8;
    //     for(int i = mod; i > cur; --i)
    //         whichNozzle.push_back(new nozzleThread(board, i));
    // }

    for(int i=0; i < _numSolenoids; i++)
    {
        whichNozzle.push_back(new nozzleThread(board, i));
        nozzleThreads.push_back(whichNozzle[i]->loopThread());
    }
}

NozzleMaster::~NozzleMaster()
{
	for(int i = 0; i < _numSolenoids; i++)
		whichNozzle[i]->stopLoop();

	for(int i = 0; i < _numSolenoids; i++)
		nozzleThreads[i].join();

	for(int i = 0; i < _numSolenoids; i++)
		delete whichNozzle[i];
}

void NozzleMaster::setSolenoid(int index, bool on){
    whichNozzle[index-1]->setSolenoid(on);
}

void NozzleMaster::nozzleFireTime(int nozzleIndex, const NozMsg &msg)
{
    // assert(nozzleIndex >= 1 && nozzleIndex <= 24);
    if(nozzleIndex < 1)
        nozzleIndex = 1;
    if(nozzleIndex > 24)
        nozzleIndex = 24;
    whichNozzle[nozzleIndex-1]->addFireTime(msg);
}
