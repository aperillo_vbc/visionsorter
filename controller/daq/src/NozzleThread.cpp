#include <iostream>
#include <time.h>
#include "daq/NozzleThread.h"

nozzleThread::nozzleThread(DAQPtr board, int solenoid):
_board(board),
_solenoid(solenoid)
{
	runFlag = true;
  _board->setSolenoid(_solenoid, false);
}

nozzleThread::~nozzleThread()
{
    _board->setSolenoid(_solenoid, false);
}

void * nozzleThread::runLoop(void *arg)
{
    while(runFlag)
    {
        std::unique_lock<std::mutex> lk(qLock);
        while(fireTimes.empty() && runFlag)
            q_cv.wait(lk);

        if(!runFlag)
            break;

        auto msg = fireTimes.front();
        fireTimes.pop_front();
        lk.unlock();
        auto now = std::chrono::system_clock::now();
        if(msg.time > now)
            std::this_thread::sleep_for(msg.time - now);

        _board->setSolenoid(_solenoid, true);		// fire the nozzle
        std::this_thread::sleep_for(msg.duration);
        _board->setSolenoid(_solenoid, false);
	}

	return nullptr;
}

void nozzleThread::stopLoop()
{
	runFlag = false;
  q_cv.notify_all();
}

std::thread nozzleThread::loopThread()
{
    return std::thread([=] { runLoop(NULL); });
}

void nozzleThread::addFireTime(const NozMsg &msg)
{
    // avoid duplicate entries for overlapped image
    if(msg.time > latestTime + latestDuration)
    {
        std::unique_lock<std::mutex> lk(qLock);
        latestTime = msg.time;
        latestDuration = msg.duration;
        fireTimes.push_back(msg);
        lk.unlock();
        q_cv.notify_one();
    }
}
