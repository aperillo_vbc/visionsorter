#include <stdio.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "daq/PCIDIO24Hclass.h"

PCIDIO24H::PCIDIO24H() {
	Board = 0;
	devicePresent = false;
	int portcheck = 0;
	int result;
        sprintf(DevNameIO, "/dev/dio24H/dio%1d_0A", Board);
	if ((fd_A = open(DevNameIO, O_RDWR )) < 0) {
	    perror(DevNameIO);
	    printf("error opening device %s\n", DevNameIO);
	    portcheck++;
	}
  	sprintf(DevNameIO, "/dev/dio24H/dio%1d_0B", Board);
  	if ((fd_B = open(DevNameIO, O_RDWR )) < 0) {
    	    perror(DevNameIO);
            printf("error opening device %s\n", DevNameIO);
	    portcheck++;
        }
        sprintf(DevNameIO, "/dev/dio24H/dio%1d_0C", Board);
        if ((fd_C = open(DevNameIO, O_RDWR )) < 0) {
            perror(DevNameIO);
            printf("error opening device %s\n", DevNameIO);
	    portcheck++;
        }

	if(portcheck ==0){			//configure stuff if device found
		devicePresent = true;
		ioctl(fd_A, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_B, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_C, DIO_SET_DIRECTION, PORT_OUTPUT);
        	unsigned short value = 255;
		result = write(fd_A, &value, 1);
		result = write(fd_B, &value, 1);
		result = write(fd_C, &value, 1);
		digPortABits = 255;
		digPortBBits = 255;
		digPortCBits = 255;
	}
}

PCIDIO24H::~PCIDIO24H() {
	if(devicePresent){
  		close(fd_A);
		close(fd_B);
	  	close(fd_C);
	}
}

bool PCIDIO24H::deviceReady(){
	return devicePresent;
}

unsigned short PCIDIO24H::setBit(int port, int pin, bool on) {
  	unsigned short value, whichBit;
	char *digPortBits;
	
	switch(port){
		case 0:
			digPortBits = &digPortABits;
			break;	
		case 1:
			digPortBits = &digPortBBits;
			break;	
		case 2:
			digPortBits = &digPortCBits;
		break;	
	}
	whichBit = (1 << pin);	
	if (on) {
		value = (~whichBit & *digPortBits);
	}
	else {
		value = (whichBit | *digPortBits);
	}
	*digPortBits = value;
	return value;
}

bool PCIDIO24H::readBit(int port, int pin){
	char value;
	char mask;
	int result;
	switch(port){
		case 0:
			result = read(fd_A, &value, 1);
			break;
		case 1:
      			result = read(fd_B, &value, 1);
			break;
		case 2:
			result = read(fd_C, &value, 1);
			break;
		default:
			break;
	}
	mask = 1 << pin;
	return (value & mask);
}

void PCIDIO24H::setSolenoid(int solenoid, bool on) 	// just set solenoid on/off
{
	int port;					// to nozzle layer
	int pin;
	int result;
	port = (solenoid-1)/8;
	pin = (solenoid-1)%8;
  	unsigned short value = setBit(port, pin, on);
	switch(port){
		case 0:
			result = write(fd_A, &value, 1);
			break;
		case 1:
      			result = write(fd_B, &value, 1);
			break;
		case 2:
			result = write(fd_C, &value, 1);
			break;
		default:
			break;
	}
}
