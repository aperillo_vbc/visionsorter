#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "daq/PCIDIO48Hclass.h"

PCIDIO48H::PCIDIO48H() {
	devicePresent = false;
	int portcheck = 0;
	int result;

		strcpy(DevNameIO, "/dev/dio48H/dio0_0A");
		if ((fd_0A = open(DevNameIO, O_RDWR)) < 0) {
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio48H/dio0_0B");
		if ((fd_0B = open(DevNameIO, O_RDWR)) < 0) {
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio48H/dio0_0C");
		if ((fd_0C = open(DevNameIO, O_RDWR)) < 0) {
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio48H/dio0_1A");
		if ((fd_1A = open(DevNameIO, O_RDWR)) < 0) {
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio48H/dio0_1B");
		if ((fd_1B = open(DevNameIO, O_RDWR)) < 0) {
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio48H/dio0_1C");
		if ((fd_1C = open(DevNameIO, O_RDWR)) < 0) {
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}

	if(portcheck ==0){			//configure stuff if device found
		devicePresent = true;
		ioctl(fd_0A, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_0B, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_0C, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_1A, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_1B, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_1C, DIO_SET_DIRECTION, PORT_OUTPUT);

		unsigned short value = 255;
		result = write(fd_0A, &value, 1);
		result = write(fd_0B, &value, 1);
		result = write(fd_0C, &value, 1);
		result = write(fd_1A, &value, 1);
		result = write(fd_1B, &value, 1);
		result = write(fd_1C, &value, 1);

		digPort0ABits = 255;
		digPort0BBits = 255;
		digPort0CBits = 255;
		digPort1ABits = 255;
		digPort1BBits = 255;
		digPort1CBits = 255;

	}
}

PCIDIO48H::~PCIDIO48H() {
	if(devicePresent){
  		close(fd_0A);
		close(fd_0B);
	  	close(fd_0C);
		close(fd_1A);
		close(fd_1B);
		close(fd_1C);

	}
}

bool PCIDIO48H::deviceReady(){
	return devicePresent;
}

unsigned short PCIDIO48H::setBit(int port, int pin, bool on) {
  	unsigned short value, whichBit;
	char *digPortBits;
	
	switch(port){
		case 0:
			digPortBits = &digPort0ABits;
			break;	
		case 1:
			digPortBits = &digPort0BBits;
			break;	
		case 2:
			digPortBits = &digPort0CBits;
			break;	
		case 3:
			digPortBits = &digPort1ABits;
			break;
		case 4:
			digPortBits = &digPort1BBits;
			break;
		case 5:
			digPortBits = &digPort1CBits;
			break;
		default:
			break;
	}
	whichBit = (1 << pin);	
	if (on) {
		value = (~whichBit & *digPortBits);
	}
	else {
		value = (whichBit | *digPortBits);
	}
	*digPortBits = value;
	return value;
}

bool PCIDIO48H::readBit(int port, int pin){
	return false;
}

void PCIDIO48H::setSolenoid(int solenoid, bool on) 	// just set solenoid on/off
{
	int port;					// to nozzle layer
	int pin;
	int result;
	port = (solenoid-1)/8;
	pin = (solenoid-1)%8;
  	unsigned short value = setBit(port, pin, on);
	switch(port){
		case 0:
        	result = write(fd_0A, &value, 1);
			break;
		case 1:
      		result = write(fd_0B, &value, 1);
			break;
		case 2:
			result = write(fd_0C, &value, 1);
			break;
		case 3:
			result = write(fd_1A, &value, 1);
			break;
		case 4:
			result = write(fd_1B, &value, 1);
			break;
		case 5:
			result = write(fd_1C, &value, 1);
			break;
		default:
			break;
	}
}
