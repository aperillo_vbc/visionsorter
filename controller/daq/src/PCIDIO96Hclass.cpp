#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "daq/PCIDIO96Hclass.h"

PCIDIO96H::PCIDIO96H() {
	devicePresent = false;
	int portcheck = 0;
	int result;

		strcpy(DevNameIO, "/dev/dio96H/dio0_0A");
		if ((fd_0A = open(DevNameIO, O_RDWR)) < 0) {
			perror("DevNameIO");
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_0B");
		if ((fd_0B = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_0C");
		if ((fd_0C = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_1A");
		if ((fd_1A = open(DevNameIO, O_RDWR)) < 0) {
			perror("DevNameIO");
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_1B");
		if ((fd_1B = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_1C");
		if ((fd_1C = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_2A");
		if ((fd_2A = open(DevNameIO, O_RDWR)) < 0) {
			perror("DevNameIO");
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_2B");
		if ((fd_2B = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_2C");
		if ((fd_2C = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_3A");
		if ((fd_3A = open(DevNameIO, O_RDWR)) < 0) {
			perror("DevNameIO");
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_3B");
		if ((fd_3B = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}
		strcpy(DevNameIO, "/dev/dio96H/dio0_3C");
		if ((fd_3C = open(DevNameIO, O_RDWR)) < 0) {
			perror(DevNameIO);
			printf("error opening device %s\n", DevNameIO);
			portcheck++;
		}

	if(portcheck ==0){			//configure stuff if device found
		devicePresent = true;
		ioctl(fd_0A, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_0B, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_0C, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_1A, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_1B, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_1C, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_2A, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_2B, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_2C, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_3A, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_3B, DIO_SET_DIRECTION, PORT_OUTPUT);
		ioctl(fd_3C, DIO_SET_DIRECTION, PORT_OUTPUT);

		unsigned short value = 255;
		result = write(fd_0A, &value, 1);
		result = write(fd_0B, &value, 1);
		result = write(fd_0C, &value, 1);
		result = write(fd_1A, &value, 1);
		result = write(fd_1B, &value, 1);
		result = write(fd_1C, &value, 1);
		result = write(fd_2A, &value, 1);
		result = write(fd_2B, &value, 1);
		result = write(fd_2C, &value, 1);
		result = write(fd_3A, &value, 1);
		result = write(fd_3B, &value, 1);
		result = write(fd_3C, &value, 1);

		digPort0ABits = 255;
		digPort0BBits = 255;
		digPort0CBits = 255;
		digPort1ABits = 255;
		digPort1BBits = 255;
		digPort1CBits = 255;
		digPort2ABits = 255;
		digPort2BBits = 255;
		digPort2CBits = 255;
		digPort3ABits = 255;
		digPort3BBits = 255;
		digPort3CBits = 255;
	}
}

PCIDIO96H::~PCIDIO96H() {
	if(devicePresent){
  		close(fd_0A);
		close(fd_0B);
	  	close(fd_0C);
		close(fd_1A);
		close(fd_1B);
		close(fd_1C);
		close(fd_2A);
		close(fd_2B);
		close(fd_2C);
		close(fd_3A);
		close(fd_3B);
		close(fd_3C);
	}
}

bool PCIDIO96H::deviceReady(){
	return devicePresent;
}

unsigned short PCIDIO96H::setBit(int port, int pin, bool on) {
  	unsigned short value, whichBit;
	char *digPortBits;
	
	switch(port){
		case 0:
			digPortBits = &digPort0ABits;
			break;	
		case 1:
			digPortBits = &digPort0BBits;
			break;	
		case 2:
			digPortBits = &digPort0CBits;
		    break;	
		case 3:
			digPortBits = &digPort1ABits;
			break;
		case 4:
			digPortBits = &digPort1BBits;
			break;
		case 5:
			digPortBits = &digPort1CBits;
			break;
		case 6:
			digPortBits = &digPort2ABits;
			break;
		case 7:
			digPortBits = &digPort2BBits;
			break;
		case 9:
			digPortBits = &digPort2CBits;
			break;
		case 9:
			digPortBits = &digPort3ABits;
			break;
		case 10:
			digPortBits = &digPort3BBits;
			break;
		case 11:
			digPortBits = &digPort3CBits;
			break;
		default:
			break;
	}
	whichBit = (1 << pin);	
	if (on) {
		value = (~whichBit & *digPortBits);
	}
	else {
		value = (whichBit | *digPortBits);
	}
	*digPortBits = value;
	return value;
}

bool PCIDIO96H::readBit(int port, int pin){
	return false;
}

void PCIDIO96H::setSolenoid(int solenoid, bool on) 	// just set solenoid on/off
{
	int port;					// to nozzle layer
	int pin;
	int result;
	port = (solenoid-1)/8;
	pin = (solenoid-1)%8;
  	unsigned short value = setBit(port, pin, on);
	switch(port){
		case 0:
        	result = write(fd_0A, &value, 1);
			break;
		case 1:
      		result = write(fd_0B, &value, 1);
			break;
		case 2:
			result = write(fd_0C, &value, 1);
			break;
		case 3:
			result = write(fd_1A, &value, 1);
			break;
		case 4:
			result = write(fd_1B, &value, 1);
			break;
		case 5:
			result = write(fd_1C, &value, 1);
			break;
		case 6:
			result = write(fd_2A, &value, 1);
			break;
		case 7:
			result = write(fd_2B, &value, 1);
			break;
		case 8:
			result = write(fd_2C, &value, 1);
			break;
		case 9:
			result = write(fd_3A, &value, 1);
			break;
		case 10:
			result = write(fd_3B, &value, 1);
			break;
		case 11:
			result = write(fd_3C, &value, 1);
			break;
		default:
			break;
	}
}
