// DAQVision1.cpp : Defines the entry point for the console application
#include <time.h>
#include <stdio.h>
#include "daq/DAQclass.h"
#include "daq/NozzleMaster.h"

int toContinue()
{
  int answer;
  answer = 0; //answer = getchar();
  printf("Continue [yY]? ");
  while((answer = getchar()) == '\0' ||
    answer == '\n');
  return ( answer == 'y' || answer == 'Y');
}


int main()
{
	double DC_5v;
	clock_t t;
	double timeTaken;
	std::chrono::system_clock::time_point fireTime;
	bool digBit;

	printf("create DAQ class\n");
	DAQPtr myDAQ = DAQPtr(new DAQ());
	::toContinue();

	printf("Test out the nozzle class\n");
	NozzleMaster *myNozzle = new NozzleMaster(myDAQ, 24);
	printf("created NozzleMaster\n");
	::toContinue();

  NozMsg msg;
  msg.time = std::chrono::system_clock::now() + std::chrono::seconds(5);
  msg.duration = std::chrono::milliseconds(500);
	myNozzle->nozzleFireTime(0, msg);
	printf("called nozzleFireTime on solenoid 0\n");

	msg.time = std::chrono::system_clock::now() + std::chrono::seconds(10);
	myNozzle->nozzleFireTime(3, msg);
	printf("called nozzleFireTime on solenoid 3\n");

	printf("Is DAQ device ready: %s\n", (myDAQ->daqDeviceReady()?"true":"false"));
	if(myDAQ->daqDeviceReady()){
		printf("set conveyor true\n");
		myDAQ->setConveyor(true);
		::toContinue();

		printf("setting conveyor false\n");
		myDAQ->setConveyor(false);
		::toContinue();

		printf("setting conveyor speed volts: 5\n");
		myDAQ->setConveyorSpeed(5.0);
		::toContinue();

		printf("setting conveyor speed volts: 0\n");
		myDAQ->setConveyorSpeed(0.0);
		::toContinue();

		printf("enable Relay power:\n");
		myDAQ->enableRelayPower();
		::toContinue();

		DC_5v = myDAQ->get5VDC();
		printf("getting 5V DC volts: %f\n", DC_5v);
		::toContinue();

		digBit = myDAQ->getInductive1_D();
		printf("getting dig Inductor1: %d\n", digBit);
		::toContinue();

		digBit = myDAQ->getInductive2_D();
		printf("getting dig Inductor1: %d\n", digBit);
		::toContinue();

		digBit = myDAQ->isDoorIlockSafe();
		printf("getting door interlock: %d\n", digBit);
		::toContinue();

		digBit = myDAQ->isOtherIlockSafe();
		printf("getting Other interlock: %d\n", digBit);
		::toContinue();
	}

	delete myNozzle;
	

	return 0;
}

