#include <stdio.h>
#include "daq/USB1024LSclass.h"
#include <thread>
#include <stdlib.h>


USB1024LS::USB1024LS(int serialNumber) {
	devicePresent = false;
	char buffer[9];
	sprintf(buffer, "%08X", serialNumber);	
	printf("serial num: %s\n", buffer);

	size_t size = 9;  
    	wchar_t* serNumber = new wchar_t[size]; 
    	size_t outSize;
    	mbstowcs(serNumber, buffer, size);

    	if((hid = hid_open(MCC_VID, USB1024LS_PID, serNumber)) > 0){
          	devicePresent = true;
		printf("MCC-1024LS found\n");      		
	}
 	else if ((hid = hid_open(MCC_VID, USB1024HLS_PID,serNumber)) > 0) {
		devicePresent = true;    		
		printf("USB 1024HLS Device is found!\n");
	}
	

	if(devicePresent){

    // usbDOut_USB1024LS(hid, DIO_PORTA, 0xff);			// set lines high
    // usbDOut_USB1024LS(hid, DIO_PORTA, 1);			// set lines high
    // usbDOut_USB1024LS(hid, DIO_PORTB, 1);
    // usbDOut_USB1024LS(hid, DIO_PORTC_LOW, 0x0f);
    // // usbDOut_USB1024LS(hid, DIO_PORTC_HI, 0xf0);
    // usbDOut_USB1024LS(hid, DIO_PORTC_HI, 0x0f);


    // std::this_thread::sleep_for(std::chrono::seconds(5));

    usbDConfigPort_USB1024LS(hid, DIO_PORTA, DIO_DIR_OUT);	//set all to outputs
    usbDConfigPort_USB1024LS(hid, DIO_PORTB, DIO_DIR_OUT);
    usbDConfigPort_USB1024LS(hid, DIO_PORTC_LOW, DIO_DIR_OUT);
    usbDConfigPort_USB1024LS(hid, DIO_PORTC_HI, DIO_DIR_OUT);

    usbDOut_USB1024LS(hid, DIO_PORTA, 0xff);			// set lines high
    usbDOut_USB1024LS(hid, DIO_PORTB, 0xff);
    usbDOut_USB1024LS(hid, DIO_PORTC_LOW, 0x0f);
    usbDOut_USB1024LS(hid, DIO_PORTC_HI, 0x0f);

    // std::this_thread::sleep_for(std::chrono::seconds(5));

    // usbDOut_USB1024LS(hid, DIO_PORTC_HI, 0xf0);
	}
}

USB1024LS::~USB1024LS() {
	if(devicePresent){
    // usbReset_USB1024LS(hid);
		hid_close(hid);
//      		hid_exit();
	}
}

bool USB1024LS::deviceReady() {return devicePresent;}

void USB1024LS::setSolenoid(int solenoid, bool on){	// just set solenoid on/off, move the sleep
	int port;					// to nozzle layer
	int pin;
	port = (solenoid-1)/8;
	pin = (solenoid-1)%8;
	switch(port){
		case 0:
			usbDBitOut_USB1024LS(hid, DIO_PORTA, pin, on ? 0 : 1);
			break;
		case 1:
			usbDBitOut_USB1024LS(hid, DIO_PORTB, pin, on ? 0 : 1);
			break;
		case 2:
			if(pin < 4)
				usbDBitOut_USB1024LS(hid, DIO_PORTC_LOW, pin, on ? 0 : 1);
			else{
				usbDBitOut_USB1024LS(hid, DIO_PORTC_HI, pin, on ? 0 : 1);
			}
			break;
		default:
			break;
	}
}
