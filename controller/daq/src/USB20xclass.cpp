#include <stdio.h>
#include <math.h>
#include "daq/USB20xclass.h"
// this file has been modified to work with MCC-205 board

USB20x::USB20x(int serialNumber) {
	devicePresent = false;
	char buffer[9];
	sprintf(buffer, "%08X", serialNumber);	
	printf("serial num: %s\n", buffer);

	if((udev = usb_device_find_USB_MCC(USB202_PID, buffer))){
		devicePresent = true;
		printf("MCC-202 found\n");		
	}
	else if((udev = usb_device_find_USB_MCC(USB205_PID, buffer))){
		devicePresent = true;
		printf("MCC-205 found\n");		
	}

	if(devicePresent){			//configure stuff if device found
//		if(serialNumber == 0x01CE1C91){
//			usbDTristateW_USB20X(udev,0xFF); // all bits input
//			digPortBits = 0x00;
//			usbDLatchW_USB20X(udev, (uint8_t)digPortBits); // set inputs low
//		}
//		else if(serialNumber == 0x01CB5434){
			usbDTristateW_USB20X(udev,0x00); // bits 0-7 output bits
			digPortBits = 0xff;
			usbDLatchW_USB20X(udev, (uint8_t)digPortBits); // set output high
//		}

	usbBuildGainTable_USB20X(udev, table_AIN); // get AI calibration table
	}
}

USB20x::~USB20x() {
	if(devicePresent){
		usbAOut_USB20X(udev, 1, 0);	// disable power to relays
 		cleanup_USB20X(udev);
	}
}

bool USB20x::deviceReady(){
	return devicePresent;
}

void USB20x::setBit(bit whichBit, bool on) {
	int temp;	
	if (on) {
		temp = (~whichBit & digPortBits);
	}
	else {
		temp = (whichBit | digPortBits);
	}
	digPortBits = temp;
	usbDLatchW_USB20X(udev, (uint8_t)temp);
}

bool USB20x::readBit(bit whichBit){
        uint8_t input = usbDPort_USB20X(udev);
	return (input & whichBit);
}

void USB20x::voltageOut(int channel, double volts){
  	uint16_t value;
	value = (volts/5.0)*4095;
//	printf("volts = %f  value = %d\n\n", volts, value);	
	usbAOut_USB20X(udev, channel, value);
}

double USB20x::voltageIn(int channel){
	uint16_t value;
	value = usbAIn_USB20X(udev, channel);
	value = rint(value*table_AIN[channel][0] + table_AIN[channel][1]);
	return volts_USB20X(value);
}
	
void USB20x::resetCTR(){
	usbCounterInit_USB20X(udev);
}

unsigned int USB20x::readCTR(){
	usbCounter_USB20X(udev);
}

