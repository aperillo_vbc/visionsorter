#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "model/Model.h"
#include "../daq/include/daq/DAQclass.h"
#include "camera/Camera.h"
#include "classifier/Classification.h"
#include <chrono>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>

/* #define FTMIN_TO_INUSEC 12./(60*1e6) */
static const double FTMIN_TO_INUSEC = 12./(60.*1e6);

class Controller;

typedef std::shared_ptr<Controller> ControllerPtr;

class Controller{
public:
    Controller(ModelPtr model, DAQPtr daq, std::vector<baslerCam::CameraPtr> cameras);
    ~Controller();

    template <typename T>
    bool inBounds(const std::string &label)
    {
        T cval = model_->getCurrentValue<T>(label);
        if(cval >= model_->getLowerBound<T>(label)
           && cval <= model_->getUpperBound<T>(label))
	// if(cval <= model_->getLowerBound<T>(label)
        //   && cval <= model_->getUpperBound<T>(label))
            return true;
        return false;
    }

    template <typename T>
    bool isOn(const std::string &label)
    {
        if(model_->getCurrentValue<T>(label))
            return true;
        return false;
    }

    inline ModelPtr getModel(){return model_;}
    // inline NozzleMasterPtr getNozzleMaster(){return noz_;}
    inline DAQPtr getDAQ(){return daq_;}

    double getConveyorSpeed();
    inline double getConveyorSpeedInuSec(){
        return getConveyorSpeed() * FTMIN_TO_INUSEC;
    }
    void setConveyorSpeed(double speed);
    void setCreepConveyorSpeed(double speed);
    void setInclineConveyorSpeed(double speed);
    void setHD75Speed(double speed);

    bool startVisionLoop(std::function<void(cv::Mat, std::string, bool)> callbackVision);
    void stopVisionLoop();

    inline void setConveyor(bool on){daq_->setConveyor(on);}
    inline void setAC(bool on){daq_->setAC(on);}
    inline void setFeed(bool on){daq_->setFeed(on);}
    inline void setAlarm(bool on){daq_->setAlarm(on);}
    inline void setSpareContactor(bool on){daq_->setSpareContactor(on);}
    inline void setVisionLights(bool on){daq_->setVisionLamps(on);}
    inline void enableRelayPower(){
        daq_->enableRelayPower();
        daq_->setSpareContactor(true);		// true is on, false is off
    }

    inline void setDebugging(bool on){debugging_ = on;}
    inline void setTerminationCondition(bool tc){tc_=tc;}
    inline baslerCam::CameraPtr getCamera(int position){
        if(cameras_.size()==0)
            return nullptr;
        for(auto &cam : cameras_)
            if(model_->getCamSet<int>(cam->getID(), "position") == position)
                return cam;
        return nullptr;
    }
    inline std::vector<baslerCam::CameraPtr> &getCameras(){return cameras_;}
    inline cv::Mat & getSegmentedImage(){return segmentedImage_;}
    void findOrigin(const baslerCam::CameraPtr &cam, cv::Mat &img);
    void stopCalibrateCamera();
    void calibrateBeltSpeed(std::ostream &out, const std::string &speedEst);
    void startCalibrateCamera(const baslerCam::CameraPtr &cam, int boardWidth, int boardHeight, double board_size_x, double board_size_y, std::function<void(cv::Mat)> callback);
    void calibrateCamera(const baslerCam::CameraPtr &cam, int boardWidth, int boardHeight, double board_size_x, double board_size_y, std::function<void(cv::Mat)> callback);
    inline void setSolenoid(int i, bool on){
        daq_->setSolenoid(i, on);
    }
    cv::Mat getPictureBGR(int position);
    cv::Mat getPictureBGR(baslerCam::CameraPtr cam);
    cv::Mat getPictureRGB(int position);
    cv::Mat getPictureRGB(baslerCam::CameraPtr cam);
    bool waitForLaserTrigger(void);
 
protected:
    ModelPtr model_;
    DAQPtr daq_;
    std::vector<baslerCam::CameraPtr> cameras_;
    bool classifier_initialized;
    std::chrono::microseconds checkStatusRate_;

    // loops and exit triggers
    void statusLoop();
    void beltSpeedSensor1Loop();
    void beltSpeedSensor2Loop();
    void nozzleSchedulerLoop();
    std::thread statusLoopThread;
    std::thread visionLoopThread;
    std::thread nozzleSchedulerLoopThread;
    std::thread calibrationThread;
    bool debugging_;
    bool statusLoop_;
    bool visionLoop_;
    bool calibrationLoop_;
    bool nozzleSchedulerLoop_;
    bool tc_;
    std::string highResImageFile_;
    bool beltSpeedSensor1Loop_;
    bool beltSpeedSensor2Loop_;
    std::thread beltSpeedSensor1Thread;
    std::thread beltSpeedSensor2Thread;
    bool levelSensorLoop_;
    std::thread levelSensorThread;
    void levelSensorLoop();

    // Nozzle Scheduling parameters
    std::queue<Classifications> classQ_;
    std::mutex nozSchedLock_;
    std::condition_variable nozSched_cv;

    // cv parameters
    cv::Mat segmentedImage_;
    cv::Mat camIntrinsic_, camDistCoeffs_;
    cv::Mat map1_, map2_;
};

#endif
