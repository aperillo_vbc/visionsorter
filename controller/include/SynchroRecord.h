#include <thread>
#include <condition_variable>
#include <mutex>
#include <vector>
#include "Controller.h"
#include "model/Model.h"
#include "camera/Camera.h"
#include <chrono>
#include <deque>
#include <cstdlib>

class SynchroRecord{
public:
    SynchroRecord(ControllerPtr controller, const std::vector<baslerCam::CameraPtr> cams, std::function<void(int, cv::Mat)> callback);
    ~SynchroRecord();
    void feedLoop(int index, baslerCam::CameraPtr cam, std::function<void(int, cv::Mat)> callback);
    void timer();
private:
    ControllerPtr controller_;
    ModelPtr model_;
    std::deque<std::condition_variable> capture_cvs;
    std::deque<std::mutex> mutexes;
    std::vector<std::thread> feeds;
    std::thread timerLoop;
    bool feed_;
    std::vector<bool> capture_image;
};
