#ifndef VISION_SYSTEM_H
#define VISION_SYSTEM_H

#include "Controller.h"
#include "model/Model.h"
#include "daq/NozzleMaster.h"
#include "daq/DAQclass.h"
#include "camera/Camera.h"
#include "camera/AstraCam.h"
#include "classifier/Classification.h"
#include <chrono>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>

typedef std::chrono::system_clock::time_point TimePt;

class Segmenter;
class ImageBatcher;
class VisionSystem;

typedef std::shared_ptr<Segmenter> SegmenterPtr;
typedef std::shared_ptr<ImageBatcher> ImageBatcherPtr;
typedef std::shared_ptr<VisionSystem> VisionSystemPtr;

struct NozAct{
    std::chrono::system_clock::time_point on;
    std::chrono::system_clock::time_point off;
//    std::chrono::system_clock::time_point on_stagger;	//used for system with staggered nozzles
//    std::chrono::system_clock::time_point off_stagger;
    std::vector<int> sols;
};

enum VS_STATE {
    VS_RUN = 0,
    VS_STANDBY = 1,
    VS_INIT = 2,
    VS_PAUSE = 3
};

#ifdef Q_DECLARE_METATYPE
Q_DECLARE_METATYPE(VS_STATE);
#endif

struct SegMsg{
    SegMsg(cv::Mat _img, cv::Mat _img_boxes, TimePt _timePt, std::vector<SubImg> _subImgs) :
        img(_img), img_boxes(_img_boxes), timePt(_timePt), subImgs(_subImgs)
    {}
    cv::Mat img, img_boxes;
    TimePt timePt;
    std::vector<SubImg> subImgs;
};

class Segmenter{
public:
    Segmenter(ControllerPtr controller, baslerCam::CameraPtr cam = nullptr, int  = 0);
    ~Segmenter();
    void startLoops(std::function<void(const SegMsg &)> queueCall);
    void stopLoops();
    void getMeanValues(cv::Mat &h_img, cv::Mat &h_img_mn);
    void threshold(cv::Mat &h_img, cv::Mat &h_img_thresh, bool bgr = true);
    void threshold(cv::cuda::GpuMat &d_img, cv::cuda::GpuMat &d_img_thresh, bool bgr = true);
    void dilate(cv::Mat &h_img, cv::Mat &h_img_dil);
    void dilate(cv::cuda::GpuMat &d_img_thresh, cv::cuda::GpuMat &d_img_dilate);
    void findContours(cv::cuda::GpuMat &d_img_thresh, std::vector<cv::Rect2i> &pieces, cv::Mat &h_img);
    void findContours(cv::Mat &h_img_bw, std::vector<cv::Rect2i> &pieces, cv::Mat &h_img);
    void crop(cv::cuda::GpuMat &d_img, int height, int width);
    cv::Rect2i cropROI(cv::cuda::GpuMat &img, int height, int width);
    void overlayFrameCard(cv::cuda::GpuMat &d_piece, cv::cuda::GpuMat &d_dest, const cv::cuda::GpuMat &global_img, const cv::Rect2i &location);
    void segment(cv::cuda::GpuMat &d_img, std::vector<cv::Rect2i> &pieces, cv::Mat &h_img, bool bgr = true);
    void resumeLoops();
    void pauseLoops();
    inline void setRecord(bool record){
        record_ = record;
    }
    inline bool isPaused(){return pause_;}
    inline baslerCam::CameraPtr getCam(){return cam_;}
    inline void setCam(baslerCam::CameraPtr cam){
        cam_ = cam;
        if(cam == nullptr)
            camSID="0";
        else
            camSID=cam->getID();
    }

    std::string getTriggerTimeString(TimePt timePt, const std::string prefix);

protected:
    void imageFeed();
    void segmentLoop(std::function<void(const SegMsg &)> queueCall);
    void pushImage(cv::Mat img, const TimePt &timePt);
    std::pair<cv::Mat, TimePt> popImage();

    void waitOnSegmentationWarmUp();
    void setSegmentationWarmedUp();

    ControllerPtr controller_;
    ModelPtr model_;
    baslerCam::CameraPtr cam_;

    std::thread segLoopThread;
    std::thread imgFeedThread;

    std::string camSID;
    std::mutex captureLock;
    std::mutex segWarmLock;
    std::mutex pauseLock;
    std::mutex finishedLock;
    std::condition_variable capture_cv;
    std::condition_variable segWarm_cv;
    std::condition_variable pause_cv;

    std::queue<cv::Mat> imageQueue;
    std::queue<TimePt> timeQueue;
    double scale_factor_;
    bool segWarmedUp_;
    bool imageFeed_;
    cv::Size img_rect_size_;
    bool pause_;
    bool record_;
    int whichSeg_;
};

class ImageBatcher{
public:
    ImageBatcher(ControllerPtr controller, const std::vector<baslerCam::CameraPtr> &cameras, size_t _batchSize = 32);
    ~ImageBatcher();
    void startLoops();
    void stopLoops();
    void pauseLoops();
    void resumeLoops();
    void popSegMsg(std::vector<SubImg> &subImgs);
    void pushSegMsg(const SegMsg &msg);
    inline bool isPaused(){return pause_;}
    inline void setRecord(bool record){
        for (auto &segmenter : segmenters)
            segmenter->setRecord(record);
    }
protected:
    ControllerPtr controller_;
    ModelPtr model_;
    size_t batch_size;
    std::vector<std::unique_ptr<Segmenter>> segmenters;
    std::mutex segLock;
    std::condition_variable segLock_cv;
    std::queue<SubImg> segQueue;
    bool batching_;
    bool pause_;
};

class VisionSystem{
public:
    VisionSystem(ControllerPtr controller);
    ~VisionSystem();
    void startLoops(std::function<void(cv::Mat, std::string, VS_STATE)> callback);
    void stopLoops();
    inline bool isClassifierInit(){return classifier_initialized;}
    /* cv::Mat labelImage(cv::Mat img, bool isbmp = true); */
    std::pair<cv::Mat, int> labelImage(cv::Mat img, bool isbmp);
    static cv::Mat jpegEncodeDecode(cv::Mat img);

    void v_init_classifier();
    void v_uninit_classifier();
    void v_classify(cv::cuda::GpuMat &d_img, std::vector<cv::Rect> &pieces, Classifications &classes);
    void resumeLoops();
    void pauseLoops();
    inline bool isPaused(){return pause_;}
    inline void setRecord(bool record){imgBatcher.setRecord(record);}
protected:
    void pushSubImages(const std::vector<SubImg> &subImgs);
    void popSubImages(std::vector<SubImg> &subImgs);
    void convertToDistance(SubImg &subimg);

    void waitOnClassWarmup();
    void setClassWarmedUp();
    void classLoop(std::function<void(cv::Mat, std::string, VS_STATE)> callback);
    void nozzleSchedLoop();

    void nozLoop();
    void popNozAct();
    void pushNozAct(const std::vector<NozAct> &msgs);

    ControllerPtr controller_;
    ModelPtr model_;
    NozzleMasterPtr noz_;
    DAQPtr daq_;
    bool warmedUp;
    ImageBatcher imgBatcher;
    bool classifier_initialized;
    bool classLoop_;
    bool nozzleSchedLoop_;

    std::thread nozLoopThread;
    std::thread nozzleSchedThread;
    std::thread classLoopThread;

    std::mutex warmLock;
    std::mutex classLock;
    std::mutex nozLock;
    std::condition_variable warm_cv;
    std::condition_variable class_cv;
    std::queue<SubImg> classQueue;
    std::queue<NozAct> nozActQueue;
    bool pause_;

    std::vector<std::queue<std::chrono::system_clock::time_point>> onTimes;
    std::vector<std::queue<std::chrono::system_clock::time_point>> offTimes;
    std::vector<int> onCounts;
};

typedef std::shared_ptr<VisionSystem> VisionSystemPtr;

#endif
