#ifndef MODEL_H
#define MODEL_H

/* #include "Camera.h" */
#include <memory>
#include <string>
#include <map>
#include <vector>
#include <atomic>
#include <cassert>

class Model;

typedef std::shared_ptr<Model> ModelPtr;

class SettingInterface
{
public:
    virtual ~SettingInterface() = default;
};

template <typename T>
class Setting : public SettingInterface {
public:
    Setting(){};
    Setting(T lbound, T ubound, T sp, T cval) :
        lowerBound_(lbound), upperBound_(ubound),
        setPoint_(sp), currentVal_(cval)
    {}
    T getLowerBound() const {
        return lowerBound_.load(std::memory_order_relaxed);
    }
    T getUpperBound() const {
        return upperBound_.load(std::memory_order_relaxed);
    }
    void setBounds(T lbound, T ubound)
    {
        lowerBound_.store(lbound, std::memory_order_relaxed);
        upperBound_.store(ubound, std::memory_order_relaxed);
    }
    T getSetPoint() const {
        return setPoint_.load(std::memory_order_relaxed);
    }
    void setSetPoint(T setPoint){
        setPoint_.store(setPoint, std::memory_order_relaxed);
    }
    void setCurrentValue(T cval){
        currentVal_.store(cval, std::memory_order_relaxed);
    }
    T getCurrentValue() const {
        return currentVal_.load(std::memory_order_relaxed);
    }
protected:
    std::atomic<T> lowerBound_, upperBound_, setPoint_, currentVal_;
};

typedef std::unique_ptr<SettingInterface> SettingPtr;

class Model{
public:
    /* setCam(int index, Camera cam); */
    template <typename T>
    void addSetting(const std::string &label){
        settings.insert(std::make_pair(label, SettingPtr(new Setting<T>)));
    }

    void addSetting(const std::string &label, SettingPtr setting){
        settings[label] = std::move(setting);
    }

    void setMeta(const std::string &label, const std::string &value){
        meta[label] = value;
    }
    std::string getMeta(const std::string &label){return meta[label];}

    template <typename T>
    void setBounds(const std::string &label, T lowerBound, T upperBound){
        if(!settings.count(label))
            settings.insert(std::make_pair(label, SettingPtr(new Setting<T>)));
        Setting<T> *type = dynamic_cast<Setting<T>*>(settings[label].get());
        assert(type != nullptr && "cast to the wrong type");
        type->setBounds(lowerBound, upperBound);
    }

    template <typename T>
    void setSetPoint(const std::string &label, T setPoint){
        if(!settings.count(label))
            settings.insert(std::make_pair(label, SettingPtr(new Setting<T>)));
        Setting<T> *type = dynamic_cast<Setting<T>*>(settings[label].get());
        assert(type != nullptr && "cast to the wrong type");
        type->setSetPoint(setPoint);
    }

    template <typename T>
    T getLowerBound(const std::string &label){
        if(!settings.count(label))
            throw std::invalid_argument(label + " does not exist");
        Setting<T> *type = dynamic_cast<Setting<T>*>(settings[label].get());
        assert(type != nullptr && "cast to the wrong type");
        return type->getLowerBound();
    }

    template <typename T>
    T getUpperBound(const std::string &label){
        if(!settings.count(label))
            throw std::invalid_argument(label + " does not exist");
        Setting<T> *type = dynamic_cast<Setting<T>*>(settings[label].get());
        assert(type != nullptr && "cast to the wrong type");
        return type->getUpperBound();
    }

    template <typename T>
    T getSetPoint(const std::string &label){
        if(!settings.count(label))
            throw std::invalid_argument(label + " does not exist");
        Setting<T> *type = dynamic_cast<Setting<T>*>(settings[label].get());
        assert(type != nullptr && "cast to the wrong type");
        return type->getSetPoint();
    }

    template <typename T>
    T getCurrentValue(const std::string &label){
        if(!settings.count(label))
            throw std::invalid_argument(label + " does not exist");
        Setting<T> *type = dynamic_cast<Setting<T>*>(settings[label].get());
        assert(type != nullptr && "cast to the wrong type");
        return type->getCurrentValue();
    }

    template <typename T>
    void setCurrentValue(const std::string &label, T currentValue){
        if(!settings.count(label))
            settings.insert(std::make_pair(label, SettingPtr(new Setting<T>)));
        Setting<T> *type = dynamic_cast<Setting<T>*>(settings[label].get());
        assert(type != nullptr && "cast to the wrong type");
        type->setCurrentValue(currentValue);
    }

    void clearList(const std::string &key){lists[key].clear();}
    void addToList(const std::string &key, const std::string &value){
        lists[key].push_back(value);
    }
    const std::vector<std::string> &getList(const std::string &key){
        return lists[key];
    }

    // void setCamSID(int id, const std::string &sid)
    // {
    //     camSID[id] = sid;
    // }
    // std::string getCamSID(int id)
    // {
    //     if(!camSID[id])
    //         throw std::invalid_argument(std::string("cam id ") + std::to_string(int) + " does not exist");
    //     return camSID[id];
    // }
    std::vector<float> getCamRectify(const std::string &id, const std::string &key){
        return camRectify[id][key];
    }
    void setCamRectify(const std::string &id, const std::string &key, const std::vector<float> &vec){
        if(!camRectify.count(id))
            camRectify[id] = std::map<std::string, std::vector<float>>();
        if(!camRectify[id].count(key))
            camRectify[id][key] = std::vector<float>();
        camRectify[id][key] = vec;
    }

    template <typename T>
    void setCamSet(const std::string &id, const std::string &key, T setPoint){
        if(!camSettings.count(id))
            camSettings.insert(std::make_pair(id, std::map<std::string, SettingPtr>()));
        if(!camSettings[id].count(key))
            camSettings[id].insert(std::make_pair(key, SettingPtr(new Setting<T>)));
        auto &camSet = camSettings[id];
        Setting<T> *type = dynamic_cast<Setting<T>*>(camSet[key].get());
        assert(type != nullptr && "cast to the wrong type");
        type->setSetPoint(setPoint);
    }

    template <typename T>
    T getCamSet(const std::string &id, const std::string &key){
        if(!camSettings.count(id))
            throw std::invalid_argument(std::string("cam id ") + id + " does not exist in camera settings.");
        if(!camSettings[id].count(key))
            throw std::invalid_argument(std::string("For cam id ") + id + std::string(", the key ") + key + " does not exist.");
        auto &camSet = camSettings[id];
        Setting<T> *type = dynamic_cast<Setting<T>*>(camSet[key].get());
        assert(type != nullptr && "cast to the wrong type");
        return type->getSetPoint();
    }

    inline const std::map<std::string, std::map<std::string, SettingPtr>> & getCamSettings()
    {
        return camSettings;
    }
    inline bool hasCamID(const std::string &sid){return camSettings.count(sid);}

    const std::vector<std::pair<int, double>> & getSolenoids(const std::string &id){
        if(!solenoidSettings.count(id))
            throw std::invalid_argument(std::string("cam id ") + id + " does not exist in solenoid settings.");
        return solenoidSettings[id];
    }
    void setSolenoids(const std::string &id, const std::vector<std::pair<int, double>> &solenoids){
        if(!solenoidSettings.count(id))
            solenoidSettings.insert(std::make_pair(id, std::vector<std::pair<int, double>>()));
        solenoidSettings[id] = solenoids;
    }

    void setSolSet(const std::string &id, size_t index, int solID, double width){
        if(!solenoidSettings.count(id))
            solenoidSettings.insert(std::make_pair(id, std::vector<std::pair<int, double>>()));
        if(solenoidSettings[id].size() < index)
            throw std::invalid_argument(std::string("For cam id ") + id + std::string(", the solenoid index ") + std::to_string(index) + " does not exist, index must either exist or be one greater than current settings size.");
        if(index >= solenoidSettings[id].size())
            solenoidSettings[id].push_back(std::make_pair(solID, width));
        else
            solenoidSettings[id][index] = std::make_pair(solID, width);
    }

    void addSolSet(const std::string &id, int solID, double width){
        if(!solenoidSettings.count(id))
            solenoidSettings.insert(std::make_pair(id, std::vector<std::pair<int, double>>()));
        solenoidSettings[id].push_back(std::make_pair(solID, width));
    }

    const std::pair<int, double> &getSolSet(const std::string &id, size_t index){
        if(!solenoidSettings.count(id))
            throw std::invalid_argument(std::string("cam id ") + id + " does not exist in solenoid settings.");
        if(solenoidSettings[id].size() <= index)
            throw std::invalid_argument(std::string("For cam id ") + id + std::string(", the solenoid index ") + std::to_string(index) + " does not exist");
        return solenoidSettings[id][index];
    }
protected:
    // CameraPtr cam1, cam2;
    std::map<std::string, SettingPtr> settings;
    std::map<std::string, std::string> meta;
    std::map<std::string, std::map<std::string, SettingPtr>> camSettings;
    // first is index number, second is width
    std::map<std::string, std::vector<std::pair<int, double>>> solenoidSettings;
    std::map<std::string, std::map<std::string, std::vector<float>>> camRectify;
    // std::map<int, std::string> camSID;
    std::map<std::string, std::vector<std::string>> lists;
};

#endif
