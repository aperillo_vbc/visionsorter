#include <iostream>
#include "Model.h"
#include <thread>
// #include <unistd.h>

void write(Model &c)
{
    for(double i = 0; i < 100; ++i)
    {
        c.setSetPoint<double>("a1", i);
        // usleep(100*1e3);
    }
}

void read(Model &c)
{
    for(size_t i = 0; i < 100; ++i)
        std::cout << c.getSetPoint<double>("a1") << std::endl;
}

int main()
{
    SettingPtr a2 = SettingPtr(new Setting<int>);

    Model c;
    c.addSetting<double>("a1");
    c.addSetting("a2", std::move(a2));
    c.setBounds<double>("a1", 4.5, 5.5);
    c.setBounds<int>("a2", 3, 5);
    // const SettingPtr &sp = b.getSetting("a");
    double upper = c.getUpperBound<double>("a1");
    double lower = c.getLowerBound<int>("a2");
    std::cout << upper << std::endl;
    std::cout << lower << std::endl;

    std::cout << "--------------------" << std::endl;

    std::thread reader(read, std::ref(c));
    std::thread writer(write, std::ref(c));

    reader.join();
    writer.join();

    return 0;
}
