#include "stdio.h"
#include "Controller.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <future>

#if CV_MAJOR_VERSION >= 3
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/highgui/highgui_c.h>
#endif

#define MIN_AREA 50*50
#define MAX_AREA 5*100*5*100
#define BELT_LENGTH 24.5

typedef std::chrono::system_clock::time_point TimePt;

Controller::Controller(ModelPtr model, DAQPtr daq, std::vector<baslerCam::CameraPtr> cameras) :
    model_(model), daq_(daq),
    cameras_(cameras)
{
    checkStatusRate_ = std::chrono::microseconds(500000); // rate at which to check status
    statusLoopThread = std::thread(&Controller::statusLoop, this);
    statusLoop_ = true;
    visionLoop_ = false;
    beltSpeedSensor1Loop_ = true;
//    beltSpeedSensor2Loop_ = true;
    beltSpeedSensor1Thread = std::thread(&Controller::beltSpeedSensor1Loop, this);
//    beltSpeedSensor2Thread = std::thread(&Controller::beltSpeedSensor2Loop, this);
//    levelSensorLoop_ = true;
//    levelSensorThread = std::thread(&Controller::levelSensorLoop, this);
    tc_ = true;
    daq_->setConveyorSpeed(model_->getSetPoint<double>("conveyorV"));
    daq_->setCreepConveyorSpeed(model_->getSetPoint<double>("creepconveyorV"));
    daq_->setInclineConveyorSpeed(model_->getSetPoint<double>("inclineconveyorV"));
    daq_->setHD75Speed(model_->getSetPoint<double>("HD75V"));
    model_->setCurrentValue<double>("conveyorSpeedEst1", 0);
    model_->setCurrentValue<double>("conveyorSpeedEst2", 0);
    highResImageFile_ = "";
    classifier_initialized = false;
    calibrationLoop_ = false;

}

Controller::~Controller()
{
    if(calibrationLoop_)
        stopCalibrateCamera();
    statusLoop_ = false;
    statusLoopThread.join();
    tc_ = false;
    beltSpeedSensor1Loop_ = false;
//    beltSpeedSensor2Loop_ = false;
    beltSpeedSensor1Thread.join();
//    beltSpeedSensor2Thread.join();
//    levelSensorLoop_ = false;
//    levelSensorThread.join();
}

double Controller::getConveyorSpeed(){
    // return 30;
    return model_->getCurrentValue<double>("conveyorSpeedEst1");
}

void Controller::calibrateBeltSpeed(std::ostream &out, const std::string &speedEst){
    std::this_thread::sleep_for(std::chrono::seconds(15));
    out << "voltage: " << 0 << std::endl;
    for(size_t i = 0; i < 5; ++i)
        out << 0 << std::endl;
    for(double i = 0.5; i < 5.5; i+=0.5)
    {
        std::cout << "voltage: " << i << std::endl;
        daq_->setConveyorSpeed(i);
        daq_->setConveyor(true);
        std::this_thread::sleep_for(std::chrono::seconds(60));
        for(size_t j = 0; j < 5; ++j)
        {
            out << model_->getCurrentValue<double>(speedEst) << std::endl;
            if(i < 1.5)
                std::this_thread::sleep_for(std::chrono::seconds(60));
            else if(i < 3.0)
                std::this_thread::sleep_for(std::chrono::seconds(30));
            else
                std::this_thread::sleep_for(std::chrono::seconds(15));
        }
    }
    daq_->setConveyor(false);
}

void Controller::setConveyorSpeed(double speed)
{
    // placeholder until we correlate speed with voltage
    model_->setSetPoint<double>("conveyorV", speed);
    // model_->setSetPoint<double>("conveyorS", speed);
    daq_->setConveyorSpeed(speed);
}

void Controller::setCreepConveyorSpeed(double speed)
{
    model_->setSetPoint<double>("creepconveyorV", speed);
    daq_->setCreepConveyorSpeed(speed);
}

void Controller::setInclineConveyorSpeed(double speed)
{
    model_->setSetPoint<double>("inclineconveyorV", speed);
    daq_->setInclineConveyorSpeed(speed);
}

void Controller::setHD75Speed(double speed)
{
    model_->setSetPoint<double>("HD75V", speed);
    daq_->setHD75Speed(speed);
}

void Controller::statusLoop()
{
    while(statusLoop_)
    {
        double val = daq_->getCabinetTemperature();
        model_->setCurrentValue<double>("ctemp", val);
        val = daq_->get24VDC();
        model_->setCurrentValue<double>("24V", val);
        val = daq_->get5VDC();
        model_->setCurrentValue<double>("5V", val);
        val = daq_->getCPAPressure();
        model_->setCurrentValue<double>("CPA", val);

        bool on = daq_->isDoorIlockSafe();
        model_->setCurrentValue<bool>("doorIlock", on);
        on = daq_->isOtherIlockSafe();
        model_->setCurrentValue<bool>("otherIlock", on);

	// if count is non zero, motor is running and speed is being sensed
//        if(daq_->readCTR(1))
//		model_->setCurrentValue<double>("inductive1", 0);
//        else
//		model_->setCurrentValue<double>("inductive1", 5);

        std::this_thread::sleep_for(checkStatusRate_);
    }
}

void Controller::beltSpeedSensor1Loop()
{
    std::chrono::time_point<std::chrono::system_clock> startTime, endTime;
    startTime = std::chrono::system_clock::now();
    int avgCounter=0;
    while(beltSpeedSensor1Loop_)
    {
      double belt_length = model_->getSetPoint<double>("belt_length");

// debug dave
//       model_->setCurrentValue<double>("conveyorSpeedEst1", 209);
      double inductive1 = daq_->getInductive1();
      model_->setCurrentValue<double>("inductive1", inductive1);


//  this code is for speed sensing using encoder and CTR
        double speedSensorFactor = model_->getSetPoint<double>("speedSensorFactor");
//	daq_->resetCTR(1);
//	startTime = std::chrono::system_clock::now();
//    	unsigned int count1 = daq_->readCTR(1);
//        std::this_thread::sleep_for(std::chrono::seconds(2));
//        endTime = std::chrono::system_clock::now();
//    	unsigned int count2 = daq_->readCTR(1);
//        auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
//	double speed = (float)(count2 - count1) / elapsed_time.count() * 1e3 * 60 * speedSensorFactor;
//        model_->setCurrentValue<double>("conveyorSpeedEst1", speed);

    	if(inductive1 > model_->getLowerBound<double>("inductive1"))
      {
           if(avgCounter==7){
            endTime = std::chrono::system_clock::now();
            auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
//            model_->setCurrentValue<double>("conveyorSpeedEst1", belt_length / elapsed_time.count() * 1e3 * 60);
            double curSpeed =  (speedSensorFactor*8) / elapsed_time.count() * 1e3 * 60;
            model_->setCurrentValue<double>("conveyorSpeedEst1",curSpeed);
            startTime = endTime;
 	    avgCounter = 0;
	    }else{
		avgCounter++;
	    }
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
       }
        std::this_thread::sleep_for(std::chrono::microseconds(500));
    }
}

void Controller::beltSpeedSensor2Loop()
{
    std::chrono::time_point<std::chrono::system_clock> startTime, endTime;
    startTime = std::chrono::system_clock::now();
    while(beltSpeedSensor2Loop_)
    {
        double belt_length = model_->getSetPoint<double>("belt_length");
        double inductive2 = daq_->getInductive2();
        model_->setCurrentValue<double>("inductive2", inductive2);
        if(inductive2 < model_->getLowerBound<double>("inductive2"))
        {
            endTime = std::chrono::system_clock::now();
            auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
            model_->setCurrentValue<double>("conveyorSpeedEst2", belt_length / elapsed_time.count() * 1e3 * 60);
            startTime = endTime;
            std::this_thread::sleep_for(std::chrono::seconds(2));
        }
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
    }
}


void Controller::levelSensorLoop()
{
    std::chrono::time_point<std::chrono::system_clock> startTime, endTime;
    startTime = std::chrono::system_clock::now();
    while(levelSensorLoop_)
    {
       if(daq_->getCreepFeeder()){
       	 double levelSensor = daq_->getLevelSensor();
         double levelSetpoint = model_->getLowerBound<double>("levelSensor");
         if(levelSensor > levelSetpoint)
         {
            std::this_thread::sleep_for(std::chrono::seconds(2));
            levelSensor = daq_->getLevelSensor();  // see if still triggered
            if(levelSensor > levelSetpoint){
            	std::this_thread::sleep_for(std::chrono::seconds(model_->getSetPoint<int>("levelOverrunSecs"))); // this will be overrun delay
		daq_->setCreepFeeder(false);
            	std::this_thread::sleep_for(std::chrono::seconds(model_->getSetPoint<int>("levelEmptySecs"))); // this will be empty delay
	    }
         }else
		daq_->setCreepFeeder(true);

       }else
		daq_->setCreepFeeder(false);

       std::this_thread::sleep_for(std::chrono::seconds(2));
    }
}


void Controller::findOrigin(const baslerCam::CameraPtr &cam, cv::Mat &img)
{
    if(cam == nullptr)
        throw std::runtime_error("The selected camera is not attached.");
    cam->getCameraSettings().print(std::cout);
    cam->startGrab();
    cv::Mat temp = cam->trigger_block_rgb();
    img = temp.clone();
    double inPerPixX, inPerPixY;
    double size = model_->getSetPoint<double>("fiducialSize");
    cv::Point2f originDist, originPix;
    try
    {
        std::tie(inPerPixX, inPerPixY) = baslerCam::Camera::findOrigin(img, size, originDist, originPix);
    }
    catch(const std::runtime_error& e)
    {
        cam->stopGrab();
        throw std::runtime_error(e.what());
    }
    std::string camSID = cam->getID();
    model_->setCamSet<double>(camSID, "inPerPixX", inPerPixX);
    model_->setCamSet<double>(camSID, "inPerPixY", inPerPixY);
    model_->setCamSet<double>(camSID, "in2PerPix2", inPerPixX * inPerPixY);
    model_->setCamSet<double>(camSID, "originXDist", originDist.x);
    model_->setCamSet<double>(camSID, "originYDist", originDist.y);
    model_->setCamSet<double>(camSID, "originXPix", originPix.x);
    model_->setCamSet<double>(camSID, "originYPix", originPix.y);
    cam->stopGrab();
}

void Controller::stopCalibrateCamera()
{
    calibrationLoop_ = false;
    calibrationThread.join();
}

void Controller::startCalibrateCamera(const baslerCam::CameraPtr &cam, int boardWidth, int boardHeight, double board_size_x, double board_size_y, std::function<void(cv::Mat)> callback)
{
    if(cam == nullptr)
        throw std::runtime_error("The selected camera is not connected.");
    calibrationLoop_ = true;
    calibrationThread = std::thread(&Controller::calibrateCamera, this, cam, boardWidth, boardHeight, board_size_x, board_size_y, callback);
}

void Controller::calibrateCamera(const baslerCam::CameraPtr &cam, int boardWidth, int boardHeight, double board_size_x, double board_size_y, std::function<void(cv::Mat)> callback)
{
  try{
    cam->startGrab();
    std::vector<std::vector<cv::Point2f>> imagePoints;
    cv::Mat camMat, distCoeffs;
    while(calibrationLoop_)
    {
        cv::Mat img = cam->trigger_block_rgb();
        cv::Mat gray;

#if CV_MAJOR_VERSION != 4 && CV_MAJOR_VERSION != 3

        cv::cvtColor(img, img, CV_RGB2GRAY);
#else
        cv::cvtColor(img, img, cv::COLOR_RGB2GRAY);
#endif

        std::vector<cv::Point2f> corners;
        bool foundCorners = findChessboardCorners(gray,
                                                  cv::Size(boardWidth, boardHeight),
                                                  corners,
                                                  cv::CALIB_CB_ADAPTIVE_THRESH
                                                  + cv::CALIB_CB_NORMALIZE_IMAGE);
        if(!foundCorners)
            continue;
        cv::drawChessboardCorners(img, cv::Size(boardWidth, boardHeight),
                                  cv::Mat(corners), foundCorners);
        callback(img);

        cornerSubPix(gray, corners,
                     cv::Size(11, 11), cv::Size(-1,-1),
                     cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER,
                                                      30, 0.1));
        imagePoints.push_back(corners);

        camMat = cv::Mat::eye(3, 3, CV_64F);
        distCoeffs = cv::Mat::zeros(8, 1, CV_64F);
        std::vector<cv::Point3f> corners_ideal;
        for(int i = 0; i < boardHeight; ++i)
            for(int j = 0; j < boardWidth; ++j)
                corners_ideal.push_back(cv::Point3f(board_size_x * j,
                                          board_size_y * i, 0));
        std::vector<std::vector<cv::Point3f>> objectPoints;
        objectPoints.push_back(corners_ideal);
        objectPoints.resize(imagePoints.size(), objectPoints[0]);

        std::vector<cv::Mat> rvecs, tvecs;
        double rms = cv::calibrateCamera(objectPoints, imagePoints, img.size(),
                                     camMat, distCoeffs, rvecs, tvecs);
        std::cout << rms << std::endl;
    }
    cam->setRectify(camMat, distCoeffs, cam->getImageSize());
    cam->stopGrab();
   }
   catch (const Pylon::GenericException & e)
   {
    	cam->stopGrab();
     	throw e;
   }
}

cv::Mat Controller::getPictureBGR(int position)
{
    baslerCam::CameraPtr cam = getCamera(position);
    if(cam == nullptr && position == 0)
        throw std::runtime_error("The left Camera is not connected.");
    if(cam == nullptr && position == 1)
        throw std::runtime_error("The right Camera is not connected.");
   try{
    	cam->startGrab();
    	cv::Mat img = cam->trigger_block_bgr();
    	cam->stopGrab();
    	return img;
    }
    catch (const Pylon::GenericException & e)
    {
    	cam->stopGrab();
     	throw e;
    }
}

cv::Mat Controller::getPictureBGR(baslerCam::CameraPtr cam)
{
    if(cam == nullptr)
        throw std::runtime_error("The Camera is not connected.");
    try{
	cam->startGrab();
    	cv::Mat img = cam->trigger_block_bgr();
    	cam->stopGrab();
    	return img;
    }
    catch (const Pylon::GenericException & e)
    {
    	cam->stopGrab();
     	throw e;
    }
}

cv::Mat Controller::getPictureRGB(int position)
{
    baslerCam::CameraPtr cam = getCamera(position);
    if(cam == nullptr && position == 0)
        throw std::runtime_error("The left Camera is not connected.");
    if(cam == nullptr && position == 1)
        throw std::runtime_error("The right Camera is not connected.");
    try{
    	cam->startGrab();
    	cv::Mat img = cam->trigger_block_rgb();
    	cam->stopGrab();
    	return img;
    }
    catch (const Pylon::GenericException & e)
    {
    	cam->stopGrab();
     	throw e;
    }
}

cv::Mat Controller::getPictureRGB(baslerCam::CameraPtr cam)
{
    if(cam == nullptr)
        throw std::runtime_error("The Camera is not connected.");
    try{
    	cam->startGrab();
	cv::Mat img = cam->trigger_block_rgb();
    	cam->stopGrab();
    	return img;
    }
    catch (const Pylon::GenericException & e)
    {
    	cam->stopGrab();
     	throw e;
    }
}

bool Controller::waitForLaserTrigger(void){
    daq_->waitForLaserTrigger(); // debug dave
}
