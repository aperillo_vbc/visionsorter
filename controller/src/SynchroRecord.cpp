#include "SynchroRecord.h"
#include "VisionSystem.h"

SynchroRecord::SynchroRecord(ControllerPtr controller, const std::vector<baslerCam::CameraPtr> cams, std::function<void(int, cv::Mat)> callback)
    : controller_(controller),
      model_(controller->getModel())
{
    int count = 0;
    feed_ = true;
    for(auto &cam : cams){
        capture_image.push_back(false);
        feeds.push_back(std::thread(&SynchroRecord::feedLoop, this, count, cam, callback));
        ++count;
    }
    mutexes.resize(count);
    capture_cvs.resize(count);
    timerLoop = std::thread(&SynchroRecord::timer, this);
}

SynchroRecord::~SynchroRecord(){
    std::vector<std::unique_lock<std::mutex>> locks;
    for(auto &mutex : mutexes)
        locks.push_back(std::unique_lock<std::mutex>(mutex));
    feed_ = false;
    for(auto &cv : capture_cvs)
        cv.notify_all();
    for(auto &lock : locks)
        lock.unlock();

    for(auto &feed : feeds){
        if(feed.joinable())
            feed.join();
    }
    if(timerLoop.joinable())
        timerLoop.join();
}

void SynchroRecord::feedLoop(int index, baslerCam::CameraPtr cam, std::function<void(int, cv::Mat)> callback){
    Segmenter segmenter(controller_, cam);
    if(cam == nullptr)
        return;
    std::string camSID = cam->getID();
    baslerCam::CameraPtr _cam = cam;
    int position = model_->getCamSet<int>(cam->getID(), "position");
    cam->startGrab();
    while(feed_){
        std::unique_lock<std::mutex> lk(mutexes[index]);
        capture_cvs[index].wait(lk, [this, index]{return capture_image[index];});
        capture_image[index] = false;
        cv::Mat image = cam->trigger_block_bgr();
        if(model_->getSetPoint<int>("segmentBeforeRecord")){
            cv::cuda::GpuMat d_img;
            cv::Mat h_img;
            std::vector<cv::Rect> pieces;
            d_img.upload(image);
            segmenter.segment(d_img, pieces, h_img, true);
            if(pieces.size())
            {
/***************************************************************************************************************************************/
                cv::Mat h_background_img = cv::imread("background_750x750.bmp");
                cv::cuda::GpuMat d_background;
                d_background.upload(h_background_img);
                cv::cuda::GpuMat d_img;
                d_img.upload(image);
                for(size_t i = 0; i < pieces.size(); ++i)
                {
                  //std::srand(time(0));
                  cv::cuda::GpuMat d_dest = d_background.clone();
                  cv::cuda::GpuMat d_piece(d_img, pieces[i]);
                  int width = d_dest.cols;
                  int height = d_dest.rows;
                  segmenter.crop(d_piece, width, height);
                  cv::Mat h_img;
                  d_piece.download(h_img);
//cv::imwrite(std::string("./data/crop_piece_")+std::to_string(i)+".jpg", h_img);
                  segmenter.overlayFrameCard(d_piece, d_dest, d_img, pieces[i]);
                  d_dest.download(h_img);

                  cv::imwrite(std::string("./data/final_piece_")+std::to_string(rand())+std::to_string(rand())+".jpg", h_img);
//cv::imwrite(cam->getTriggerTimeString(), h_img);
                  callback(position,h_img);
                }


/***************************************************************************************************************************************/
                //cv::imwrite(cam->getTriggerTimeString(), image);
                //callback(position, image);
            }
        }
        else{
            cv::imwrite(cam->getTriggerTimeString(), image);
            callback(position, image);
        }
    }
    cam->stopGrab();
}

void SynchroRecord::timer(){
    double travelLength = model_->getSetPoint<double>("synchroImageLength");
    while(feed_){
        if(model_->getSetPoint<int>("matchBeltSpeedRecord")){
            std::chrono::microseconds sleepTime = std::chrono::microseconds((int)(travelLength / controller_->getConveyorSpeedInuSec()));
            std::this_thread::sleep_for(sleepTime);
        }
        else{
            double fps = model_->getSetPoint<double>("fps");
            std::chrono::milliseconds sleepTime = std::chrono::milliseconds((int)(1.0/fps*1e3));
            std::this_thread::sleep_for(sleepTime);
        }
        std::vector<std::unique_lock<std::mutex>> locks;
        int count = 0;
        for(auto &mutex : mutexes){
            locks.push_back(std::unique_lock<std::mutex>(mutex));
            capture_image[count++] = true;
        }
        for(auto &cv : capture_cvs)
            cv.notify_all();
    }
}
