#include "stdio.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <future>
#include <VisionSystem.h>
#include <thread>
#include <chrono>
#include <sys/time.h>
#include "stdlib.h"

#define RESIZE_MAX_HEIGHT 750
#define RESIZE_MAX_WIDTH 750
#define sysNow() std::chrono::system_clock::now()

long long current_timestamp()
{
    struct timeval te;
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    return milliseconds;
}

void SaveImageFile(cv::Mat imageToSave)
{
    std::time_t t = std::time(0);   // get time now
    std::tm* now = std::localtime(&t);
    long milliseconds;
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    milliseconds = round(spec.tv_nsec / 1.0e6);

    char filename[128];
    memset(filename, 0, sizeof(filename));
    sprintf(filename, "Image_%d%d%d_%ld.jpg",
            now->tm_hour,
            now->tm_min,
            now->tm_sec,
            milliseconds);

    cv::imwrite(filename, imageToSave);

    // Check the file to make sure it isn't corrupted.

    cv::Mat fileRead = cv::imread(filename);

    std::string sysCall = "rm -rf ";
    sysCall += filename;

    if(fileRead.empty())
    {
        // The file couldn't be read, delete the file with a system call.

        system(sysCall.c_str());
    }
}

std::string ticToc(std::chrono::system_clock::time_point start)
{
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(sysNow()-start);
    return std::to_string(elapsed.count()) + "(ms)";
}

Segmenter::Segmenter(ControllerPtr controller, baslerCam::CameraPtr cam, int whichSeg)
    : controller_(controller),
      model_(controller->getModel()),
      cam_(cam),
      whichSeg_(whichSeg)
{
    scale_factor_ = 1./255.;
    segWarmedUp_ = false;
    imageFeed_ = false;
    img_rect_size_ = cv::Size(299, 299);
    if(cam == nullptr)
        camSID="0";
    else
        camSID=cam->getID();
    pause_ = false;
    record_ = false;
}

Segmenter::~Segmenter(){
    stopLoops();
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void Segmenter::startLoops(std::function<void(const SegMsg &)> pushSegMsg){
    segWarmedUp_ = false;
    imageFeed_ = true;
    if(cam_ == nullptr)
        throw std::runtime_error("Cannot start segmentation loop, no Camera attached");
    segLoopThread = std::thread(&Segmenter::segmentLoop, this, pushSegMsg);
       segLoopThread.detach();
    waitOnSegmentationWarmUp();
    imgFeedThread = std::thread(&Segmenter::imageFeed, this);
}

void Segmenter::stopLoops(){
    // release the locks on the waiting segmenter
    // set the state variables to exit the loops
    std::unique_lock<std::mutex> clk(captureLock);
    imageFeed_ = false;
    capture_cv.notify_all();
    clk.unlock();

    std::unique_lock<std::mutex> wlk(segWarmLock);
    segWarmedUp_ = true;
    segWarm_cv.notify_all();
    wlk.unlock();

    // if the system is paused unpause it with the exit variables set
    resumeLoops();

    if(imgFeedThread.joinable())
        imgFeedThread.join();
    if(segLoopThread.joinable())
        segLoopThread.join();

    std::queue<cv::Mat>().swap(imageQueue);
    std::queue<TimePt>().swap(timeQueue);
}

void Segmenter::pauseLoops(){
    std::lock_guard<std::mutex> lk(pauseLock);
    pause_ = true;
}

void Segmenter::resumeLoops(){
    std::lock_guard<std::mutex> lk(pauseLock);
    pause_ = false;
    pause_cv.notify_all();
}

void Segmenter::imageFeed(){
    if(cam_ == nullptr)
        return;
    cv::cuda::setDevice(0);
    // local copies of shared pointers
    baslerCam::CameraPtr cam = cam_;
    ControllerPtr controller = controller_;
    cam->startGrab();

    double travelLength = model_->getCamSet<double>(camSID, "imageLength") - model_->getSetPoint<double>("imageOverlap");

   // warmup
    {
        cv::Mat image = cam->trigger_block_rgb();
    }

    while(imageFeed_)
    {
      try {
        if(pause_)
        {
            std::unique_lock<std::mutex> lk(pauseLock);
            cam->stopGrab();
            pause_cv.wait(lk, [this]{return !pause_;});
            lk.unlock();
            cam->startGrab();
            continue;
        }
        double beltSpeed = controller->getConveyorSpeedInuSec();
        std::chrono::microseconds time_available;
        if(beltSpeed <= 1e-12)
            time_available = std::chrono::microseconds((int)(1/model_->getSetPoint<double>("fps")*1e6));
        else
            time_available = std::chrono::microseconds((int)(travelLength / beltSpeed));
        // std::cout << "belt speed is: " << beltSpeed << " time_available is: " << time_available.count() / 1e3 << " ms" << std::endl;
	// std::cout << "imagegrab from Seg: " << whichSeg_ << std::endl;
	// dave debug used to debug camera trigger speed	
	//controller->waitForLaserTrigger();

        cv::Mat image = cam->trigger_block_rgb();
        pushImage(image, cam->getTriggerTime());
        auto sleepTime = std::chrono::duration_cast<std::chrono::milliseconds>(time_available - (std::chrono::system_clock::now() - cam->getTriggerTime()));
        std::this_thread::sleep_for(sleepTime);
     }
     catch (const Pylon::GenericException & e)
     {
    	std::cerr << "Failed trigger/retrieve. Reason: "
  	<< e.GetDescription() << std::endl;
     	// throw e;
     }
    }
    cam->stopGrab();
}

cv::Rect2i Segmenter::cropROI(cv::cuda::GpuMat &img, int height, int width)
{
    if (img.rows == height && img.cols == height)
        return cv::Rect2i(0,0, width, height);
    int y = (int)((img.rows - height)/2);
    int x = (int)((img.cols - width)/2);

    // strictly bigger
    if(img.rows > height && img.cols > width)
        return cv::Rect2i(x, y, width, height);
    else if(img.rows > height) // too tall
        return cv::Rect2i(0, y, img.cols, height);
    else if(img.cols > width) // too wide
        return cv::Rect2i(x, 0, width, img.rows);
    else // either just right or small enough on both axis
        return cv::Rect2i(0, 0, img.cols, img.rows);
}

void Segmenter::crop(cv::cuda::GpuMat &img, int height, int width){
    cv::Rect2i roi = cropROI(img, height, width);
    img = cv::cuda::GpuMat(img, roi);
}

void Segmenter::overlayFrameCard(cv::cuda::GpuMat &d_piece, cv::cuda::GpuMat &d_dest, const cv::cuda::GpuMat &global_img, const cv::Rect2i &location)
{
    int g_height = global_img.rows;
    int g_width = global_img.cols;

    int height = d_dest.rows;
    int width = d_dest.cols;

    int y = 0;
    int dy = d_piece.rows;
    if(location.tl().y <= 1)
        y = 0;
    else if(location.br().y >= g_height - 2)
        y = (int)(height - dy);
    else
        y = (int)((height - dy) / 2);

    int x = 0;
    int dx = d_piece.cols;
    if(location.tl().x <= 1)
        x = 0;
    else if(location.br().x >= g_width - 2)
        x = (int)(width - dx);
    else
        x = (int)((width - dx) / 2);

    cv::Rect2i destROI = cv::Rect2i(x, y, dx, dy);
    // std::cout << "x y dx dy " << x << " " << y << " " << dx << " " << dy << std::endl;

    cv::cuda::GpuMat d_dest_roi = d_dest;
    d_dest_roi = d_dest(destROI);
    d_piece.copyTo(d_dest_roi);
}

void Segmenter::segmentLoop(std::function<void(const SegMsg &)> pushSegMsg)
{
    cv::cuda::setDevice(0);
    //warm up
    {
        cv::Mat h_img;
        h_img = cv::imread("seg_warmup.jpeg");
        std::vector<cv::Rect> pieces;
        cv::Mat img_boxes = h_img.clone();
        cv::cuda::GpuMat d_img;
        d_img.upload(h_img);
        segment(d_img, pieces, img_boxes, false);
        setSegmentationWarmedUp();
    }

    cv::Mat h_background_img = cv::imread("background_750x750.bmp");
    cv::cuda::GpuMat d_background_img, d_background;
    d_background.upload(h_background_img);
    cv::cuda::cvtColor(d_background, d_background, CV_BGR2RGB);
    d_background.convertTo(d_background_img, CV_32FC3, scale_factor_);

    while(imageFeed_)
     {
        cv::Mat h_img;
        TimePt timePt;
        std::tie(h_img, timePt) = popImage();
        if(!imageFeed_)
            break;

        auto start = sysNow();

        std::vector<cv::Rect2i> pieces;
        cv::Mat img_boxes = h_img.clone();
        cv::cuda::GpuMat d_img;
        d_img.upload(h_img);
        SegMsg msg(h_img, img_boxes, timePt, std::vector<SubImg>());
        segment(d_img, pieces, msg.img_boxes, false);
        //std::cout << "time to segment: " << ticToc(start) << std::endl;

        cv::cuda::GpuMat d_img_mod;
        d_img.convertTo(d_img_mod, CV_32FC3, scale_factor_);

        // normalization
        // extract regions of interest and resize to input size for neural net
        for(size_t i = 0; i < pieces.size(); ++i)
        {
            cv::cuda::GpuMat d_dest = d_background_img.clone();;
            cv::cuda::GpuMat d_piece(d_img_mod, pieces[i]);

            int width = d_dest.cols;
            int height = d_dest.rows;
            crop(d_piece, height, width);
            overlayFrameCard(d_piece, d_dest, d_img_mod, pieces[i]);
            cv::cuda::resize(d_dest, d_dest, img_rect_size_, 0, 0, cv::INTER_NEAREST);
            cv::Mat temp;

            d_dest.download(temp);

            // Save the tmp file to disk.

            std::thread writeToFile(SaveImageFile, temp);
            writeToFile.detach();

            msg.subImgs.emplace_back(temp, pieces[i], timePt, camSID);
        }

        if(msg.subImgs.size() > 0)
        {
            // std::cout << "time to segment: " << ticToc(start) << std::endl;
	    // std::cout << "No of segmented pieces: " << msg.subImgs.size() << std::endl;
            // cv::imwrite(getTriggerTimeString(timePt,"data/"), h_img);    // this writes full image to a file

            pushSegMsg(msg);
        }
    }
}
// copy this function to your system and add prototype to VisionSystem.h
std::string Segmenter::getTriggerTimeString(TimePt timePt, const std::string prefix)
{
            std::stringstream timeStr;
            timeStr << prefix << "cam" << "_";
            auto now_c = std::chrono::system_clock::to_time_t(timePt);
            timeStr << std::put_time(std::localtime(&now_c), "%H-%M-%S-");
            timeStr << std::setfill('0') << std::setw(3);
            auto fraction = timePt - std::chrono::time_point_cast<std::chrono::seconds>(timePt);
            timeStr << std::chrono::duration_cast<std::chrono::milliseconds>(fraction).count();
            timeStr << std::put_time(std::localtime(&now_c), "_%m-%d-%y");
            timeStr << ".bmp";
            return timeStr.str();
}

void Segmenter::setSegmentationWarmedUp()
{
    std::lock_guard<std::mutex> lk(segWarmLock);
    segWarmedUp_ = true;
    segWarm_cv.notify_all();
}

void Segmenter::waitOnSegmentationWarmUp()
{
    std::unique_lock<std::mutex> lk(segWarmLock);
    segWarm_cv.wait(lk, [this]{return segWarmedUp_;});
}

void Segmenter::pushImage(cv::Mat img, const TimePt &timePt){
    // std::cout<<"entered push image function "<<std::endl;
    std::unique_lock<std::mutex> lk(captureLock);
    imageQueue.push(img.clone());
    timeQueue.push(timePt);
    capture_cv.notify_all();
    //std::cout<<"going to exit push image function "<<std::endl;
}

std::pair<cv::Mat, TimePt> Segmenter::popImage(){
    std::unique_lock<std::mutex> clk(captureLock);
    capture_cv.wait(clk, [this]{return !imageQueue.empty() || !imageFeed_;});
    if(!imageFeed_)
        return std::make_pair(cv::Mat(), TimePt());

    auto timePt = timeQueue.front();
    timeQueue.pop();
    auto img = imageQueue.front();
    imageQueue.pop();
    return std::make_pair(img, timePt);
}

void Segmenter::getMeanValues(cv::Mat &h_img, cv::Mat &h_img_mn)
{
	stringstream ss;
	for (int x=0; x<=h_img_mn.cols-128; x+=128)
	{
        for (int y=0; y<=h_img_mn.rows-96; y+=96) 
		{
        	// get the average for the whole block:
        	cv::Rect roi(x,y,128,96);
        	cv::Scalar mean;			
        	mean = cv::mean(h_img_mn(roi));
			ss << setprecision(1) << fixed << mean[0];	
			//set (x,y) inside cv::Point2d() such that its the centre of the grid, in this case around (64,48)
			cv::putText(h_img_mn, ss.str(), cv::Point2d(x+50,y+48),cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255,255,255));
			//std::cout<<"Mean of "<< y << " and " << x << " " <<mean[0]<<std::endl;
			ss.str(string());
    	}
	}
       
}

void Segmenter::threshold(cv::Mat &h_img, cv::Mat &h_img_thresh, bool bgr)
{
    cv::cuda::GpuMat d_img, d_img_thresh;
    d_img.upload(h_img);
    threshold(d_img, d_img_thresh, bgr);
    d_img_thresh.download(h_img_thresh);
}

void Segmenter::threshold(cv::cuda::GpuMat &d_img, cv::cuda::GpuMat &d_img_thresh, bool bgr)
{
    cv::cuda::GpuMat d_highlight, d_shadow;
    if(d_img.channels() > 1)
    {
        if(bgr)
            cv::cuda::cvtColor(d_img, d_highlight, CV_BGR2GRAY);
        else
            cv::cuda::cvtColor(d_img, d_highlight, CV_RGB2GRAY);
    }
    else
        d_highlight = d_img.clone();
    d_shadow = d_highlight.clone();
    int highlightThresh = model_->getSetPoint<int>("highlightThresh");
    int shadowThresh = model_->getSetPoint<int>("shadowThresh");
    cv::cuda::threshold(d_highlight, d_highlight, highlightThresh, 255, cv::THRESH_BINARY);
    cv::cuda::threshold(d_shadow, d_shadow, shadowThresh, 255, cv::THRESH_BINARY);
    cv::cuda::bitwise_not(d_shadow, d_shadow);
    cv::cuda::add(d_highlight, d_shadow, d_img_thresh);
}

void Segmenter::dilate(cv::Mat &h_img, cv::Mat &h_img_dil)
{
    cv::cuda::GpuMat d_img;
    d_img.upload(h_img);
    dilate(d_img, d_img);
    d_img.download(h_img_dil);
}

void Segmenter::dilate(cv::cuda::GpuMat &d_img_thresh, cv::cuda::GpuMat &d_img_dilate)
{
    int dilationSize = model_->getSetPoint<int>("dilationSize");
    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT,
                                                cv::Size(
                                                    dilationSize,
                                                    dilationSize));
    int iterations = model_->getSetPoint<int>("dilationIterations");
    cv::Ptr<cv::cuda::Filter> dilateFilter = cv::cuda::createMorphologyFilter(
        cv::MORPH_DILATE,
        CV_8UC1,
        element,
        cv::Point(-1,-1),
        iterations);
    dilateFilter->apply(d_img_thresh, d_img_dilate);
}

void Segmenter::findContours(cv::cuda::GpuMat &d_img_thresh, std::vector<cv::Rect2i> &pieces, cv::Mat &h_img)
{
    cv::Mat h_img_bw;
    d_img_thresh.download(h_img_bw);
    findContours(h_img_bw, pieces, h_img);
}

void Segmenter::findContours(cv::Mat &h_img_bw, std::vector<cv::Rect2i> &pieces, cv::Mat &h_img)
{
    pieces.resize(0);
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(h_img_bw, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    // std::cout << "time to find contour: " << ticToc(start) << std::endl;
    double minArea = model_->getSetPoint<double>("pieceMinAreaIn");
    double maxArea = model_->getSetPoint<double>("pieceMaxAreaIn");
    double conv;
    conv = model_->getCamSet<double>(camSID, "in2PerPix2");
    for (size_t i = 0; i < contours.size(); i++)
    {
        cv::drawContours(h_img, contours, i, cv::Scalar(0, 0, 255), 2);
        cv::Rect2i bRect = boundingRect(cv::Mat(contours[i]));
        // make sure that the piece spans neither axis
        // as this is clearly an anomoly
        // also check that the rectangle is of acceptable size
        if((bRect.tl().y <= 1 && bRect.br().y >= h_img_bw.rows - 2)
           || (bRect.tl().x <= 1 && bRect.br().x >= h_img_bw.cols - 2)
           || bRect.area() * conv < minArea
           || bRect.area() * conv > maxArea)
            continue;
        cv::rectangle(h_img, bRect.tl(), bRect.br(), cv::Scalar(255, 0, 0), 2, 8, 0);
        pieces.push_back(bRect);
    }
}

void Segmenter::segment(cv::cuda::GpuMat &d_img, std::vector<cv::Rect> &pieces, cv::Mat &h_img, bool bgr)
{
    // auto start = sysNow();
    cv::cuda::GpuMat d_img_thresh;
    threshold(d_img, d_img_thresh, bgr);
    dilate(d_img_thresh, d_img_thresh);
    findContours(d_img_thresh, pieces, h_img);
    // auto timeTest = sysNow();
    // std::cout << "time to std::cout" << ticToc(timeTest) << std::endl;
    // std::cout << "time to contour and find bound rect: " << ticToc(start) << std::endl;
}

ImageBatcher::ImageBatcher(ControllerPtr controller, const std::vector<baslerCam::CameraPtr> &cameras, size_t _batch_size)
    : controller_(controller),
      model_(controller_->getModel()),
      batch_size(_batch_size)
{
    int i = 0;
    for(auto &cam : cameras)
    {
        i++;
        std::unique_ptr<Segmenter> segmenter = std::unique_ptr<Segmenter>(new Segmenter(controller, cam, i));
        segmenters.push_back(std::move(segmenter));
    }
    batching_ = true;
    pause_ = false;
}

ImageBatcher::~ImageBatcher(){
    stopLoops();
}

void ImageBatcher::pushSegMsg(const SegMsg &msg){
    std::unique_lock<std::mutex> lk(segLock);
    for(auto &subImg : msg.subImgs)
        segQueue.push(subImg);
    segLock_cv.notify_all();
}

void ImageBatcher::popSegMsg(std::vector<SubImg> &subImgs){
    subImgs.resize(0);
    std::unique_lock<std::mutex> slk(segLock);
    segLock_cv.wait(slk, [this]{return !segQueue.empty() || !batching_;});
    if(!batching_)
        return;

    for(size_t i = 0; !segQueue.empty() && i < model_->getSetPoint<size_t>("class_batch_size"); ++i)
    {
        subImgs.push_back(segQueue.front());
        segQueue.pop();
    }
}

void ImageBatcher::pauseLoops(){
    pause_ = true;
    for(auto &seg : segmenters){
        if(!seg->isPaused())
            seg->pauseLoops();
    }
}

void ImageBatcher::resumeLoops(){
    for(auto &seg : segmenters){
        if(seg->isPaused())
            seg->resumeLoops();
    }
    pause_ = false;
}

void ImageBatcher::startLoops(){
    batching_ = true;
    for(auto &seg : segmenters)
    {
        seg->startLoops(std::bind(&ImageBatcher::pushSegMsg, this,
                                 std::placeholders::_1));
    }
}

void ImageBatcher::stopLoops(){
    std::unique_lock<std::mutex> slk(segLock);
    batching_ = false;
    segLock_cv.notify_all();
    slk.unlock();

    for(auto &seg : segmenters)
        seg->stopLoops();
    std::queue<SubImg>().swap(segQueue);
}

VisionSystem::VisionSystem(ControllerPtr controller)
    : controller_(controller),
      model_(controller_->getModel()),
      daq_(controller_->getDAQ()),
      imgBatcher(controller, controller->getCameras())
{
    warmedUp = false;
    classLoop_ = false;
    nozzleSchedLoop_ = false;
    classifier_initialized = false;
    pause_ = false;
    int numValves = model_->getSetPoint<int>("numValves");
    onTimes.resize(numValves);
    offTimes.resize(numValves);
    onCounts.resize(numValves, 0);
}

VisionSystem::~VisionSystem(){
    stopLoops();
    if(isClassifierInit())
    {
        v_uninit_classifier();
        finalize_classifier();
    }
}

void VisionSystem::v_init_classifier()
{
    if(classifier_initialized)
        return;
    init_classifier(model_->getMeta("modelPath"));
    classifier_initialized = true;
}

void VisionSystem::v_uninit_classifier()
{
    if(classifier_initialized)
        uninit_classifier();
    classifier_initialized = false;
}

void VisionSystem::classLoop(std::function<void(cv::Mat, std::string, VS_STATE)> callback)
{
    if(!classifier_initialized)
    {
        callback(cv::Mat(), std::string("Please wait, loading the model: \n") + model_->getMeta("modelPath"), VS_STATE::VS_INIT);
        try{
            v_init_classifier();
        }
        catch(const std::runtime_error &e){
            callback(cv::Mat(), e.what(), VS_STATE::VS_STANDBY);
            uninit_classifier();
            return;
        }
        classifier_initialized = true;
    }
    // classifier warmup
    try{
        cv::Mat h_img = cv::imread("class_warmup.jpeg");
        cv::cuda::GpuMat d_img;
        d_img.upload(h_img);
        d_img.convertTo(d_img, CV_32FC3, 1./255.);
        d_img.download(h_img);
        SubImg subimg(h_img, cv::Rect2d());
        std::vector<SubImg> subimgs;
        subimgs.push_back(subimg);
        classify(subimgs);
        std::cout<<"classify in the vison system"<<std::endl;
    }
    catch(const std::runtime_error &e){
        callback(cv::Mat(), e.what(), VS_STATE::VS_STANDBY);
        uninit_classifier();
        return;
    }
    setClassWarmedUp();
    while(classLoop_)
    {
        std::vector<SubImg> subImgs;
        imgBatcher.popSegMsg(subImgs);
        if(!classLoop_)
            break;
        // auto start = sysNow();
        try{
            classify(subImgs);
        }
        catch(const std::runtime_error &e){
            callback(cv::Mat(), e.what(), VS_STATE::VS_STANDBY);
            uninit_classifier();
            return;
        }
        // std::cout << "time to classify and return: " << ticToc(start) << " " << subImgs.size() << " pieces" << std::endl;
        // for(auto &subimg : subImgs)
            // printSubImg(std::cout, subimg);
        pushSubImages(subImgs);
    }
}

void VisionSystem::setClassWarmedUp()
{
    std::lock_guard<std::mutex> wlk(warmLock);
    warmedUp = true;
    warm_cv.notify_all();
}

void VisionSystem::waitOnClassWarmup()
{
    std::unique_lock<std::mutex> wlk(warmLock);
    warm_cv.wait(wlk, [this]{return warmedUp;});
}

void VisionSystem::pauseLoops(){
    pause_ = true;
    imgBatcher.pauseLoops();
}

void VisionSystem::resumeLoops(){
    pause_ = false;
    if(imgBatcher.isPaused())
        imgBatcher.resumeLoops();
}

void VisionSystem::stopLoops()
{
    imgBatcher.stopLoops();

    std::unique_lock<std::mutex> wlk(warmLock);
    warmedUp = true;
    warm_cv.notify_all();
    wlk.unlock();

    std::unique_lock<std::mutex> clk(classLock);
    nozzleSchedLoop_ = false;
    class_cv.notify_all();
    clk.unlock();

    classLoop_ = false;

    if(nozLoopThread.joinable())
        nozLoopThread.join();
    if(nozzleSchedThread.joinable())
        nozzleSchedThread.join();
    if(classLoopThread.joinable())
        classLoopThread.join();

    std::queue<SubImg>().swap(classQueue);
    std::vector<std::queue<std::chrono::system_clock::time_point>>().swap(onTimes);
    std::vector<std::queue<std::chrono::system_clock::time_point>>().swap(offTimes);
    std::vector<int>().swap(onCounts);
    std::queue<NozAct>().swap(nozActQueue);

    if(isClassifierInit())
        v_uninit_classifier();
}

void VisionSystem::startLoops(std::function<void(cv::Mat, std::string, VS_STATE)> callback)
{
    if(controller_->getCameras().size() == 0)
    {
        callback(cv::Mat(), "Error, the Vision Loops cannot run without cameras. Please attach cameras and restart program", VS_STATE::VS_STANDBY);
        return;
    }
    warmedUp = false;
    classLoop_ = true;
    nozzleSchedLoop_ = true;
    // start the nozzle scheduler
    nozLoopThread = std::thread(&VisionSystem::nozLoop, this);
    nozzleSchedThread = std::thread(&VisionSystem::nozzleSchedLoop, this);
    // start the classifier loop
    classLoopThread = std::thread(&VisionSystem::classLoop, this, callback);
    waitOnClassWarmup();
    // start the segmenters and image feed
    imgBatcher.startLoops();
    if (!pause_ && classLoop_)
        callback(cv::Mat(), "Vision Loop is Running.", VS_STATE::VS_RUN);
}

void VisionSystem::pushSubImages(const std::vector<SubImg> &subImgs)
{
    std::unique_lock<std::mutex> lk(classLock);
    for(auto &subImg : subImgs)
        classQueue.push(subImg);
    lk.unlock();
    class_cv.notify_one();
}

void VisionSystem::popSubImages(std::vector<SubImg> &subImgs)
{
        subImgs.resize(0);
        std::unique_lock<std::mutex> lk(classLock);
        class_cv.wait(lk, [this]{return !classQueue.empty() || !nozzleSchedLoop_;});
        if(!nozzleSchedLoop_)
            return;

        while(!classQueue.empty())
        {
            subImgs.push_back(classQueue.front());
            classQueue.pop();
        }
}

// opencv rectangles, x,y are top left corner, i.e., smallest values on the rectangle
void VisionSystem::convertToDistance(SubImg &subimg)
{
    const std::string &id = subimg.camSID;
    double convX, convY;
    convX = model_->getCamSet<double>(id, "inPerPixX");
    convY = model_->getCamSet<double>(id, "inPerPixY");
    const cv::Rect2i &rect = subimg.rectP;
    subimg.rectD = cv::Rect2d(rect.x*convX, rect.y*convY, rect.width*convX, rect.height*convY);
}

void VisionSystem::nozzleSchedLoop()
{
    while(nozzleSchedLoop_)
    {
        std::vector<SubImg> subImgs;
        popSubImages(subImgs);
        if(subImgs.empty())
            break;

        // cleanse the pieces that do not get air blasted
        int count0 = 0;
        auto it = subImgs.begin();
        //double probClass1 = model_->getSetPoint<double>("probClass1");
        while(it != subImgs.end())
        {
	    
            int index = std::distance(it->classes.begin(), std::max_element(it->classes.begin(), it->classes.end()));
            // don't count the seam
 //           if(index == 0){
 //               std::cout << "seam detected" << std::endl;
		// std::cout << "if index is equal to 3- size of subimages: " << subImgs.size()<< std::endl;
 //               subImgs.erase(it);
 //               continue;
 //           } 
            // for testing, scrap is 0 coin is 1
            if(index == 0){
                //std::cout << "seam detected one" << std::endl;
		//std::cout << "if index is less than 3- size of subimages: " << subImgs.size()<< std::endl;
                ++count0;
                //saveSubImg(*it);
                //cv::imwrite(std::string("./data/new/final_piece_")+std::to_string(rand())+".jpg", subImgs);
                subImgs.erase(it);
                continue;
            }
            else{
                // saveSubImg(*it);
            }
            ++it;
        }
        model_->setCurrentValue<int>("class1Count", model_->getCurrentValue<int>("class1Count") + subImgs.size());
        model_->setCurrentValue<int>("class0Count", model_->getCurrentValue<int>("class0Count") + count0);

        // sort the images so that nozzles will check for pieces by spatial and thus temporal priority
        std::sort(subImgs.begin(), subImgs.end(),
                  [](const SubImg & a, const SubImg & b) -> bool
                  {
                      return a.rectP.tl().x > b.rectP.tl().x;
                  });

        double beltSpeed = controller_->getConveyorSpeedInuSec();
        std::vector<NozAct> msgs;
        for(auto &subimg : subImgs)
        {
            // get the calibrated system measurements
            // from fiducial top right to the nozzle top left
            double nozXDist = model_->getCamSet<double>(subimg.camSID, "nozXDist");
            double nozYDist = model_->getCamSet<double>(subimg.camSID, "nozYDist");
            // from the fiducial top right to the image origin
            double originXDist = model_->getCamSet<double>(subimg.camSID, "originXDist");
            double originYDist = model_->getCamSet<double>(subimg.camSID, "originYDist");

            // remember that increasing lane order is negative Y direction
            double nozX = nozXDist + originXDist;
            double nozY = nozYDist + originYDist;

            convertToDistance(subimg);
            // std::cout << "nozDist: " << nozXDist << ", " << nozYDist << std::endl;
            // std::cout << "originDist: " << originXDist << ", " << originYDist << std::endl;
            // std::cout << "noz wrt to origin: " << nozX << ", " << nozY << std::endl;

            double x = subimg.xD() + subimg.widthD();
            //std::cout << "subimg.xD()" << subimg.xD() << std::endl;
            double y1 = subimg.yD();
            double y2 = y1 + subimg.heightD();
            //std::cout << "xP,yP: " << subimg.xP() << ", " << subimg.yP()
            //          << " xD,yD: " << subimg.xD() << ", " << subimg.yD()
            //          << " x, y1, y2: "  << " : " << x << ", " << y1 << ", " << y2 << std::endl;

            std::vector<int> sols;
            double pos = nozY;
            double neededIntersect = model_->getSetPoint<double>("neededIntersectPercent");
            NozAct msg;
            for(auto &sol : model_->getSolenoids(subimg.camSID))
            {
                double intersectPercent = 0;
                double nextPos = pos + sol.second;
                double height = sol.second;
                if(y1 >= nextPos || y2 <= pos) // strictly before or after the valve
                    intersectPercent = 0;
                else if(y1 >= pos && y2 <= nextPos) // smaller than valve
                    intersectPercent = (y2-y1) / height;
                else if(y1 <= pos && y2 >= nextPos) // encompasses
                    intersectPercent = 1;
                else if(y1 >= pos) // overlaps to the right
                    intersectPercent = (nextPos - y1) / height;
                else // overlaps to the left
                    intersectPercent = (y2 - pos) / height;

                // std::cout << intersectPercent << " " << pos << ", " << nextPos << std::endl;

                if(intersectPercent >= neededIntersect)
                    msg.sols.push_back(sol.first);
                pos = nextPos;
            }
            if(msg.sols.empty())
                continue;

             //std::cout << "firing sols: ";
             //for(auto &sol : msg.sols)
             //    std::cout << sol << " ";
             //std::cout << std::endl;

            x = nozX - x;
            // std::cout << "x: " << x << " beltSpeed: " << beltSpeed << std::endl;

            if(subimg.widthD() > 3.5)
                msg.on = subimg.timePt + std::chrono::microseconds(
                    (int)(x / beltSpeed
                          + model_->getSetPoint<int>("nozOffsetTime")*1000
                          + model_->getSetPoint<int>("nozLargePieceDelay")*1000));
            else{
                msg.on = subimg.timePt + std::chrono::microseconds(
                    (int)(x / beltSpeed
                          + model_->getSetPoint<int>("nozOffsetTime")*1000));}
            // std::cout <<  "piece width: " << subimg.widthD() << " large piece boost: " << model_->getSetPoint<int>("nozLargePieceDurationBoost") << " small piece boost: " << model_->getSetPoint<int>("nozSmallPieceDurationBoost") << std::endl;

            if(subimg.widthD() < 2.25)
                msg.off = msg.on + std::chrono::microseconds(
                    (int)( subimg.widthD() / beltSpeed
                    + model_->getSetPoint<int>("nozSmallPieceDurationBoost")*1000));
            else
                msg.off = msg.on + std::chrono::microseconds(
                    (int)(subimg.widthD() / beltSpeed
                     + model_->getSetPoint<int>("nozLargePieceDurationBoost")*1000));

//          double stagger = model_->getSetPoint<double>("nozzleStagger");	// used for systems with staggered nozzles
//	    msg.on_stagger = msg.on + std::chrono::microseconds((int)(stagger/beltSpeed));
//	    msg.off_stagger = msg.off + std::chrono::microseconds((int)(stagger/beltSpeed));

            // std::cout << "msg " << std::chrono::duration_cast<std::chrono::milliseconds>(msg.off - msg.on).count() << std::endl;
            msgs.push_back(msg);
        }
        pushNozAct(msgs);
    }
}

void VisionSystem::pushNozAct(const std::vector<NozAct> &msgs)
{
    std::lock_guard<std::mutex> lk(nozLock);
    for(auto & act : msgs)
        nozActQueue.push(act);
}

void VisionSystem::popNozAct()
{
    if(!nozActQueue.empty())
    {
        if(nozLock.try_lock())
        {
            while(!nozActQueue.empty())
            {
                auto &act = nozActQueue.front();
                for(auto & i : act.sols){
//                if(i%2){					// used for systems with staggered nozzles
//		               onTimes[i-1].push(nozActQueue.front().on_stagger);
//                     offTimes[i-1].push(nozActQueue.front().off_stagger);
//		          }else{
//  if we overlap images reject extra entries for same piece
//					if (!offTimes[i - 1].empty()) {
//						if (offTimes[i - 1].back() > nozActQueue.front().on) continue;  // don't store multiple times for same piece
//					}
					onTimes[i-1].push(nozActQueue.front().on);
                    offTimes[i-1].push(nozActQueue.front().off);
// 		          }
                }
                nozActQueue.pop();
            }
            nozLock.unlock();
        }
    }
}

void VisionSystem::nozLoop()
{
    while(classLoop_)
    {
        popNozAct();
        auto now = std::chrono::system_clock::now();
        for(size_t i = 0; i < onTimes.size(); ++i)
        {
            if(!onTimes[i].empty())
            {
                auto &on = onTimes[i].front();
                if(now >= on)
                {
                    daq_->setSolenoid(i+1, true);
                    onCounts[i]++;
                    onTimes[i].pop();
                }
            }
        }
        for(size_t i = 0; i < offTimes.size(); ++i)
        {
            if(!offTimes[i].empty())
            {
                auto &off = offTimes[i].front();
                if(now >= off)
                {
                    if(onCounts[i] == 1)
                    {
                        daq_->setSolenoid(i+1, false);
                    }
                    offTimes[i].pop();
                    onCounts[i]--;
                }
            }
        }
    }
}

void VisionSystem::v_classify(cv::cuda::GpuMat &d_img, std::vector<cv::Rect> &pieces, Classifications &classes)
{
    // std::cout<<"the v_classify function"<<std::endl;
    classify(d_img, pieces, classes);
}

std::pair<cv::Mat, int> VisionSystem::labelImage(cv::Mat img, bool isbmp)
{
    Segmenter seg(controller_);
    auto start = sysNow();
    v_init_classifier();
    cv::Mat img_boxes = img.clone();
    cv::cuda::GpuMat d_img;
    // if(isbmp){
    //     cv::Mat imgTemp = VisionSystem::jpegEncodeDecode(img);
    //     d_img.upload(imgTemp);
    // }
    // else{
        d_img.upload(img);
    // }

    std::vector<cv::Rect> pieces;
    // cv::cuda::cvtColor(d_img, d_img, CV_RGB2BGR);
    d_img.download(img_boxes);
    seg.segment(d_img, pieces, img_boxes, false);

    Classifications classes;
    for(auto &rect : pieces)
    {
        Scrap scrap;
        scrap.rectP = cv::Rect2d(rect.x, rect.y, rect.width, rect.height);
 	std::cout << "x: " << rect.x << " y: " << rect.y << " w: " << rect.width << " h: " << rect.height << std::endl;  // dave debug
        scrap.rectD = scrap.rectP;
        classes.push_back(scrap);
    }

    //classify
    if(pieces.size())
        v_classify(d_img, pieces, classes);
    else
        return std::make_pair(img_boxes, 0);

    for(size_t i = 0; i < pieces.size(); i++)
    {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << classes[i].probClass0;
        std::string class_label(ss.str());
        ss.str(std::string());
        ss << std::fixed << std::setprecision(2) << classes[i].probClass1;
        class_label += ", " + ss.str();
        putText(img_boxes, class_label.c_str(), pieces[i].tl() + cv::Point(0, -10), cv::FONT_HERSHEY_TRIPLEX, 1.4, cv::Scalar(255, 0, 0), 2, cv::LINE_8, false);
        std::cout << class_label << std::endl;   
    }
    int elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(sysNow() - start).count();
    return std::make_pair(img_boxes, elapsed);
}

cv::Mat VisionSystem::jpegEncodeDecode(cv::Mat img)
{
    std::vector<uchar> buff;
    std::vector<int> param(2);
    param[0] = cv::IMWRITE_JPEG_QUALITY;
    param[1] = 75;
    cv::imencode(".jpg", img, buff, param);
    return cv::imdecode(buff, cv::IMREAD_COLOR);
}
