#include "stdio.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <future>

#define sysNow() std::chrono::system_clock::now()

#define RESIZE_MAX_HEIGHT 560
#define RESIZE_MAX_WIDTH 750

int main(int argc, char *argv[])
{
    if(argc < 2){
        std::cout << "please include image to normalize" << std::endl;
        return 1;
    }
    cv::Mat h_background_img = cv::imread("background_750x750.bmp");
    cv::cuda::GpuMat d_background_img;
    d_background_img.upload(h_background_img);
    cv::cuda::GpuMat d_dest = d_background_img.clone();
    cv::cuda::GpuMat d_dest_roi = d_dest;

    cv::Mat h_img1 = cv::imread(argv[1]);
    cv::cuda::GpuMat d_img1;
    d_img1.upload(h_img1);
    int width = d_img1.cols;
    int height = d_img1.rows;

    // crop image if it is too big
    // check width
    cv::Rect2i dest_roi;
    int x = width - RESIZE_MAX_WIDTH;
    int y = height - RESIZE_MAX_HEIGHT;
    int dx = x / 2;
    int dy = y / 2;
    cv::Mat h_img;
    if (x >= 0 && y >= 0)
    {
        std::cout << "too big, cropping" << std::endl;
        cv::Rect2i src_roi(dx, dy, RESIZE_MAX_WIDTH, RESIZE_MAX_HEIGHT);
        d_img1 = cv::cuda::GpuMat(d_img1, src_roi);
        d_img1.download(h_img);
        cv::imwrite("resized.jpeg", h_img);
        return 0;
    }
    else if (x > 0)
    {
        std::cout << "too long, cropping, and overlaying" << std::endl;
        cv::Rect2i src_roi(dx, 0, RESIZE_MAX_WIDTH, d_img1.rows);
        d_img1 = cv::cuda::GpuMat(d_img1, src_roi);
        dest_roi = cv::Rect2i(0, abs(dy), RESIZE_MAX_WIDTH, d_img1.rows);
        d_dest_roi = d_dest(dest_roi);
        d_img1.copyTo(d_dest_roi);
    }
    else if (y > 0)
    {
        std::cout << "too tall, cropping, and overlaying" << std::endl;
        cv::Rect2i src_roi(0, dy, d_img1.cols, RESIZE_MAX_HEIGHT);
        d_img1 = cv::cuda::GpuMat(d_img1, src_roi);
        dest_roi = cv::Rect2i(abs(dx), 0, d_img1.cols, RESIZE_MAX_HEIGHT);
        d_dest_roi = d_dest(dest_roi);
        d_img1.copyTo(d_dest_roi);
    }
    else
    {
        std::cout << "too small, overlaying" << std::endl;
        dest_roi = cv::Rect2i(abs(dx), abs(dy), d_img1.cols, d_img1.rows);
        d_dest_roi = d_dest(dest_roi);
        d_img1.copyTo(d_dest_roi);
    }
    d_dest.download(h_img);
    // cv::imshow("resized", h_img);
    cv::imwrite("resized.jpeg", h_img);
    return 0;
}
