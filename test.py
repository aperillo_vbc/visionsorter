#!/usr/bin/python3
import random
import time
import datetime
import argparse
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
import scipy.misc

parser = argparse.ArgumentParser()
parser.add_argument("path", help="a directory containing images you wish to seperate train on")
parser.add_argument("model", help="model which makes the predictions")
parser.add_argument('-c','--classes', nargs='+', help='List the classes you wish to train on, default is whatever os.listdir returns in working directory', required=True)

args = parser.parse_args()

path = args.path

# classes = [dir for dir in os.listdir(path) if os.path.isdir(os.path.join(path, dir))]
if args.classes is not None:
    classes = args.classes

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.regularizers import l2
from keras.utils import np_utils
from keras import backend as K
from keras.callbacks import ModelCheckpoint
from keras.applications import inception_v3
from keras.preprocessing import image

model = load_model(args.model)

img_rows, img_cols = 299, 299
shape = (img_rows, img_cols)

predictions = np.zeros((len(classes), len(classes)))
for j, _class in enumerate(classes):
    for imgFile in glob.glob(os.path.join(path, _class, "*.bmp")):
        img = scipy.misc.imread(imgFile)
        img = scipy.misc.imresize(img, shape, interp='nearest')
        img = img / 255.
        imgs = [img]
        probabilities = model.predict(np.array(imgs), batch_size=1)
        i = np.argmax(probabilities[0])
        predictions[i,j] += 1
        if i != j:
            probStr = ""
            for prob in probabilities[0]:
                probStr += str(prob) + " "
            print(probStr+imgFile)

print(predictions)

totals = np.sum(predictions, axis=0)
accuracies = np.zeros(len(classes))
for j, _class in enumerate(classes):
     accuracies[j] = predictions[j,j] / totals[j]
     for i, _class in enumerate(classes):
         predictions[i,j] = predictions[i,j] / totals[j]
print("accuracies: ", accuracies)
print(predictions)

import pandas as pd
import seaborn
df_cm = pd.DataFrame(predictions, index=classes, columns=classes)
seaborn.heatmap(df_cm, annot=True, fmt='.2f', cmap="Blues")
plt.show()
# input("Press Enter to continue...")
