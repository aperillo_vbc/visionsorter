#!/usr/bin/python3
from scipy import ndimage
import numpy as np
import os
import sys
from shutil import copyfile
import random
import time
import datetime
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("path", help="a directory containing images you wish to seperate train on")
parser.add_argument("-t", "--target", help="an optional target directory to store the generated model files, defaults to path directory")
parser.add_argument("-b", "--batch", type=int, help="an optional batch size, default is 32")
parser.add_argument("-m", "--model", help="an optional model to start from")
parser.add_argument('-c','--classes', nargs='+', help='List the classes you wish to train on, default is whatever os.listdir returns in working directory')
parser.add_argument('-r','--rotate', type=int, help="an optional range of rotation, default is 180")
parser.add_argument('-e','--epochs', type=int, help="an optional number of epochs to train, default is 100")
parser.add_argument('--no_horizontal_flip', action="store_false", help="horizontally flip the images, default is true")
parser.add_argument('-v','--no_vertical_flip', action="store_false", help="vertically flip the images, default is true")
parser.add_argument('--width_shift', type=float, help="the range to shift images on the X axis, default is 0.25")
parser.add_argument('--height_shift', type=float, help="the range to shift images on the Y axis, default is 0.25")
parser.add_argument("-s", "--no_shuffle", action="store_false", help="turn data shuffle off")

args = parser.parse_args()

path = args.path

target = path
if args.target is not None:
    target = args.target

batch_size = 16
if args.batch is not None:
    batch_size = args.batch
epochs = 100
if args.epochs is not None:
    epochs = args.epochs
rotate = 180
if args.rotate is not None:
    rotate = args.rotate
width_shift = 0.25
if args.width_shift is not None:
    width_shift = args.width_shift
height_shift = 0.25
if args.height_shift is not None:
    height_shift = args.height_shift
horizontal_flip = args.no_horizontal_flip
vertical_flip = args.no_vertical_flip

classes = [dir for dir in os.listdir(path) if os.path.isdir(os.path.join(path, dir))]
if args.classes is not None:
    classes = args.classes

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.regularizers import l2
from keras.utils import np_utils
from keras import backend as K
from keras.callbacks import ModelCheckpoint
from keras.applications import inception_v3
from keras.preprocessing import image

model = None
if args.model is not None:
    model = load_model(args.model)
else:
    base_model = inception_v3.InceptionV3(weights='imagenet', include_top=False)
    for layer in base_model.layers[:5]:
       layer.trainable = False
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)
    predictions = Dense(len(classes), activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
    from keras.optimizers import SGD
    model.compile(optimizer=SGD(lr=0.0001, momentum=0.9), loss='categorical_crossentropy', metrics=["accuracy"])

datagen = ImageDataGenerator(rotation_range=rotate,
                             horizontal_flip=horizontal_flip,
                             vertical_flip=vertical_flip,
                             width_shift_range=width_shift,
                             height_shift_range=height_shift,
                             rescale=1./255.,
                             fill_mode='nearest')

img_rows, img_cols = 299, 299
shape = (img_rows, img_cols)

train_generator = datagen.flow_from_directory(
        directory=path,
        target_size=shape,
        color_mode='rgb',
        classes=classes,
        batch_size=batch_size,
        shuffle=True)

#Add checkpoint to save best model
filepath = os.path.join(target, "V3")
for _class in classes:
    filepath += "_" + _class
filepath += "_" + datetime.datetime.now().strftime('%I-%M%p_%d-%b-%Y') + "_{epoch:02d}-{loss:.3f}-{acc:.4f}"  + ".hdf5"

checkpoint = ModelCheckpoint(filepath, verbose=1)
callbacks_list = [checkpoint]

total = 0
for _class in classes:
    total += len(os.listdir(os.path.join(path, _class)))

print("Training on " + str(total) + " images.")

hist = model.fit_generator(generator=train_generator,
                           steps_per_epoch=total/batch_size,
                           epochs=epochs,
                           verbose=1,
                           callbacks=callbacks_list)
