#ifndef ASTRA_CAMERA_STREAM_H
#define ASTRA_CAMERA_STREAM_H

#include <QTimer>
#include <QPixmap>
#include <QLabel>
#include "Controller.h"

class AstraCamStream : public QObject{
    Q_OBJECT

public:
    AstraCamStream(QObject *parent = 0) : QObject(parent){}
    ~AstraCamStream(){
    }
public slots:
    void startTimers(){
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(captureImage()));
        timer->start(50);
    }
    void stopTimers(){
    }
private slots:
    void captureImage(){
        emit imgCaptured();
    }
signals:
    void imgCaptured();

private:
    QTimer *timer;
};

#endif
