#ifndef CAMERA_STREAM_H
#define CAMERA_STREAM_H
#include "stdio.h"

#include <QTimer>
#include <QPixmap>
#include <QLabel>
#include "Controller.h"

class CameraStream : public QObject{
    Q_OBJECT

public:
    CameraStream(QObject *parent = 0, baslerCam::CameraPtr _cam = nullptr, QLabel *_label = nullptr) : QObject(parent), cam(_cam), label(_label){}
    ~CameraStream(){
        stopTimers();
        cam->stopGrab();
    }
    const QPixmap &getImage(){return img;}
public slots:
    void startTimers(){
        timer = new QTimer(this);
        cam->startGrab();
        connect(timer, SIGNAL(timeout()), this, SLOT(captureImage()));
        timer->start(250);
    }
    void stopTimers(){
	timer->stop();
    }
private slots:
    void captureImage(){
	try{        
  	   if(cam != nullptr)
           {
           	cv::Mat _img = cam->trigger_block_rgb();
           	QImage image(_img.data, _img.cols, _img.rows, _img.step, QImage::Format_RGB888);
           	img = QPixmap::fromImage(image);
           }
           emit imgCaptured(label, &img);
	}
    	catch (const Pylon::GenericException & e)
    	{
     	   stopTimers();
           cam->stopGrab();
           return;
    	}
    }
signals:
    void imgCaptured(QLabel*, QPixmap*);

private:
    baslerCam::CameraPtr cam;
    QLabel *label;
    QTimer *timer;
    QPixmap img;
};

#endif
