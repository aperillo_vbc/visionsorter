#include "KeyPad.h"
#include "ui_KeyPad.h"
#include <QShowEvent>
#include <QTimer>

KeyPad::KeyPad(QWidget *parent, const QString &passcode, const QString &initialCode) :
    QDialog(parent),
    ui(new Ui::KeyPad),
    passcode_(passcode)
{
    ui->setupUi(this);
    ui->code->setText(initialCode);
    // when the developer has set the initial code to equal the passcode,
    // this will let them skip past the login
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(checkCode()));
    timer->start(1000);
}

KeyPad::~KeyPad()
{
    delete ui;
}

void KeyPad::checkCode()
{
    QString tcode = ui->code->text();
    if(tcode == passcode_)
        accept();
}

void KeyPad::checkCode(const QString &str)
{
    if(str == "C")
        reject();

    QString tcode = ui->code->text();
    if(str == "D")
        tcode.chop(1);
    else if(str != "")
        tcode.append(str);

    if(tcode == passcode_)
        accept();
    else
        ui->code->setText(tcode);
}

void KeyPad::on_pushButton_0_pressed(){checkCode("0");}
void KeyPad::on_pushButton_1_pressed(){checkCode("1");}
void KeyPad::on_pushButton_2_pressed(){checkCode("2");}
void KeyPad::on_pushButton_3_pressed(){checkCode("3");}
void KeyPad::on_pushButton_4_pressed(){checkCode("4");}
void KeyPad::on_pushButton_5_pressed(){checkCode("5");}
void KeyPad::on_pushButton_6_pressed(){checkCode("6");}
void KeyPad::on_pushButton_7_pressed(){checkCode("7");}
void KeyPad::on_pushButton_8_pressed(){checkCode("8");}
void KeyPad::on_pushButton_9_pressed(){checkCode("9");}

void KeyPad::on_pushButton_Delete_pressed(){(checkCode("D"));}
void KeyPad::on_pushButton_Cancel_pressed(){(checkCode("C"));}
