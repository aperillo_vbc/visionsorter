#ifndef KEYPAD_H
#define KEYPAD_H

#include <QDialog>

namespace Ui {
class KeyPad;
}

class KeyPad : public QDialog
{
    Q_OBJECT

public:
    explicit KeyPad(QWidget *parent = 0, const QString &passcode = "", const QString &initialCode = "");
    ~KeyPad();

private slots:
    void on_pushButton_0_pressed();
    void on_pushButton_1_pressed();
    void on_pushButton_2_pressed();
    void on_pushButton_3_pressed();
    void on_pushButton_4_pressed();
    void on_pushButton_5_pressed();
    void on_pushButton_6_pressed();
    void on_pushButton_7_pressed();
    void on_pushButton_8_pressed();
    void on_pushButton_9_pressed();

    void on_pushButton_Delete_pressed();
    void on_pushButton_Cancel_pressed();
    void checkCode();
private:
    void checkCode(const QString &str);
    Ui::KeyPad *ui;
    QString passcode_;
    QString currentCode_;
};

#endif // KEYPAD_H
