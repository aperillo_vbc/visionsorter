#include "OperatorWindow.h"
#include "ui_OperatorWindow.h"
#include <QPainter>
#include "StatusCircleWidget.h"
#include <iostream>
#include "KeyPad.h"
#include <QComboBox>
#include <QSettings>
#include <QTimer>
#include <QImage>
#include <QTextBrowser>
#include <QLineEdit>
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <QMessageBox>
#include <QFileDialog>
#include <chrono>
#include <QThread>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <stdlib.h>
#include "CameraStream.h"
#include "AstraCamStream.h"
#include <gperftools/profiler.h>

OperatorWindow::OperatorWindow(QWidget *parent, VisionSystemPtr vision, ControllerPtr controller, RecipesPtr recipes) :
    QMainWindow(parent),
    ui(new Ui::OperatorWindow),
    vision_system_(vision),
    controller_(controller),
    model_(controller->getModel()),
    recipes_(recipes)
{
    ui->setupUi(this);
    qRegisterMetaType<VS_STATE>( "VS_STATE" );
    ui->recipeComboBox->blockSignals(true);
    for(auto &recipe : recipes_->getRecipes())
        ui->recipeComboBox->addItem(recipe);
    ui->recipeComboBox->setCurrentText(recipes_->getRecipe());
    ui->recipeComboBox->blockSignals(false);



    airNozzleButtons.resize(24);
    airNozzleLabels.resize(24);
    for(size_t i = 0; i < 24; ++i)
    {
        airNozzleButtons[i] = new SquareHollowButton;
        airNozzleLabels[i] = new QLabel;
        auto button = airNozzleButtons[i];
        auto label = airNozzleLabels[i];
        size_t k = i + 1;
        label->setText(QString::number(k));
        if(i > 15)
        {
            ui->airNozzleLayout3->addWidget(button, i, 0);
            ui->airNozzleLayout3->addWidget(label, i, 1);
        }
        else if(i > 7)
        {
            ui->airNozzleLayout2->addWidget(button, i, 0);
            ui->airNozzleLayout2->addWidget(label, i, 1);
        }
        else
        {
            ui->airNozzleLayout1->addWidget(button, i, 0);
            ui->airNozzleLayout1->addWidget(label, i, 1);
        }

        QObject::connect(button, &SquareHollowButton::pressed, this, [this, i] {
                    setSolenoid(i);
                 });
    }
    connect(this, SIGNAL(stateUpdate(VS_STATE)), this, SLOT(updateState(VS_STATE)));
    connect(this, SIGNAL(logUpdate(QString)), this, SLOT(updateLog(QString)));
//    connect(this, SIGNAL(visionImageUpdate(void)), this, SLOT(updateVisionImage(void)));
    connect(this, SIGNAL(classLogUpdate(QString)), this, SLOT(updateClassLog(QString)));
    connect(this, SIGNAL(classImageUpdate()), this, SLOT(updateClassImage()));
    connect(this, SIGNAL(error(QString)), this, SLOT(throwErrorBox(QString)));

    try{
        initializeValues();
    }
    catch(const std::invalid_argument &e){
        log(e.what());
    }
    catch(const std::runtime_error &e){
        log(e.what());
    }
    controller_->enableRelayPower();
    ui->debugTextBrowse->append("Welcome, the sorter is in standby.");
    synchro_record_ = nullptr;
    ui->recordLabel1->setScaledContents(true);
    ui->recordLabel2->setScaledContents(true);

    qCam = nullptr;
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    if(cameras.count() > 0)
    {
        std::cout << "UVC Cameras found: " << QCameraInfo::availableCameras().count() << std::endl;
        qCam = new QCamera(cameras[0]);
        qCamView = new QCameraViewfinder(this);
        qCamCapture = new QCameraImageCapture(qCam, this);
        qCam->setViewfinder(qCamView);
        ui->lowResCam->addWidget(qCamView);
        qCam->start();
    }
    else
    {
        std::cout << "No UVC cameras found, checking for OpenNI" << std::endl;
        astraCam_ = AstraCamPtr(new AstraCam);
        try{
            astraCam_->init();
            // Low res camera on operator screen
            AstraCamStream *stream = new AstraCamStream(0);
            aCamStreamThread = new QThread();
//            ui->lowResCam->
            stream->moveToThread(aCamStreamThread);
            connect(aCamStreamThread, SIGNAL(started()), stream, SLOT(startTimers()));
            connect(aCamStreamThread, SIGNAL(finished()), stream, SLOT(deleteLater()));
            connect(aCamStreamThread, SIGNAL(finished()), aCamStreamThread, SLOT(deleteLater()));
            connect(stream, SIGNAL(imgCaptured()), this, SLOT(updateLowResImage()));
            aCamStreamThread->start();
        }
        catch(const std::runtime_error& e){
            std::cout << e.what() << std::endl;
        }
    }
}

void OperatorWindow::updateLowResImage()
{
    if(!astraCam_->colorStreamIsValid() || ui->stackedWidget->currentIndex() != 0)
        return;
    uchar *data = (uchar*)astraCam_->getColorFrameData();
    QImage img(data, 640, 480, QImage::Format_RGB888);

//    ui->lowResCamLabel->setScaledContents(true);
//    ui->lowResCamLabel->setPixmap(QPixmap::fromImage(img));
}

void OperatorWindow::updateImageColor(QLabel *label, cv::Mat img)
{
    if(ui->stackedWidget->currentIndex() == 0)
        return;
    QImage image(img.data, img.cols, img.rows, img.step, QImage::Format_RGB888);
    label->setScaledContents(true);
    label->setPixmap(QPixmap::fromImage(image));
}

OperatorWindow::~OperatorWindow()
{
    QSettings settings("UHV", "Vision_Sorter");
    settings.beginGroup("recipes");
    settings.setValue("recipe", ui->recipeComboBox->currentText());
    settings.setValue("VSactive",0);
    settings.endGroup();

// ProfilerStop();

    if(qCam != nullptr)
        qCam->stop();
    delete synchro_record_;
    delete ui;
}


void OperatorWindow::updateState(VS_STATE state)
{
    if(state == VS_STATE::VS_STANDBY)
    {
        vision_system_->stopLoops();
        ui->machineStatus->setText("<i>Standby</i>");
        ui->startButton->setFlat(false);
    }
    else if(state == VS_STATE::VS_PAUSE)
    {
        if(!vision_system_->isPaused())
            vision_system_->pauseLoops();
        ui->machineStatus->setText("<i>Paused</i>");
        log("Vision Loop is Paused");
        ui->startButton->setFlat(false);
    }
    else if(state == VS_STATE::VS_INIT)
    {
        ui->machineStatus->setText("<i>Initializing</i>");
    }
    else
    {
        ui->machineStatus->setText("<i>Running</i>");
    }
}

void OperatorWindow::log(std::string str)
{
    emit logUpdate(QString::fromStdString(str));
}

void OperatorWindow::updateLog(QString arg)
{
    ui->debugTextBrowse->append(arg);
}

void OperatorWindow::visionLoopCallback(cv::Mat img, std::string status, VS_STATE state)
{
    segmentedImage_ = img;
    emit visionImageUpdate();
    if(state == VS_STATE::VS_STANDBY)
        status += std::string("Vision Loop is Stopped");
    log(status);
    emit stateUpdate(state);
}

void OperatorWindow::on_startButton_pressed()
{
    if(ui->recordImagesButton->isChecked())
        vision_system_->setRecord(true);
    else
        vision_system_->setRecord(false);

    log("Vision Loop is Initializing...");
    emit stateUpdate(VS_STATE::VS_INIT);
    if(vision_system_->isPaused()){
        vision_system_->resumeLoops();
        emit stateUpdate(VS_STATE::VS_RUN);
        log("Vision Loop is Running.");
    }
    else{
        QFuture<void> future = QtConcurrent::run(
                    this->vision_system_.get(),
                    &VisionSystem::startLoops,
                    std::bind(&OperatorWindow::visionLoopCallback, this,
                              std::placeholders::_1,
                              std::placeholders::_2,
                              std::placeholders::_3));
    }
}

void OperatorWindow::on_stopButton_released()
{
    if(!vision_system_->isPaused())
        vision_system_->pauseLoops();
    ui->startButton->setFlat(false);
    emit stateUpdate(VS_STATE::VS_PAUSE);
    log("Vision Loop is Paused");
}

void OperatorWindow::on_maintenanceModeButton_released()
{
    KeyPad k(0, QString::number(model_->getSetPoint<int>("maintenancePass")), "1234");
    if(k.exec() && model_->getSetPoint<bool>("mode") != true)
    {
        model_->setSetPoint<bool>("mode", true);
        ui->machineMode->setText("Maintenance\nMode");
    }
    else
        return;
}

void OperatorWindow::on_operatorModeButton_released()
{
    model_->setSetPoint<bool>("mode", false);
    ui->machineMode->setText("Operator\nMode");
}

void OperatorWindow::on_acButton_pressed()
{
   if(ui->acButton->isFlat())
   {
       controller_->setAC(true);
       ui->acButton_2->setFlat(true);
   }
   else
   {
       controller_->setAC(false);
       ui->acButton_2->setFlat(false);
   }
}

void OperatorWindow::on_lightsButton_pressed()
{
   if(ui->lightsButton->isFlat())
   {
       controller_->setVisionLights(true);
       ui->lightsButton_2->setFlat(true);
   }
   else
   {
       controller_->setVisionLights(false);
       ui->lightsButton_2->setFlat(false);
   }
}

void OperatorWindow::on_conveyorButton_pressed()
{
    if(ui->conveyorButton->isFlat())
    {
        controller_->setConveyor(true);
        ui->conveyorButton_2->setFlat(true);
    }
    else
    {
        controller_->setConveyor(false);
        ui->conveyorButton_2->setFlat(false);
    }
}

void OperatorWindow::on_feederButton_pressed()
{
   if(ui->feederButton->isFlat())
   {
       controller_->setFeed(true);
       ui->feederButton_2->setFlat(true);
   }
   else
   {
       controller_->setFeed(false);
       ui->feederButton_2->setFlat(false);
   }
}
void OperatorWindow::on_acButton_2_pressed(){
    if(ui->acButton_2->isFlat())
    {
        controller_->setAC(true);
        ui->acButton->setFlat(true);
    }
    else
    {
        controller_->setAC(false);
        ui->acButton->setFlat(false);
    }
}
void OperatorWindow::on_lightsButton_2_pressed(){
    if(ui->lightsButton_2->isFlat())
    {
        controller_->setVisionLights(true);
        ui->lightsButton->setFlat(true);
    }
    else
    {
        controller_->setVisionLights(false);
        ui->lightsButton->setFlat(false);
    }
}
void OperatorWindow::on_conveyorButton_2_pressed(){
    if(ui->conveyorButton_2->isFlat())
    {
        controller_->setConveyor(true);
        ui->conveyorButton->setFlat(true);
    }
    else
    {
        controller_->setConveyor(false);
        ui->conveyorButton->setFlat(false);
    }
}
void OperatorWindow::on_feederButton_2_pressed(){
    if(ui->feederButton_2->isFlat())
    {
        controller_->setFeed(true);
        ui->feederButton->setFlat(true);
    }
    else
    {
        controller_->setFeed(false);
        ui->feederButton->setFlat(false);
    }
}

void OperatorWindow::on_engineerModeButton_released()
{
    KeyPad k(0, QString::number(model_->getSetPoint<int>("engineerPass")), "1234");
    if(!k.exec())
    {
        model_->setSetPoint<bool>("mode", false);
        return;
    }
    else
    {
        model_->setSetPoint<bool>("mode", true);
        ui->stackedWidget->setCurrentIndex(1);
        ui->tabWidget->setCurrentIndex(1);
    }
}

void OperatorWindow::on_recipeComboBox_currentIndexChanged(const QString &arg1)
{
    try{
        recipes_->loadRecipe(arg1);
    }
    catch(const std::runtime_error &e){
        emit logUpdate(QString::fromStdString(e.what()));
        return;
    }
    initializeValues();
}

void OperatorWindow::updateAllStatus()
{
    if(model_->getLowerBound<double>("24V") < model_->getCurrentValue<double>("24V") )
        ui->_24VStatusInd->setStatus(true);
    else
        ui->_24VStatusInd->setStatus(false);

    if(model_->getLowerBound<double>("5V") < model_->getCurrentValue<double>("5V") )
        ui->_5VStatusInd->setStatus(true);
    else
        ui->_5VStatusInd->setStatus(false);

    if(model_->getLowerBound<double>("CPA") < model_->getCurrentValue<double>("CPA") )
        ui->CPAStatusInd->setStatus(true);
    else
        ui->CPAStatusInd->setStatus(false);

    if(controller_->inBounds<double>("ctemp"))
        ui->TempStatusInd->setStatus(true);
    else
        ui->TempStatusInd->setStatus(false);

    if(model_->getLowerBound<double>("inductive1") > model_->getCurrentValue<double>("inductive1") )
        ui->belt1StatusInd->setStatus(true);
    else
        ui->belt1StatusInd->setStatus(false);

//    if(model_->getLowerBound<double>("inductive2") < model_->getCurrentValue<double>("inductive2") )
//        ui->belt2StatusInd->setStatus(true);
//    else
//        ui->belt2StatusInd->setStatus(false);

    ui->_5VCurrentValue->setText(QString::number(model_->getCurrentValue<double>("5V")));
    ui->_24VCurrentValue->setText(QString::number(model_->getCurrentValue<double>("24V")));
    ui->CPACurrentValue->setText(QString::number(model_->getCurrentValue<double>("CPA")));
    ui->tempCurrentValue->setText(QString::number(model_->getCurrentValue<double>("ctemp")));

    ui->doorILStatusInd->setStatus(model_->getCurrentValue<bool>("doorIlock"));
    ui->otherILStatusInd->setStatus(model_->getCurrentValue<bool>("otherIlock"));

    ui->inductiveSensor1CurrentValue->setText(QString::number(model_->getCurrentValue<double>("inductive1")));
    ui->inductiveSensor2CurrentValue->setText(QString::number(model_->getCurrentValue<double>("inductive2")));

    // scrap count
    int class0Count = model_->getCurrentValue<int>("class0Count");
    int class1Count = model_->getCurrentValue<int>("class1Count");
    int totalScrap = class0Count + class1Count;
    std::string class0Alias = model_->getMeta("class0Alias");
    std::string class1Alias = model_->getMeta("class1Alias");
    int width = class0Alias.size() > class1Alias.size() ? class0Alias.size() : class1Alias.size();
    double percent0, percent1;
    if(totalScrap == 0)
    {
        percent0 = 0;
        percent1 = 0;
    }
    else
    {
        percent0 = (double)class0Count / (double)totalScrap * 100.0;
        percent1 = (double)class1Count / (double)totalScrap * 100.0;
    }
    ui->sorterDataTextBrowser->setText(QString("Counts:\n%1: %2\n%3: %4\nPercentage:\n%1: %5\n%3: %6")
                                   .arg(QString::fromStdString(class0Alias),width, ' ')
                                   .arg(QString::number(class0Count))
                                   .arg(QString::fromStdString(class1Alias),width, ' ')
                                   .arg(QString::number(class1Count))
                                   .arg(QString::number(percent0))
                                   .arg(QString::number(percent1)));
    ui->scrapDataTextBrowser->setText(QString("Total Count: %1\n")
                                    .arg(QString::number(totalScrap)));

    ui->beltSpeedCurrentValue->setText(QString::number(model_->getCurrentValue<double>("conveyorSpeedEst1")));

    ui->currentRecipeEngineerLineEdit->setText(recipes_->getRecipe());
}

void OperatorWindow::on_voltageControl_sliderReleased()
{
    double v = (double)ui->voltageControl->value() / 100;
    v = v * 5;
    v = std::abs(v);
    controller_->setConveyorSpeed(v);
}

void OperatorWindow::on_inclineVoltageControl_sliderReleased()
{
    double v = (double)ui->inclineVoltageControl->value() / 100;
    v = v * 5;
    v = std::abs(v);
    controller_->setInclineConveyorSpeed(v);
}

void OperatorWindow::on_creepVoltageControl_sliderReleased()
{
    double v = (double)ui->creepVoltageControl->value() / 100;
    v = v * 5;
    v = std::abs(v);
    controller_->setCreepConveyorSpeed(v);
}

void OperatorWindow::on_HD75voltageControl_sliderReleased()
{
    double v = (double)ui->HD75voltageControl->value() / 100;
    v = v * 5;
    v = std::abs(v);
    controller_->setHD75Speed(v);
}

void OperatorWindow::setSolenoid(size_t i)
{
	int offset = 0;
	if(ui->NozBank2Radio->isChecked())
    		offset = 24;
   	if(airNozzleButtons[i]->isFlat())
        	controller_->setSolenoid(i+offset+1, true);
    	else
        	controller_->setSolenoid(i+offset+1, false);
}

void OperatorWindow::on_pushButton_clicked()
{
    this->close();
}

void OperatorWindow::on_speedSensorFactorbox_editingFinished()
{
    model_->setSetPoint<double>("speedSensorFactor",ui->speedSensorFactorbox->text().toDouble());
}

void OperatorWindow::initializeValues()
{
    ui->voltageControl->setValue((int)model_->getSetPoint<double>("conveyorV")/5.*100);
    ui->creepVoltageControl->setValue((int)model_->getSetPoint<double>("creepconveyorV")/5.*100);
    ui->inclineVoltageControl->setValue((int)model_->getSetPoint<double>("inclineconveyorV")/5.*100);
    ui->HD75voltageControl->setValue((int)model_->getSetPoint<double>("HD75V")/5.*100);

    ui->shadowThreshBox->setText(QString::number(model_->getSetPoint<int>("shadowThresh")));
    ui->shadowSlider->setValue(model_->getSetPoint<int>("shadowThresh"));
    ui->highlightThreshBox->setText(QString::number(model_->getSetPoint<int>("highlightThresh")));
    ui->highlightSlider->setValue(model_->getSetPoint<int>("highlightThresh"));
    ui->dilationSizeBox->setText(QString::number(model_->getSetPoint<int>("dilationSize")));
    ui->dilationSizeSlider->setValue(model_->getSetPoint<int>("dilationSize"));
    ui->dilationIterationsBox->setText(QString::number(model_->getSetPoint<int>("dilationIterations")));
    ui->dilationIterationsSlider->setValue(model_->getSetPoint<int>("dilationIterations"));
    ui->nozzleOffsetTimeLineEdit->setText(QString::number(model_->getSetPoint<int>("nozOffsetTime")));
    ui->nozzleOffsetTimeSlider->setValue(model_->getSetPoint<int>("nozOffsetTime"));
    ui->smallPieceBoostLineEdit->setText(QString::number(model_->getSetPoint<int>("nozSmallPieceDurationBoost")));
    ui->smallPieceBoostSlider->setValue(model_->getSetPoint<int>("nozSmallPieceDurationBoost"));
    ui->largePieceBoostLineEdit->setText(QString::number(model_->getSetPoint<int>("nozLargePieceDurationBoost")));
    ui->largePieceBoostSlider->setValue(model_->getSetPoint<int>("nozLargePieceDurationBoost"));
    ui->largePieceDelayLineEdit->setText(QString::number(model_->getSetPoint<int>("nozLargePieceDelay")));
    ui->largePieceDelaySlider->setValue(model_->getSetPoint<int>("nozLargePieceDelay"));
    ui->intersectPercentLineEdit->setText(QString::number(model_->getSetPoint<double>("neededIntersectPercent")*100));
    ui->intersectPercentSlider->setValue(model_->getSetPoint<double>("neededIntersectPercent")*100);
    ui->speedSensorFactorbox->setText(QString::number(model_->getSetPoint<double>("speedSensorFactor")));

    ui->minimumAreaBox->setText(QString::number(model_->getSetPoint<double>("pieceMinAreaIn")));
    ui->maximumAreaBox->setText(QString::number(model_->getSetPoint<double>("pieceMaxAreaIn")));
    ui->_24VLowerBound->setText(QString::number(model_->getLowerBound<double>("24V")));
    ui->_24VUpperBound->setText(QString::number(model_->getUpperBound<double>("24V")));
    ui->_5VLowerBound->setText(QString::number(model_->getLowerBound<double>("5V")));
    ui->_5VUpperBound->setText(QString::number(model_->getUpperBound<double>("5V")));
    ui->CPALowerBound->setText(QString::number(model_->getLowerBound<double>("CPA")));
    ui->CPAUpperBound->setText(QString::number(model_->getUpperBound<double>("CPA")));
    ui->tempLowerBound->setText(QString::number(model_->getLowerBound<double>("ctemp")));
    ui->tempUpperBound->setText(QString::number(model_->getUpperBound<double>("ctemp")));
    ui->inductiveSensor1LowerBound->setText(QString::number(model_->getLowerBound<double>("inductive1")));
    ui->inductiveSensor1UpperBound->setText(QString::number(model_->getUpperBound<double>("inductive1")));
    ui->inductiveSensor2LowerBound->setText(QString::number(model_->getLowerBound<double>("inductive2")));
    ui->inductiveSensor2UpperBound->setText(QString::number(model_->getUpperBound<double>("inductive2")));
    ui->fiducialSizeLineEdit->setText(QString::number(model_->getSetPoint<double>("fiducialSize")));
    ui->sidComboBox->blockSignals(true);
    std::vector<int> positionsFound;
    for(auto &cam : controller_->getCameras())
    {
        int position = model_->getCamSet<int>(cam->getID(), "position");
        if(position != -1)
            positionsFound.push_back(position);
        ui->sidComboBox->addItem(QString::fromStdString(cam->getID()));
    }
    if(positionsFound.size() == 1)
    {
        int position = positionsFound[0];
        for(auto &cam : controller_->getCameras())
        {
            if(position == model_->getCamSet<int>(cam->getID(), "position"))
                continue;
            if(position == 0)
                model_->setCamSet<int>(cam->getID(), "position", 1);
            else
                model_->setCamSet<int>(cam->getID(), "position", 0);
        }
    }
    if(positionsFound.size() == 0)
    {
        int count = 0;
        for(auto &cam : controller_->getCameras())
            model_->setCamSet<int>(cam->getID(), "position", count++);
    }
    ui->sidComboBox->blockSignals(false);
    int sidIndex = ui->sidComboBox->currentIndex();
    if(sidIndex != -1)
        ui->sidComboBox->setCurrentIndex(sidIndex);
    updateCameraSettingsView();

    ui->segmentBeforeRecordCheckBox->setChecked(model_->getSetPoint<int>("segmentBeforeRecord"));
    ui->matchBeltSpeedRecordCheckBox->setChecked(model_->getSetPoint<int>("matchBeltSpeedRecord"));
    ui->imageLengthLineEdit->setText(QString::number(model_->getSetPoint<double>("synchroImageLength")));
    ui->fpsLineEdit->setText(QString::number(model_->getSetPoint<double>("fps")));
}

void OperatorWindow::on_saveColorImageButton_1_released()
{
    auto cam = getCamera();
    cv::Mat img;
    try{
        img = controller_->getPictureBGR(cam);
    }
    catch(const std::runtime_error& e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    cv::imwrite(std::string("./data/") + ui->saveColorImageBox->text().toStdString(), img);
}

///////////////// Segmentation Engineering /////////////////
void OperatorWindow::setSegmentationImage(cv::Mat img)
{
    QImage image(img.data, img.cols, img.rows, img.step, QImage::Format_Grayscale8);
    ui->imageLabel_3->setScaledContents(true);
    ui->imageLabel_3->setPixmap(QPixmap::fromImage(image));
}

void OperatorWindow::on_segmentLoadPictureButton_released()
{
    QString newFileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "data", tr("Image Files (*.bmp *.jpeg *.jpg *.png)"));
    if(newFileName == "")
        return;
    segStruct.img = cv::imread(newFileName.toStdString());
    segStruct.cimg = segStruct.img.clone();
    cv::cvtColor(segStruct.img, segStruct.img, CV_BGR2GRAY);
    segStruct.hasImage = true;
    segStruct.hasBWImg = false;
    segStruct.hasdilimg = false;
    setSegmentationImage(segStruct.img);
    segStruct.segmenter = SegmenterPtr(new Segmenter(controller_, nullptr));
}

void OperatorWindow::on_takeGrayScalePictureButton_released()
{
    auto cam = getCamera();
    segStruct.segmenter = SegmenterPtr(new Segmenter(controller_, cam));
    cv::Mat img;
    try{
        img = controller_->getPictureBGR(cam);
        segStruct.cimg = img.clone();
        cv::cvtColor(img, img, CV_BGR2GRAY);
    	cv::circle(img,cv::Point(0,0),10.0,cv::Scalar(255,0,255),15,15);
    }
    catch(const std::runtime_error& e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    catch(const Pylon::GenericException & e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }

    segStruct.img = img.clone();
    segStruct.hasImage = true;
    segStruct.hasBWImg = false;
    segStruct.hasdilimg = false;
    setSegmentationImage(img);
}

void OperatorWindow::on_GetLuminanceValues_released()
{
    if(!segStruct.hasImage)
        return;
    cv::Mat img = segStruct.img.clone();
    segStruct.segmenter->getMeanValues(segStruct.img,img);
    setSegmentationImage(img);
}

void OperatorWindow::on_thresholdButton_released()
{
    if(!segStruct.hasImage)
        return;
    segStruct.segmenter->threshold(segStruct.img,segStruct.bwimg,true);
    segStruct.hasBWImg = true;
    setSegmentationImage(segStruct.bwimg);
}

void OperatorWindow::on_dilateButton_released()
{
    if(!segStruct.hasBWImg)
        return;
    segStruct.segmenter->dilate(segStruct.bwimg, segStruct.dilimg);
    segStruct.hasdilimg = true;
    setSegmentationImage(segStruct.dilimg);
}

void OperatorWindow::on_findContoursButton_released()
{
    if(!segStruct.hasdilimg)
        return;
    cv::Mat img = segStruct.img.clone();
    segStruct.segmenter->findContours(segStruct.dilimg, segStruct.pieces, img);
    setSegmentationImage(img);
}

void OperatorWindow::on_savePiecesButton_released()
{
    if(segStruct.pieces.empty())
        return;
    cv::Mat h_background_img = cv::imread("background_750x750.bmp");
    cv::cuda::GpuMat d_background;
    d_background.upload(h_background_img);
    cv::cuda::GpuMat d_img;
    d_img.upload(segStruct.cimg);
    for(size_t i = 0; i < segStruct.pieces.size(); ++i)
    {
        cv::cuda::GpuMat d_dest = d_background.clone();
        cv::cuda::GpuMat d_piece(d_img, segStruct.pieces[i]);
        int width = d_dest.cols;
        int height = d_dest.rows;
        segStruct.segmenter->crop(d_piece, width, height);
        cv::Mat h_img;
        d_piece.download(h_img);
        cv::imwrite(std::string("./data/crop_piece_")+std::to_string(i)+".jpg", h_img);
        segStruct.segmenter->overlayFrameCard(d_piece, d_dest, d_img, segStruct.pieces[i]);
        d_dest.download(h_img);

        cv::imwrite(std::string("./data/scrap/final_piece_")+std::to_string(rand())+".jpg", h_img);
    }
}

void OperatorWindow::on_intersectPercentSlider_valueChanged(int value)
{
    ui->intersectPercentLineEdit->setText(QString::number(value));
    model_->setSetPoint<double>("neededIntersectPercent", value / 100.);
}

void OperatorWindow::on_highlightSlider_valueChanged(int value)
{
    ui->highlightThreshBox->setText(QString::number(value));
    model_->setSetPoint<int>("highlightThresh", value);
}

void OperatorWindow::on_shadowSlider_valueChanged(int value)
{
    ui->shadowThreshBox->setText(QString::number(value));
    model_->setSetPoint<int>("shadowThresh", value);
}

void OperatorWindow::on_dilationIterationsSlider_valueChanged(int value)
{
    ui->dilationIterationsBox->setText(QString::number(value));
    model_->setSetPoint<int>("dilationIterations", value);
}

void OperatorWindow::on_dilationSizeSlider_valueChanged(int value)
{
    ui->dilationSizeBox->setText(QString::number(value));
    model_->setSetPoint<int>("dilationSize", value);
}

///////////////// Classification Engineering /////////////////
void OperatorWindow::on_classifyLoadPicture_released()
{
    QString newFileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "data", tr("Image Files (*.bmp *.jpeg *.jpg *.png)"));
    if(newFileName == "")
        return;
    QStringList list = newFileName.split('.');
    if(list.empty())
        return;
    classStruct.extension = list.last();
    classStruct.img = cv::imread(newFileName.toStdString());
    cv::cvtColor(classStruct.img, classStruct.img, CV_BGR2RGB);
    ui->classPicLineEdit->setText(newFileName);
    classStruct.hasImage = true;
    classStruct.hasClassImage = false;
    setClassImage(classStruct.img);
}

void OperatorWindow::on_snapClassPicture_released()
{
    auto cam = getCamera();
    if(cam==nullptr)
        return;
    cv::Mat img;
    try{
        img = controller_->getPictureRGB(cam);
    }
    catch(const Pylon::GenericException & e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    catch(const std::runtime_error& e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    ui->classPicLineEdit->setText("From Camera");
    classStruct.extension = "bmp";
    classStruct.img = img.clone();
    classStruct.hasImage = true;
    classStruct.hasClassImage = false;
    setClassImage(img);
}

void OperatorWindow::setClassImage(cv::Mat img)
{
    QImage image(img.data, img.cols, img.rows, img.step, QImage::Format_RGB888);
    ui->imageLabel_4->setScaledContents(true);
    ui->imageLabel_4->setPixmap(QPixmap::fromImage(image));
}

void OperatorWindow::updateClassLog(QString arg)
{
    ui->classifyTextBrowse->append(arg);
}


void OperatorWindow::throwErrorBox(QString error)
{
    QMessageBox errorBox;
    errorBox.warning(0, "Error", error);
}

void OperatorWindow::classifyClassPicture()
{
    if(!classStruct.hasImage)
        return;
    bool isbmp = classStruct.extension == "bmp" ? true : false;
    try{
        std::cout << "current device: " << cv::cuda::getDevice() << std::endl;
        cv::cuda::setDevice(0);
        int elapsed;
        std::tie(classStruct.classImg, elapsed) = vision_system_->labelImage(classStruct.img, isbmp);
        QString str = QString::number(elapsed);
        emit classLogUpdate(QString("Time for segmentation and classification: ") + str + " ms");
    }
    catch(const std::runtime_error &e)
    {
        emit error(e.what());
        return;
    }
    classStruct.hasClassImage = true;
    emit classImageUpdate();
}

void OperatorWindow::updateClassImage()
{
    setClassImage(classStruct.classImg);
}

void OperatorWindow::on_classifyClassPicture_released()
{
    if(!vision_system_->isClassifierInit())
    {
        emit classLogUpdate("Intitializing neural network, this could take a minute...");
        emit classLogUpdate("Ignore the first timing result after initialization, the GPU needs a warm up run.");
    }
    QFuture<void> future = QtConcurrent::run(this, &OperatorWindow::classifyClassPicture);
}

void OperatorWindow::on_classifySaveResultButtom_released()
{
    if(classStruct.hasClassImage)
    {
        cv::Mat img;
        cv::cvtColor(classStruct.classImg, img, CV_RGB2BGR);
        cv::imwrite(ui->classSavePicLineEdit->text().toStdString(), img);
    }
}

///////////////// Camera Calibration /////////////////
void OperatorWindow::on_positionCameraButton_released()
{
    cv::Mat img;
    baslerCam::CameraPtr cam = getCamera();
    try
    {
        controller_->findOrigin(cam, img);
        std::cout << "inPerPixX " << model_->getCamSet<double>(cam->getID(), "inPerPixX") << std::endl;
        std::cout << "inPerPixY " << model_->getCamSet<double>(cam->getID(), "inPerPixY") << std::endl;
        std::cout << "originXDist " << model_->getCamSet<double>(cam->getID(), "originXDist") << std::endl;
        std::cout << "originYDist " << model_->getCamSet<double>(cam->getID(), "originYDist") << std::endl;
        std::cout << "originXPix " << model_->getCamSet<double>(cam->getID(), "originXPix") << std::endl;
        std::cout << "originYPix " << model_->getCamSet<double>(cam->getID(), "originYPix") << std::endl;
    }
    catch(const Pylon::GenericException & e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    catch(const std::runtime_error& e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    QImage image(img.data, img.cols, img.rows, img.step, QImage::Format_RGB888);
    QPixmap pixmap = QPixmap::fromImage(image);
    if(getCameraPosition()==0)
        ui->calibrateLabelL->setPixmap(pixmap);
    else
        ui->calibrateLabelR->setPixmap(pixmap);
}

void OperatorWindow::on_startCalibrationButton_released()
{
    QLabel *label;
    if(getCameraPosition()==0)
        label = ui->calibrateLabelL;
    else
        label = ui->calibrateLabelR;

    baslerCam::CameraPtr cam = getCamera();
    try{
        controller_->startCalibrateCamera(cam,
                                      ui->boardWidth->text().toInt(), ui->boardHeight->text().toInt(),
                                      ui->squareSizeX->text().toDouble(), ui->squareSizeY->text().toDouble(),
                                      std::bind(&OperatorWindow::updateImageColor, this, label, std::placeholders::_1));
    }
    catch(const Pylon::GenericException & e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    catch(const std::runtime_error& e)
    {
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    ui->startCalibrationButton->setFlat(true);
    ui->startCalibrationButton->setEnabled(false);
}

void OperatorWindow::on_stopCalibrationButton_released()
{
    ui->stopButton->setEnabled(false);
    if(ui->startCalibrationButton->isEnabled())
        return;
    controller_->stopCalibrateCamera();
    ui->startCalibrationButton->setFlat(false);
    ui->startCalibrationButton->setEnabled(true);
    ui->stopButton->setEnabled(true);
}

int OperatorWindow::getCameraPosition()
{
    if(ui->tabWidget->currentIndex() == 3)
    {
        if(ui->calibrateCamLRadio->isChecked())
            return 0;
        else
            return 1;
    }
    else if(ui->tabWidget->currentIndex() == 5)
    {
        if(ui->cam1Radio_3->isChecked())
            return 0;
        else
            return 1;
    }
    else if(ui->tabWidget->currentIndex() == 6)
    {
        if(ui->classifyCamLRadio->isChecked())
            return 0;
        else
            return 1;
    }
    else{
        return 0;
    }
}

baslerCam::CameraPtr OperatorWindow::getCamera()
{
    return controller_->getCamera(getCameraPosition());
}

void OperatorWindow::updateCameraSettingsView(){
    auto cam = getCamera();
    if(cam==nullptr)
        return;
    std::string sid = cam->getID();
    unsigned int expoTime = model_->getCamSet<unsigned int>(sid, "exposureTime");
    int colorTemp = model_->getCamSet<int>(sid, "colorTemp");
    int tint = model_->getCamSet<int>(sid, "tint");
    int hue = model_->getCamSet<int>(sid, "hue");
    unsigned short gain = model_->getCamSet<unsigned short>(sid, "gain");
    double nozXDist = model_->getCamSet<double>(sid, "nozXDist");
    double nozYDist = model_->getCamSet<double>(sid, "nozYDist");
    double imageLength = model_->getCamSet<double>(sid, "imageLength");
    int hFlip = model_->getCamSet<int>(sid, "hFlip");
    int vFlip = model_->getCamSet<int>(sid, "vFlip");

    ui->camExposureSlider->setValue((int) expoTime);
    ui->camGainSlider->setValue((int) gain);
    ui->camColorSlider->setValue(colorTemp);
    ui->camTintSlider->setValue(tint);
    ui->camHueSlider->setValue(hue);
    ui->camNozXDistLineEdit->setText(QString::number(nozXDist));
    ui->camNozYDistLineEdit->setText(QString::number(nozYDist));
    ui->camImageLengthLineEdit->setText(QString::number(imageLength));
    ui->camHueLineEdit->setText(QString::number(hue));
    ui->camTintLineEdit->setText(QString::number(tint));
    ui->camExpoLineEdit->setText(QString::number(expoTime));
    ui->camGainLineEdit->setText(QString::number(gain));
    ui->camHFlipCheckBox->setChecked(hFlip);
    ui->camVFlipCheckBox->setChecked(vFlip);

    int index = ui->sidComboBox->findText(QString::fromStdString(sid));
    ui->sidComboBox->blockSignals(true);
    ui->sidComboBox->setCurrentIndex(index);
    ui->sidComboBox->blockSignals(false);
}

void OperatorWindow::on_camExposureSlider_valueChanged(int value)
{
    auto cam = getCamera();
    if(cam==nullptr)
        return;
    std::string sid(cam->getID());
    model_->setCamSet(sid, "exposureTime", (unsigned int)value);
    cam->setExposureTime((unsigned int) value);
    ui->camExpoLineEdit->setText(QString::number(value));
}


void OperatorWindow::on_camGainSlider_valueChanged(int value)
{
    auto cam = getCamera();
    if(cam==nullptr)
        return;
    std::string sid(cam->getID());
    model_->setCamSet(sid, "gain", (unsigned short)value);
    cam->setGain((unsigned int) value);
    ui->camGainLineEdit->setText(QString::number(value));
}

void OperatorWindow::on_camColorSlider_valueChanged(int value)
{
    auto cam = getCamera();
    if(cam==nullptr)
        return;
    std::string sid(cam->getID());
    model_->setCamSet(sid, "colorTemp", value);
    int tint = ui->camTintSlider->value();
    model_->setCamSet(sid, "tint", tint);
    cam->setColorTemp(value, tint);
    ui->camColorLineEdit->setText(QString::number(value));
}

void OperatorWindow::on_camHueSlider_valueChanged(int value)
{
    auto cam = getCamera();
    if(cam==nullptr)
        return;
    std::string sid(cam->getID());
    model_->setCamSet(sid, "hue", value);
    cam->setHue(value);
    ui->camHueLineEdit->setText(QString::number(value));
}

void OperatorWindow::on_camTintSlider_valueChanged(int value)
{
    auto cam = getCamera();
    if(cam==nullptr)
        return;
    std::string sid(cam->getID());
    baslerCam::CameraPtr camera = getCamera();
    int colorTemp = ui->camColorSlider->value();
    model_->setCamSet(sid, "colorTemp", colorTemp);
    model_->setCamSet(sid, "tint", value);
    cam->setColorTemp(colorTemp, value);
    ui->camTintLineEdit->setText(QString::number(value));
}

void OperatorWindow::on_calibrateCamRRadio_toggled(bool checked)
{
    updateCameraSettingsView();
}

void OperatorWindow::updateCalibrationLabel(QLabel *label, QPixmap* img){
    label->setPixmap(*img);
}

void OperatorWindow::on_streamCamerasButton_released()
{
    camStreamThread = new QThread();
    for(auto &cam : controller_->getCameras())
    {
        if(cam==nullptr)
            continue;
        int position = model_->getCamSet<int>(cam->getID(), "position");
        QLabel *label;
        if(position == 0)
            label = ui->calibrateLabelL;
        else
            label = ui->calibrateLabelR;
        label->setScaledContents(true);
        CameraStream *stream = new CameraStream(0, cam, label);
        stream->moveToThread(camStreamThread);
        connect(camStreamThread, SIGNAL(started()), stream, SLOT(startTimers()));
        connect(camStreamThread, SIGNAL(finished()), stream, SLOT(deleteLater()));
        connect(camStreamThread, SIGNAL(finished()), camStreamThread, SLOT(deleteLater()));
        connect(stream, SIGNAL(imgCaptured(QLabel*, QPixmap*)), this, SLOT(updateCalibrationLabel(QLabel*, QPixmap*)));
    }
    ui->streamCamerasButton->setDisabled(true);
    camStreamThread->start();
}

void OperatorWindow::on_stopCamerasButton_released()
{
    if(ui->streamCamerasButton->isEnabled())
        return;
    camStreamThread->quit();
    // cameraCalibrationImageTimer->stop();
    // for(auto &cam : controller_->getCameras())
    //     cam->stopPush();
    ui->streamCamerasButton->setDisabled(false);
}

void OperatorWindow::on_camNozXDistLineEdit_editingFinished()
{
    double xDist = ui->camNozXDistLineEdit->text().toDouble();
    std::string id(getCamera()->getID());
    model_->setCamSet<double>(id, "nozXDist", xDist);
}

void OperatorWindow::on_camNozYDistLineEdit_editingFinished()
{
    double yDist = ui->camNozYDistLineEdit->text().toDouble();
    std::string id(getCamera()->getID());
    model_->setCamSet<double>(id, "nozYDist", yDist);
}

void OperatorWindow::on_sidComboBox_currentIndexChanged(const QString &arg1)
{
    int orig_position = model_->getCamSet<int>(arg1.toStdString(), "position");
    int current_position = getCameraPosition();
    // swap the cameras
    if(orig_position != current_position)
    {
        std::string id2(controller_->getCamera(current_position)->getID());
        model_->setCamSet<int>(arg1.toStdString(), "position", current_position);
        model_->setCamSet<int>(id2, "position", orig_position);
    }
    updateCameraSettingsView();
}

void OperatorWindow::on_saveRecipeButton_released()
{
    QString recipe = ui->saveRecipeFileName->text();
    try{
        recipes_->saveRecipe(recipe);
    }
    catch(const std::invalid_argument &e){
        QMessageBox errorBox;
        errorBox.warning(0, "Error", e.what());
        return;
    }
    recipes_->setRecipe(recipe);
    ui->recipeComboBox->setCurrentText(recipe);
}

void OperatorWindow::on_fiducialSizeLineEdit_editingFinished()
{
    double fsize = ui->fiducialSizeLineEdit->text().toDouble();
    model_->setSetPoint<double>("fiducialSize", fsize);
}

void OperatorWindow::on_camImageLengthLineEdit_editingFinished()
{
    double imageLength = ui->camImageLengthLineEdit->text().toDouble();
    std::string id(getCamera()->getID());
    model_->setCamSet<double>(id, "imageLength", imageLength);
}


void OperatorWindow::on_resetCountsButton_released()
{
    model_->setCurrentValue<int>("class0Count", 0);
    model_->setCurrentValue<int>("class1Count", 0);
}

void OperatorWindow::on_nozzleOffsetTimeSlider_valueChanged(int value)
{
    ui->nozzleOffsetTimeLineEdit->setText(QString::number(value));
    model_->setSetPoint<int>("nozOffsetTime", value);
}

void OperatorWindow::on_smallPieceBoostSlider_valueChanged(int value)
{
    ui->smallPieceBoostLineEdit->setText(QString::number(value));
    model_->setSetPoint<int>("nozSmallPieceDurationBoost", value);
}

void OperatorWindow::on_largePieceBoostSlider_valueChanged(int value)
{
    ui->largePieceBoostLineEdit->setText(QString::number(value));
    model_->setSetPoint<int>("nozLargePieceDurationBoost", value);
}


void OperatorWindow::on_largePieceDelaySlider_valueChanged(int value)
{
    ui->largePieceDelayLineEdit->setText(QString::number(value));
    model_->setSetPoint<int>("nozLargePieceDelay", value);
}


void OperatorWindow::updateBounds(){
    model_->setBounds<double>("24V", ui->_24VLowerBound->text().toDouble(), ui->_24VUpperBound->text().toDouble());
    model_->setBounds<double>("5V", ui->_5VLowerBound->text().toDouble(), ui->_5VUpperBound->text().toDouble());
    model_->setBounds<double>("CPA", ui->CPALowerBound->text().toDouble(), ui->CPAUpperBound->text().toDouble());
    model_->setBounds<double>("ctemp", ui->tempLowerBound->text().toDouble(), ui->tempUpperBound->text().toDouble());
    model_->setBounds<double>("inductive1",
                              ui->inductiveSensor1LowerBound->text().toDouble(),
                              ui->inductiveSensor1UpperBound->text().toDouble());
    model_->setBounds<double>("inductive2",
                              ui->inductiveSensor2LowerBound->text().toDouble(),
                              ui->inductiveSensor2UpperBound->text().toDouble());
}

void OperatorWindow::on__24VLowerBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on__5VLowerBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_CPALowerBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_tempLowerBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_inductiveSensor1LowerBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_inductiveSensor2LowerBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on__24VUpperBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on__5VUpperBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_CPAUpperBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_tempUpperBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_inductiveSensor1UpperBound_editingFinished(){
    updateBounds();
}
void OperatorWindow::on_inductiveSensor2UpperBound_editingFinished(){
    updateBounds();
}

void OperatorWindow::on_tabWidget_currentChanged(int index)
{
//    if(index == 0)
//    {
//        ui->recipeComboBox->blockSignals(true);
//        recipes_->loadRecipes();
//        for(auto & recipe : recipes_->getRecipes())
//        {
//            if(ui->recipeComboBox->findText(recipe) == -1)
//                ui->recipeComboBox->addItem(recipe);
//        }
//        ui->recipeComboBox->setCurrentText(recipes_->getRecipe());
//        ui->recipeComboBox->blockSignals(false);

//        ui->stackedWidget->setCurrentIndex(0);
//    }
}

void OperatorWindow::on_stopRecordButton_released()
{
    if(synchro_record_ != nullptr){
        ui->recordButton->setFlat(false);
        delete synchro_record_;
        synchro_record_ = nullptr;
    }
}

void OperatorWindow::setRecordImage(int index, QPixmap img)
{
    if(index == 0)
        ui->recordLabel1->setPixmap(img);
    else
        ui->recordLabel2->setPixmap(img);
}

void OperatorWindow::recordCallback(int index, cv::Mat _img)
{
    cv::Mat rimg;
    cv::rotate(_img, rimg, cv::ROTATE_90_COUNTERCLOCKWISE);
    QImage image(rimg.data, rimg.cols, rimg.rows, rimg.step, QImage::Format_RGB888);
    emit imageRecorded(index, QPixmap::fromImage(image.rgbSwapped()));
}

void OperatorWindow::on_recordButton_pressed()
{
    connect(this, SIGNAL(imageRecorded(int, QPixmap)), this, SLOT(setRecordImage(int,QPixmap))),
    synchro_record_ = new SynchroRecord(controller_,
                                        controller_->getCameras(),
                                        std::bind(&OperatorWindow::recordCallback, this,
                                                  std::placeholders::_1,
                                                  std::placeholders::_2));
}

void OperatorWindow::on_tabWidget_tabBarClicked(int index)
{
    if(index == 0)
    {
        ui->recipeComboBox->blockSignals(true);
        recipes_->loadRecipes();
        for(auto & recipe : recipes_->getRecipes())
        {
            if(ui->recipeComboBox->findText(recipe) == -1)
                ui->recipeComboBox->addItem(recipe);
        }
        ui->recipeComboBox->setCurrentText(recipes_->getRecipe());
        ui->recipeComboBox->blockSignals(false);

        ui->stackedWidget->setCurrentIndex(0);
    }
}

void OperatorWindow::on_segmentBeforeRecordCheckBox_stateChanged(int arg1)
{
    if(arg1)
        model_->setSetPoint<int>("segmentBeforeRecord", 1);
    else
        model_->setSetPoint<int>("segmentBeforeRecord", 0);
}

void OperatorWindow::on_matchBeltSpeedRecordCheckBox_stateChanged(int arg1)
{
    if(arg1)
        model_->setSetPoint<int>("matchBeltSpeedRecord", 1);
    else
        model_->setSetPoint<int>("matchBeltSpeedRecord", 0);
}

void OperatorWindow::on_imageLengthLineEdit_editingFinished()
{
    model_->setSetPoint<double>("synchroImageLength", ui->imageLengthLineEdit->text().toDouble());
}

void OperatorWindow::on_fpsLineEdit_editingFinished()
{
    model_->setSetPoint<double>("fps", ui->fpsLineEdit->text().toDouble());
}

void OperatorWindow::on_camHFlipCheckBox_clicked(bool checked)
{
    auto cam = getCamera();
    if(cam == nullptr)
        return;
    std::string id(cam->getID());
    model_->setCamSet<int>(id, "hFlip", checked);
    cam->setHFlip(checked);
}

void OperatorWindow::on_camVFlipCheckBox_clicked(bool checked)
{
    auto cam = getCamera();
    if(cam == nullptr)
        return;
    std::string id(cam->getID());
    model_->setCamSet<int>(id, "vFlip", checked);
    cam->setVFlip(checked);
}

void OperatorWindow::on_minimumAreaBox_editingFinished()
{
    model_->setSetPoint<double>("pieceMinAreaIn",ui->minimumAreaBox->text().toDouble());
}

void OperatorWindow::on_maximumAreaBox_editingFinished()
{
    model_->setSetPoint<double>("pieceMaxAreaIn",ui->maximumAreaBox->text().toDouble());
}
