#ifndef OPERATORWINDOW_H
#define OPERATORWINDOW_H

#include <QMainWindow>
#include "VisionSystem.h"
#include "Controller.h"
#include "SynchroRecord.h"
#include "Recipes.h"
#include "SquareHollowButton.h"
#include <QLabel>
#include <QtMultimedia/QCamera>
#include <QtMultimedia/QCameraInfo>
#include <QCameraViewfinder>
#include <QCameraImageCapture>

struct SegmentStruct{
    SegmentStruct() : hasImage(false), hasBWImg(false), hasdilimg(false)
    {}
    cv::Mat cimg;
    cv::Mat img;
    cv::Mat bwimg;
    cv::Mat dilimg;
    bool hasImage;
    bool hasBWImg;
    bool hasdilimg;
    baslerCam::CameraPtr cam;
    std::vector<cv::Rect2i> pieces;
    SegmenterPtr segmenter;
};

struct ClassifyStruct{
    ClassifyStruct() : hasImage(false), hasClassImage(false)
    {}
    cv::Mat img;
    cv::Mat classImg;
    bool hasImage;
    bool hasClassImage;
    QString extension;
};

namespace Ui {
class OperatorWindow;
}

class OperatorWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit OperatorWindow(QWidget *parent = 0, VisionSystemPtr vision = nullptr, ControllerPtr controller = nullptr, RecipesPtr recipes = nullptr);
    ~OperatorWindow();
    int getCameraPosition();
    baslerCam::CameraPtr getCamera();
    void updateImageGray(cv::Mat img);
    /* void updateImageColor(cv::Mat img); */
    void updateImageColor(QLabel *label, cv::Mat img);
    void setSegmentationImage(cv::Mat img);
    void classifyClassPicture();
    void log(std::string str);
    void setClassImage(cv::Mat img);
    void recordCallback(int index, cv::Mat _img);
public slots:
    void updateLog(QString arg);
    void updateState(VS_STATE state);
//    void updateVisionImage(void);
    void updateAllStatus();
    void updateLowResImage();
    void updateCalibrationLabel(QLabel *label, QPixmap* img);
    void updateClassLog(QString arg);
    void updateClassImage();
    void throwErrorBox(QString error);

private slots:
    void visionLoopCallback(cv::Mat img, std::string status, VS_STATE state);
    void on_stopButton_released();
    void on_startButton_pressed();

    void on_maintenanceModeButton_released();
    void on_operatorModeButton_released();
    void on_engineerModeButton_released();

    void on_acButton_pressed();
    void on_conveyorButton_pressed();
    void on_lightsButton_pressed();
    void on_feederButton_pressed();
    void on_acButton_2_pressed();
    void on_conveyorButton_2_pressed();
    void on_lightsButton_2_pressed();
    void on_feederButton_2_pressed();
    void on_recipeComboBox_currentIndexChanged(const QString &arg1);

    void on_positionCameraButton_released();
    void on_startCalibrationButton_released();
    void on_stopCalibrationButton_released();

    void on_voltageControl_sliderReleased();
    void on_inclineVoltageControl_sliderReleased();
    void on_creepVoltageControl_sliderReleased();
    void on_HD75voltageControl_sliderReleased();
    void setSolenoid(size_t i);
    void on_speedSensorFactorbox_editingFinished();

    void on_pushButton_clicked();
    void on_takeGrayScalePictureButton_released();
    void on_GetLuminanceValues_released();
    void on_thresholdButton_released();
    void on_dilateButton_released();
    void on_findContoursButton_released();
    void on_highlightSlider_valueChanged(int value);
    void on_shadowSlider_valueChanged(int value);
    void on_dilationIterationsSlider_valueChanged(int value);
    void on_dilationSizeSlider_valueChanged(int value);

    void on_saveColorImageButton_1_released();
    void on_savePiecesButton_released();

    void on_snapClassPicture_released();

    void on_classifyClassPicture_released();

    void on_classifyLoadPicture_released();

    void on_classifySaveResultButtom_released();

    void on_segmentLoadPictureButton_released();

    void on_camExposureSlider_valueChanged(int value);

    void on_camColorSlider_valueChanged(int value);

    void on_camHueSlider_valueChanged(int value);

    void on_camTintSlider_valueChanged(int value);

    void on_calibrateCamRRadio_toggled(bool checked);

    void on_streamCamerasButton_released();

    void on_stopCamerasButton_released();

    void on_camNozXDistLineEdit_editingFinished();

    void on_camNozYDistLineEdit_editingFinished();
    void on_sidComboBox_currentIndexChanged(const QString &arg1);

    void on_saveRecipeButton_released();

    void on_fiducialSizeLineEdit_editingFinished();

    void on_camImageLengthLineEdit_editingFinished();


    void on_resetCountsButton_released();

    void on_nozzleOffsetTimeSlider_valueChanged(int value);

    void on_smallPieceBoostSlider_valueChanged(int value);

    void on__24VLowerBound_editingFinished();
    void on__5VLowerBound_editingFinished();
    void on_CPALowerBound_editingFinished();
    void on_tempLowerBound_editingFinished();
    void on__24VUpperBound_editingFinished();
    void on__5VUpperBound_editingFinished();
    void on_CPAUpperBound_editingFinished();
    void on_tempUpperBound_editingFinished();
    void on_largePieceBoostSlider_valueChanged(int value);
    void on_largePieceDelaySlider_valueChanged(int value);

    void on_camGainSlider_valueChanged(int value);

    void on_inductiveSensor1LowerBound_editingFinished();

    void on_inductiveSensor2LowerBound_editingFinished();

    void on_inductiveSensor1UpperBound_editingFinished();

    void on_inductiveSensor2UpperBound_editingFinished();

    void on_tabWidget_currentChanged(int index);
    void on_stopRecordButton_released();

    void on_recordButton_pressed();
    void setRecordImage(int index, QPixmap img);
    void on_tabWidget_tabBarClicked(int index);

    void on_segmentBeforeRecordCheckBox_stateChanged(int arg1);

    void on_matchBeltSpeedRecordCheckBox_stateChanged(int arg1);

    void on_imageLengthLineEdit_editingFinished();

    void on_fpsLineEdit_editingFinished();

    void on_camHFlipCheckBox_clicked(bool checked);
    void on_camVFlipCheckBox_clicked(bool checked);
    void on_intersectPercentSlider_valueChanged(int value);

    void on_minimumAreaBox_editingFinished();

    void on_maximumAreaBox_editingFinished();



signals:
    void stateUpdate(VS_STATE);
    void logUpdate(QString);
    void visionImageUpdate(void);
    void engineer_window();
    void calibrationLabelStreamed(int);
    void classImageUpdate();
    void classLogUpdate(QString);
    void error(QString);
    void imageRecorded(int, QPixmap);
private:
    QCamera* qCam;
    AstraCamPtr astraCam_;
    QCameraViewfinder *qCamView;
    QCameraImageCapture* qCamCapture;

    void updateBounds();
    void updateCameraSettingsView();
    void initializeValues();
    Ui::OperatorWindow *ui;
    VisionSystemPtr vision_system_;
    ControllerPtr controller_;

    ModelPtr model_;
    RecipesPtr recipes_;
    QVector<SquareHollowButton*> airNozzleButtons;
    QVector<QLabel*> airNozzleLabels;
    SegmentStruct segStruct;
    ClassifyStruct classStruct;
    QTimer *cameraCalibrationImageTimer;
    cv::Mat segmentedImage_, classifiedImage_;
    QThread *camStreamThread;
    QThread *aCamStreamThread;
    SynchroRecord *synchro_record_;
};

#endif // OPERATORWINDOW_H
