#include "Recipes.h"
#include <iostream>
#include <QMessageBox>

Recipes::Recipes(ModelPtr model) : model_(model)
{
    defaultModel_ = ModelPtr(new Model);
    initializeDefaultModel(defaultModel_);
    initializeDefaultModel(model_);
    loadRecipes();

    QSettings settings("UHV", "Vision_Sorter");
    settings.beginGroup("recipes");
    QString recipe = settings.value("recipe").toString();
    int VSactive = settings.value("VSactive").toInt();
    if(VSactive){	// program already running, show dialog error box
    	QMessageBox errorBox;
        errorBox.warning(0, "Error", "Previous copy running!");
        throw std::runtime_error(std::string("VisionSorter "));
    }
    //std::cout << settings.fileName().toStdString() << std::endl;
    //std::cout << recipe.toStdString() << std::endl;
    if(recipes_.count(recipe))
        recipe_ = recipe;
    else
        recipe = "";
    
    settings.setValue("VSactive",1);
    settings.endGroup();

    loadRecipe(recipe_);
}

void Recipes::initializeDefaultModel(const ModelPtr &model)
{
    // passwords
    model->setSetPoint<bool>("mode", false);
    model->setSetPoint<int>("operatorPass", 1234);
    model->setSetPoint<int>("maintenancePass", 1234);
    model->setSetPoint<int>("engineerPass", 1234);

    // mechanics
    model->setSetPoint<double>("conveyorV", 2.5);
    model->setSetPoint<double>("conveyorS", 150);
    model->setSetPoint<double>("creepconveyorV", 0.0);
    model->setSetPoint<double>("inclineconveyorV", 0.0);
    model->setSetPoint<double>("HD75V", 0.0);
    model->setSetPoint<double>("belt_length", 24.5);
    model->setBounds<double>("ctemp", 0, 100);
    model->setBounds<double>("CPA", 60, 100);
    model->setBounds<double>("24V", 22, 25);
    model->setBounds<double>("5V", 4.7, 5);
    model->setBounds<double>("inductive1", 3.5, 4.8);
    // model->setCurrentValue<double>("inductive1", 0);
    // model->setCurrentValue<double>("inductive2", 0);
    model->setBounds<double>("inductive2", 3.5, 5);
    model->setCurrentValue<double>("conveyorSpeedEst1", 0);
    model->setCurrentValue<double>("conveyorSpeedEst2", 0);
    model->setSetPoint<bool>("doorIlock", true);
    model->setSetPoint<bool>("otherIlock", true);
    model->setSetPoint<double>("speedSensorFactor", 1.0);
//    model->setSetPoint<double>("nozzleStagger", 0.0);		// used for systems with staggered nozzles
    model->setBounds<double>("levelSensor", 3.5, 5);
    model->setSetPoint<int>("levelOverrunSecs", 5);
    model->setSetPoint<int>("levelEmptySecs", 15);

    // cameras
    model->setCamSet<int>("0", "position", -1);
    model->setCamSet<int>("0", "colorTemp", 7182);
    model->setCamSet<int>("0", "tint", 800);
    model->setCamSet<int>("0", "hue", 0);
    model->setCamSet<unsigned int>("0", "exposureTime", 1400);
    model->setCamSet<unsigned int>("0", "eSize", 1);
    model->setCamSet<unsigned short>("0", "gain", 100);
    model->setCamSet<int>("0", "hFlip", false);
    model->setCamSet<int>("0", "vFlip", false);
    model->setCamSet<int>("0", "rectify", false);
    model->setCamSet<int>("0", "hwTrigger", false);
    model->setCamSet<int>("0", "triggerLine", 0);

    // solenoid indexes
    for(int i = 0; i < 24; ++i){
        model->setSolSet("0", i, 24-i, 1);
    }
    // system positioning
    model->setCamSet<double>("0", "originXDist", 0);
    model->setCamSet<double>("0", "originYDist", 0);
    // from the top left of the image to the top right of the fiducial
    model->setCamSet<double>("0", "originXPix", 0);
    model->setCamSet<double>("0", "originYPix", 0);
    model->setCamSet<double>("0", "inPerPixX", 0.006346);
    model->setCamSet<double>("0", "inPerPixY", 0.006288);
    model->setCamSet<double>("0", "in2PerPix2", 0.000040);

    // measured
    // from the top right of the fiducial to
    // the top left point of the first nozzle
    // in the camera's list
    model->setCamSet<double>("0", "nozXDist", 0);
    model->setCamSet<double>("0", "nozYDist", 0);
    model->setCamSet<double>("0", "imageLength", 15);
    model->setSetPoint<double>("fiducialSize", 2.0935);

    // classification and segmentation
    model->setMeta("class0Alias", "cast");
    model->setMeta("class1Alias", "wrought");
    model->setSetPoint<bool>("ignoreEdge", false);
    model->setSetPoint<double>("imageOverlap", 0);
    model->setSetPoint<double>("probClass1", 0.5);
    model->setSetPoint<size_t>("class_batch_size", 32);
    model->setSetPoint<int>("highlightThresh", 100);
    model->setSetPoint<int>("shadowThresh", 6);
    model->setSetPoint<int>("dilationSize", 6);
    model->setSetPoint<int>("dilationIterations", 8);
    model->setSetPoint<double>("pieceMinAreaIn", 1.5);
    model->setSetPoint<double>("pieceMaxAreaIn", 28);
    model->setSetPoint<int>("nozOffsetTime", 0);
    model->setSetPoint<int>("nozSmallPieceDurationBoost", 0);
    model->setSetPoint<int>("nozLargePieceDurationBoost", 0);
    model->setSetPoint<int>("nozLargePieceDelay", 0);
    model->setSetPoint<double>("neededIntersectPercent", 0.1);
    model->setSetPoint<int>("numValves", 24);
    model->setSetPoint<double>("fps", 4);
    model->setSetPoint<int>("segmentBeforeRecord", 0);
    model->setSetPoint<int>("matchBeltSpeedRecord", 0);
    model->setSetPoint<double>("synchroImageLength", 15);

    // directories and models
    model->setMeta("dataDir", "data");
    model->setMeta("modelPath", "controller/classifier/models/V3_C1_C3_J_S_W1_W2_W3_W4_W5_W6_07-52PM_04-Jan-2018_155-0.009-0.9982.hdf5");
}

void Recipes::loadRecipe(const QString &recipe)
{
    if(recipe == "")
    {
        initializeDefaultModel(model_);
        return;
    }
    YAML::Node config = YAML::LoadFile(std::string("recipes/") + recipe.toStdString());
    recipe_ = recipe;

    try{
        // mechanics
        configSetPoint<double>(config, "conveyorV");
        configSetPoint<double>(config, "conveyorS");
        configSetPoint<double>(config, "creepconveyorV");
        configSetPoint<double>(config, "inclineconveyorV");
        configSetPoint<double>(config, "HD75V");
        configSetBounds<double>(config, "inductive1");
        configSetBounds<double>(config, "inductive2");
        configSetPoint<double>(config, "belt_length");
        configSetBounds<double>(config, "CPA");
        configSetBounds<double>(config, "ctemp");
        configSetBounds<double>(config, "24V");
        configSetBounds<double>(config, "5V");
        configSetPoint<int>(config, "operatorPass");
        configSetPoint<int>(config, "maintenancePass");
        configSetPoint<int>(config, "engineerPass");
        configSetPoint<double>(config, "speedSensorFactor");
//        configSetPoint<double>(config, "nozzleStagger");	// used for systems with staggered nozzles
        configSetBounds<double>(config, "levelSensor");
        configSetPoint<int>(config, "levelOverrunSecs");
        configSetPoint<int>(config, "levelEmptySecs");

        configSetPoint<double>(config, "fiducialSize");

        configMeta(config, "dataDir");
        configMeta(config, "modelPath");
        configSetPoint<bool>(config, "ignoreEdge");
        configSetPoint<double>(config, "imageOverlap");
        configMeta(config, "class0Alias");
        configMeta(config, "class1Alias");
        configSetPoint<double>(config, "probClass1");
        configSetPoint<size_t>(config, "class_batch_size");

        // classification and segmentation
        configSetPoint<int>(config, "highlightThresh");
        configSetPoint<int>(config, "shadowThresh");
        configSetPoint<int>(config, "dilationSize");
        configSetPoint<int>(config, "dilationIterations");
        configSetPoint<double>(config, "pieceMinAreaIn");
        configSetPoint<double>(config, "pieceMaxAreaIn");
        configSetPoint<int>(config, "nozOffsetTime");
        configSetPoint<int>(config, "nozSmallPieceDurationBoost");
        configSetPoint<int>(config, "nozLargePieceDurationBoost");
        configSetPoint<int>(config, "nozLargePieceDelay");
        configSetPoint<double>(config, "neededIntersectPercent");
        configSetPoint<int>(config, "numValves");

        // cameras
        for(const auto &cam : config["cam"])
        {
            std::string i = cam.first.as<std::string>();

            configSolenoids(config, i);

            configCamSet<int>(config, i, "position");
            configCamSet<double>(config, i, "originXPix");
            configCamSet<double>(config, i, "originYPix");
            configCamSet<double>(config, i, "originXDist");
            configCamSet<double>(config, i, "originYDist");
            configCamSet<double>(config, i, "in2PerPix2");
            configCamSet<double>(config, i, "inPerPixX");
            configCamSet<double>(config, i, "inPerPixY");
            configCamSet<double>(config, i, "nozXDist");
            configCamSet<double>(config, i, "nozYDist");
            configCamSet<double>(config, i, "imageLength");
            configCamSet<int>(config, i, "colorTemp");
            configCamSet<int>(config, i, "tint");
            configCamSet<int>(config, i, "hue");
            configCamSet<unsigned int>(config, i, "exposureTime");
            configCamSet<unsigned int>(config, i, "eSize");
            configCamSet<unsigned short>(config, i, "gain");
            configCamSet<int>(config, i, "hFlip");
            configCamSet<int>(config, i, "vFlip");
            configCamSet<int>(config, i, "rectify");
            configCamSet<int>(config, i, "hwTrigger");
            configCamSet<int>(config, i, "triggerLine");

            if(config["cam"][i]["intr"])
                model_->setCamRectify(i, "intr", config["cam"][i]["intr"].as<std::vector<float>>());
            if(config["cam"][i]["distor"])
                model_->setCamRectify(i, "distor", config["cam"][i]["distor"].as<std::vector<float>>());
        }
        configSetPoint<double>(config, "fps");
        configSetPoint<int>(config, "segmentBeforeRecord");
        configSetPoint<int>(config, "matchBeltSpeedRecord");
        configSetPoint<double>(config, "synchroImageLength");

    }
    catch(const std::exception &e)
    {
        throw std::runtime_error(std::string(e.what()) + "Failed to load the recipe\n");
    }
}

void Recipes::configCamera(YAML::Node &config, const std::string &sid)
{
        if(!model_->hasCamID(sid))
            return;

        config["cam"][sid]["position"] = getCamSet<int>(sid, "position");
        config["cam"][sid]["colorTemp"] = getCamSet<int>(sid, "colorTemp");
        config["cam"][sid]["tint"] = getCamSet<int>(sid, "tint");
        config["cam"][sid]["hue"] = getCamSet<int>(sid, "hue");
        config["cam"][sid]["exposureTime"] = getCamSet<unsigned int>(sid, "exposureTime");
        config["cam"][sid]["eSize"] = getCamSet<unsigned int>(sid, "eSize");
        config["cam"][sid]["gain"] = getCamSet<unsigned short>(sid, "gain");
        config["cam"][sid]["hFlip"] = getCamSet<int>(sid, "hFlip");
        config["cam"][sid]["vFlip"] = getCamSet<int>(sid, "vFlip");
        config["cam"][sid]["hwTrigger"] = getCamSet<int>(sid, "hwTrigger");
        config["cam"][sid]["triggerLine"] = getCamSet<int>(sid, "triggerLine");

        // system positioning
        config["cam"][sid]["originXDist"] = getCamSet<double>(sid, "originXDist");
        config["cam"][sid]["originYDist"] = getCamSet<double>(sid, "originYDist");
        config["cam"][sid]["originXPix"] = getCamSet<double>(sid, "originXPix");
        config["cam"][sid]["originYPix"] = getCamSet<double>(sid, "originYPix");
        config["cam"][sid]["inPerPixX"] = getCamSet<double>(sid, "inPerPixX");
        config["cam"][sid]["inPerPixY"] = getCamSet<double>(sid, "inPerPixY");
        config["cam"][sid]["in2PerPix2"] = getCamSet<double>(sid, "in2PerPix2");
        config["cam"][sid]["imageLength"] = getCamSet<double>(sid, "imageLength");

        // measured
        config["cam"][sid]["nozXDist"] = getCamSet<double>(sid, "nozXDist");
        config["cam"][sid]["nozYDist"] = getCamSet<double>(sid, "nozYDist");

        // rectification
        config["cam"][sid]["rectify"] = getCamSet<int>(sid, "rectify");

        // solenoids
        // for(auto &solenoid : model_->getSolenoids(sid)){
        //     config["cam"][sid]["solenoids"].push_back(solenoid.first);
        //     config["cam"][sid]["solenoids"][solenoid.first]["width"] = solenoid.second;
        // }
        config["cam"][sid]["solenoids"] = model_->getSolenoids(sid);
}

void Recipes::saveRecipe(const QString &recipe)
{
    YAML::Node config;
    // setpoint
    // lower bound
    // upper bound

    // lower bound
    // upper bound

    // mechanics
    config["conveyorV"] = getSetPoint<double>("conveyorV");
    config["conveyorS"] = getSetPoint<double>("conveyorS");
    config["ctemp"].push_back(getLowerBound<double>("ctemp"));
    config["ctemp"].push_back(getUpperBound<double>("ctemp"));
    config["CPA"].push_back(getLowerBound<double>("CPA"));
    config["CPA"].push_back(getUpperBound<double>("CPA"));
    config["24V"].push_back(getLowerBound<double>("24V"));
    config["24V"].push_back(getUpperBound<double>("24V"));
    config["5V"].push_back(getLowerBound<double>("5V"));
    config["5V"].push_back(getUpperBound<double>("5V"));
    config["operatorPass"] = getSetPoint<int>("operatorPass");
    config["maintenancePass"] = getSetPoint<int>("maintenancePass");
    config["engineerPass"] = getSetPoint<int>("engineerPass");
    config["inductive1"].push_back(getLowerBound<double>("inductive1"));
    config["inductive1"].push_back(getUpperBound<double>("inductive1"));
    config["inductive2"].push_back(getLowerBound<double>("inductive2"));
    config["inductive2"].push_back(getUpperBound<double>("inductive2"));
    config["belt_length"] = getSetPoint<double>("belt_length");
    config["creepconveyorV"] = getSetPoint<double>("creepconveyorV");
    config["inclineconveyorV"] = getSetPoint<double>("inclineconveyorV");
    config["HD75V"] = getSetPoint<double>("HD75V");
    config["speedSensorFactor"] = getSetPoint<double>("speedSensorFactor");
//    config["nozzleStagger"] = getSetPoint<double>("nozzleStagger");	// used for systems with staggered nozzles
    config["levelSensor"].push_back(getLowerBound<double>("levelSensor"));
    config["levelSensor"].push_back(getUpperBound<double>("levelSensor"));
    config["levelOverrunSecs"] = getSetPoint<int>("levelOverrunSecs");
    config["levelEmptySecs"] = getSetPoint<int>("levelEmptySecs");

    // cameras
    for(auto &cam : model_->getCamSettings())
        configCamera(config, cam.first);
    config["fps"] = getSetPoint<double>("fps");
    config["segmentBeforeRecord"] = getSetPoint<int>("segmentBeforeRecord");
    config["matchBeltSpeedRecord"] = getSetPoint<int>("matchBeltSpeedRecord");
    config["synchroImageLength"] = getSetPoint<double>("synchroImageLength");

    config["fiducialSize"] = getSetPoint<double>("fiducialSize");

    // classification and segmentation
    config["modelPath"] = model_->getMeta("modelPath");
    config["class0Alias"] = model_->getMeta("class0Alias");
    config["class1Alias"] = model_->getMeta("class1Alias");
    config["probClass1"] = getSetPoint<double>("probClass1");
    config["imageOverlap"] = getSetPoint<double>("imageOverlap");
    config["class_batch_size"] = getSetPoint<size_t>("class_batch_size");
    config["highlightThresh"] = getSetPoint<int>("highlightThresh");
    config["shadowThresh"] = getSetPoint<int>("shadowThresh");
    config["dilationSize"] = getSetPoint<int>("dilationSize");
    config["dilationIterations"] = getSetPoint<int>("dilationIterations");
    config["pieceMinAreaIn"] = getSetPoint<double>("pieceMinAreaIn");
    config["pieceMaxAreaIn"] = getSetPoint<double>("pieceMaxAreaIn");
    config["nozOffsetTime"] = getSetPoint<int>("nozOffsetTime");
    config["nozSmallPieceDurationBoost"] = getSetPoint<int>("nozSmallPieceDurationBoost");
    config["nozLargePieceDurationBoost"] = getSetPoint<int>("nozLargePieceDurationBoost");
    config["nozLargePieceDelay"] = getSetPoint<int>("nozLargePieceDelay");
    config["neededIntersectPercent"] = getSetPoint<double>("neededIntersectPercent");
    config["numValves"] = getSetPoint<int>("numValves");

    // camera rectification
    config["dataDir"] = model_->getMeta("dataDir");

    std::ofstream fout(std::string("recipes/") + recipe.toStdString());
    fout << config;
    fout.close();
}

std::vector<baslerCam::CameraPtr> Recipes::getCameras(DAQPtr daq)
{
    std::vector<baslerCam::CameraPtr> camVec;
    try
    {
    	Pylon::CTlFactory& TlFactory = Pylon::CTlFactory::GetInstance();
    	Pylon::DeviceInfoList_t lstDevices;
    	TlFactory.EnumerateDevices( lstDevices );

    	if(lstDevices.empty())
        	throw Pylon::GenericException("No cameras were found", "Recipes.cpp", 346);

    	// find the available cameras
    	if ( ! lstDevices.empty() ) {
        	Pylon::DeviceInfoList_t::const_iterator it;
        	for ( it = lstDevices.begin(); it != lstDevices.end(); ++it )
        	{
            	   std::ostringstream stream;
            	   stream << it->GetModelName() << it->GetSerialNumber();
            	   std::string id = stream.str();
	    	   std::cout << std::string("Found camera ") << id << std::endl;
    	    	   Pylon::IPylonDevice* device = nullptr;
            	   device = TlFactory.CreateDevice(*it);

            	   camVec.push_back(cameraFactory(device, id, daq));
        	}
    	}
    }
    catch (const Pylon::GenericException & e)
    {
    	std::cerr << "Failed to enumerate devices. Reason: "
  	<< e.GetDescription() << std::endl;
    }

    return camVec;
}

baslerCam::CameraPtr Recipes::cameraFactory(Pylon::IPylonDevice* device, std::string sid, DAQPtr daq)
{
    baslerCam::CameraSettings camSet;
    baslerCam::CameraPtr cam;
    try{
    	Pylon::CBaslerUsbInstantCamera *cam_ = new Pylon::CBaslerUsbInstantCamera(device);

    	if(model_->hasCamID(sid))
    	{
           camSet.colorTemp = model_->getCamSet<int>(sid, "colorTemp");
           camSet.tint = model_->getCamSet<int>(sid, "tint");
           camSet.hue = model_->getCamSet<int>(sid, "hue");
           camSet.exposureTime = model_->getCamSet<unsigned int>(sid, "exposureTime");
           camSet.eSize = model_->getCamSet<unsigned int>(sid, "eSize");
           camSet.gain = model_->getCamSet<unsigned short>(sid, "gain");
           camSet.hFlip = model_->getCamSet<int>(sid, "hFlip");
           camSet.vFlip = model_->getCamSet<int>(sid, "vFlip");
           camSet.hwTrigger = model_->getCamSet<int>(sid, "hwTrigger");
           camSet.triggerLine = model_->getCamSet<int>(sid, "triggerLine");
 
           cam = baslerCam::CameraPtr(new baslerCam::Camera(cam_, camSet, sid, daq));
           bool rectify = model_->getCamSet<int>(sid, "rectify");
           model_->setCamSet<bool>(sid, "configured", true);
           if(rectify)
           	cam->setRectify(model_->getCamRectify(sid, "intr"),
                            model_->getCamRectify(sid, "distor"));
    	}
    	else
    	{	
           std::cout << std::string("Camera id ") << sid << std::string(" not found in the recipe.") << std::endl;
           // create a new entry
           camSet.colorTemp = 7182;
           camSet.tint = 800;
           camSet.hue = 0;
           camSet.gain = 100;
           camSet.exposureTime = 1400;
           camSet.eSize = 1;
           camSet.hwTrigger = false;
           camSet.triggerLine = 0;

           model_->setCamSet<int>(sid, "position", -1);
           model_->setCamSet<bool>(sid, "configured", false);
           model_->setCamSet<double>(sid, "originXPix", 0);
           model_->setCamSet<double>(sid, "originYPix", 0);
           model_->setCamSet<double>(sid, "originXDist", 0);
           model_->setCamSet<double>(sid, "originYDist", 0);
           model_->setCamSet<double>(sid, "in2PerPix2", 0);
           model_->setCamSet<double>(sid, "inPerPixX", 0);
           model_->setCamSet<double>(sid, "inPerPixY", 0);
           model_->setCamSet<double>(sid, "nozXDist", 0);
           model_->setCamSet<double>(sid, "nozYDist", 0);
           model_->setCamSet<double>(sid, "imageLength", 0);
           model_->setCamSet<int>(sid, "colorTemp", 7182);
           model_->setCamSet<int>(sid, "tint", 800);
           model_->setCamSet<int>(sid, "hue", 0);
           model_->setCamSet<unsigned int>(sid, "exposureTime", 1400);
           model_->setCamSet<unsigned int>(sid, "eSize", 1);
           model_->setCamSet<unsigned short>(sid, "gain", 100);
           model_->setCamSet<int>(sid, "hFlip", 0);
           model_->setCamSet<int>(sid, "vFlip", 0);
           model_->setCamSet<int>(sid, "rectify", false);
	   model_->setCamSet<int>(sid, "hwTrigger", false);
 	   model_->setCamSet<int>(sid, "triggerLine", 0);

           cam = baslerCam::CameraPtr(new baslerCam::Camera(cam_, camSet, sid, daq));
           model_->setCamSet<bool>(sid, "configured", false);
    	}
    }
    catch (const Pylon::GenericException & e)
    {
    	std::cerr << "Failed create CInstantCamera. Reason: "
  	<< e.GetDescription() << std::endl;
    }

    return baslerCam::CameraPtr(cam);
}

void Recipes::loadRecipes()
{
    recipes_.clear();
    DIR *dir;
    struct dirent *ent;

    // check that directory exists
    if((dir = opendir("./recipes")) == NULL)
    {
        if(mkdir("recipes", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1)
        {
            throw std::runtime_error("The recipes directory does not exist and could not be created.");
            return;
        }
    }

    // loop through directory
    dir = opendir("./recipes");
    bool defaultFound = false;
    while(( ent = readdir(dir)) != NULL )
    {
        if(ent->d_name[0] == '.')
            continue;
        size_t length = strlen(ent->d_name);
        if(length < 3)
            continue;
        if(ent->d_name[length-1] != 'p'
           && ent->d_name[length-2] != 'c'
           && ent->d_name[length-3] != 'r')
            continue;

        recipes_.insert(ent->d_name);
        if(strcmp(ent->d_name, "default.rcp") == 0)
            defaultFound = true;
    }
}

void Recipes::configMeta(YAML::Node &config, const std::string &key)
{
    try{
        if(config[key])
            model_->setMeta(key, config[key].as<std::string>());
        else
            throw std::runtime_error(std::string("Key ") + key + " is missing from the recipe.\n");
    }
    catch(const YAML::RepresentationException &e){
        throw std::runtime_error(std::string(e.what()) + std::string("\nFailed to load key: \"") + key + "\"\n");
    }
    catch(const std::runtime_error &e){
        throw std::runtime_error(e.what());
    }
}
