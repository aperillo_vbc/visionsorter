#ifndef VISION_RECIPES_H
#define VISION_RECIPES_H

#include "Model.h"
#include "camera/Camera.h"
#include <QSettings>
#include <QString>
#include <fstream>
#include <dirent.h>
#include <sys/stat.h>
#include <set>
#include <yaml-cpp/yaml.h>

class Recipes;

typedef std::shared_ptr<Recipes> RecipesPtr;

class Recipes{
public:
    Recipes(ModelPtr model);
    void initializeDefaultModel(const ModelPtr &model);
    void loadRecipe(const QString &recipe);
    void saveRecipe(const QString &recipe);
    std::vector<baslerCam::CameraPtr> getCameras(DAQPtr daq);

    baslerCam::CameraPtr cameraFactory(Pylon::IPylonDevice* device, std::string id, DAQPtr daq);
    void writeDefaultRecipe();
    void loadRecipes();
    inline const std::set<QString> &getRecipes(){return recipes_;}
    inline QString getRecipe(){return recipe_;}
    inline void setRecipe(const QString &recipe){recipe_ = recipe;}

    template<typename T>
    std::string getCamSet(const std::string &sid, const std::string &key){
        return std::to_string(model_->getCamSet<T>(sid, key));
    }

    template<typename T>
    std::string getSetPoint(const std::string &key){
        return std::to_string(model_->getSetPoint<T>(key));
    }
    template<typename T>
    std::string getLowerBound(const std::string &key){
        return std::to_string(model_->getLowerBound<T>(key));
    }
    template<typename T>
    std::string getUpperBound(const std::string &key){
        return std::to_string(model_->getUpperBound<T>(key));
    }

    void configSID(YAML::Node &config, int id);
    void configMeta(YAML::Node &config, const std::string &key);

    template <typename T>
    void configSetPoint(YAML::Node &config, const std::string &key)
    {
        try{
            T value;
            if(config[key])
                value = config[key].as<T>();
            else
                value = defaultModel_->getSetPoint<T>(key);
            model_->setSetPoint<T>(key, value);
        }
        catch(const YAML::RepresentationException &e){
            throw std::runtime_error(std::string(e.what()) + std::string("\nRepresentation Exception: Failed to load key: \"") + key + "\"\n");
        }
        catch(const std::invalid_argument &e)
        {
            throw std::runtime_error(std::string(e.what()) + std::string(" and is missing from the default recipe.\n"));
        }
    }

    template <typename T>
    void configSetBounds(YAML::Node &config, const std::string &key)
    {
        try{
            T value1, value2;
            if(config[key])
            {
                if(config[key][0] && config[key][1])
                {
                    value1 = config[key][0].as<T>();
                    value2 = config[key][1].as<T>();
                }
                else
                {
                    value1 = defaultModel_->getLowerBound<T>(key);
                    value2 = defaultModel_->getUpperBound<T>(key);
                }
            }
            else
            {
                value1 = defaultModel_->getLowerBound<T>(key);
                value2 = defaultModel_->getUpperBound<T>(key);
            }
            model_->setBounds<T>(key, value1, value2);
        }
        catch(const YAML::RepresentationException &e){
            throw std::runtime_error(std::string("YAML Representation Error\nFailed to load key: \"") + key + "\"\n");
        }
        catch(const std::invalid_argument &e)
        {
            throw std::runtime_error(std::string(e.what()) + std::string(" and is missing from the default recipe, during configBounds\n"));
        }
    }

    void configSolenoids(YAML::Node &config, const std::string &id)
    {
        try{
            if(config["cam"][id]["solenoids"])
            {
                for(size_t i = 0; i < config["cam"][id]["solenoids"].size(); ++i){
                    int solID = config["cam"][id]["solenoids"][i][0].as<int>();
                    double width = config["cam"][id]["solenoids"][i][1].as<double>();
                    model_->setSolSet(id, i, solID, width);
                }
            }
            else{
                model_->setSolenoids(id, model_->getSolenoids("0"));
            }
        }
        catch(const YAML::RepresentationException &e){
            throw std::runtime_error(std::string("YAML Representation Error\nFailed to load solenoids for camera \"") + id + "\"\n");
        }
        catch(const std::invalid_argument &e){
            throw std::runtime_error(std::string(e.what()) + std::string(" and is missing from the default recipe, during configSolenoids.\n"));
        }
    }

    template <typename T>
    void configCamSet(YAML::Node &config, const std::string &id, const std::string &key)
    {
        try{
            T value;
            if(config["cam"][id][key])
                value = config["cam"][id][key].as<T>();
            else
                value = defaultModel_->getCamSet<T>("0", key);
            model_->setCamSet<T>(id, key, value);
        }
        catch(const YAML::RepresentationException &e){
            throw std::runtime_error(std::string("YAML Reprsentation Error\nFailed to load camera \"") + id +  "\" setting \"" + key + "\"\n");
        }
        catch(const std::invalid_argument &e){
            throw std::runtime_error(std::string(e.what()) + std::string(" and is missing from the default recipe, during configCam\n"));
        }
    }

protected:
    void configCamera(YAML::Node &config, const std::string &sid);

    QString recipe_;
    ModelPtr model_;
    ModelPtr defaultModel_;
    std::set<QString> recipes_;
};

#endif
