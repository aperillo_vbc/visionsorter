#include "RoundButton.h"
#include <QPainter>
#include <QRect>
#include <QPaintEvent>
#include <QStyleOption>
#include <cmath>
#include <iostream>

RoundButton::RoundButton(QWidget *parent) : QPushButton(parent)
{
    flat = false;
}

void RoundButton::mousePressEvent(QMouseEvent* event)
{
    setFlat(true);
    emit pressed();
}

void RoundButton::mouseReleaseEvent(QMouseEvent* event)
{
    setFlat(false);
    emit released();
}

void RoundButton::setFlat(bool makeFlat)
{
    flat = makeFlat;
    update();
}

void RoundButton::paintEvent(QPaintEvent *)
{
//    setText("");
    QStyleOption opt;
    opt.init(this);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QColor color1 = opt.palette.color(QPalette::Background);
    if(flat)
    {
        QColor color0 = opt.palette.color(QPalette::WindowText);
        painter.setPen(QPen(color0));
        QBrush brush0(color1, Qt::SolidPattern);
        painter.setBrush(brush0);
        painter.drawEllipse(QRectF(1, 1,
                            width()-2, height()-2));

        double diag = sqrt(width()*width() + height()*height());
        double gap = (diag - width())/2.;
        // cos(45) = 0.52532198881;
        double xGap = 0.52532198881 * gap;
        double yGap = 0.52532198881 * gap;

        QFont font = painter.font();
        font.setPixelSize(height()/4.);
        painter.setFont(font);

        painter.setPen(color0);
        painter.setBrush(Qt::NoBrush);
        QRect rect(xGap, yGap, width()-2.*xGap, height()-2.*yGap);
        painter.drawText(rect, Qt::AlignCenter, text());
    }
    else
    {
        QColor color0 = opt.palette.color(QPalette::WindowText);
        painter.setPen(QPen(color0));
        QBrush brush0(color0, Qt::SolidPattern);
        painter.setBrush(brush0);
        painter.drawEllipse(QRectF(1, 1,
                            width()-2, height()-2));


        double diag = sqrt(width()*width() + height()*height());
        double gap = (diag - width())/2.;
        // cos(45) = 0.52532198881;
        double xGap = 0.52532198881 * gap;
        double yGap = 0.52532198881 * gap;


        QFont font = painter.font();
        font.setPixelSize(height()/4.);
        painter.setFont(font);

        painter.setPen(color1);
        painter.setBrush(Qt::NoBrush);
        QRect rect(xGap, yGap, width()-2.*xGap, height()-2.*yGap);
        painter.drawText(rect, Qt::AlignCenter, text());
    }
}
