#ifndef ROUNDBUTTON_H
#define ROUNDBUTTON_H

#include <QPushButton>

namespace Ui {
class RoundButton;
}

class RoundButton : public QPushButton
{
    Q_OBJECT

public:
    RoundButton(QWidget *parent = nullptr);
    void setFlat(bool makeFlat);
protected:
    virtual void mousePressEvent(QMouseEvent* event);
    virtual void mouseReleaseEvent(QMouseEvent* event);
private:
    bool flat;
    void paintEvent(QPaintEvent *);
};

#endif // ROUNDBUTTON_H
