#include "RoundHardToggleButton.h"
#include <QPainter>
#include <QRect>
#include <QPaintEvent>
#include <QStyleOption>
#include <cmath>
#include <iostream>

RoundHardToggleButton::RoundHardToggleButton(QWidget *parent) : QPushButton(parent)
{
    flat = false;
}

void RoundHardToggleButton::mousePressEvent(QMouseEvent* event)
{
    if(!flat)
    {
        setFlat(true);
        emit pressed();
    }
}

void RoundHardToggleButton::setFlat(bool makeFlat)
{
    flat = makeFlat;
    update();
}

bool RoundHardToggleButton::isFlat() const
{
    return flat;
}

void RoundHardToggleButton::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QColor color1 = opt.palette.color(QPalette::Background);
    if(flat)
    {
        QColor color0 = opt.palette.color(QPalette::WindowText);
        QBrush brush(color0, Qt::BDiagPattern);
        painter.setBrush(brush);
        painter.setPen(QPen(color1));
        painter.drawEllipse(QRectF(1, 1,
                                   width()-2, height()-2));
    }
    else
    {
        QColor color0 = opt.palette.color(QPalette::WindowText);
        painter.setPen(QPen(color0));
        QBrush brush0(color0, Qt::SolidPattern);
        painter.setBrush(brush0);
        painter.drawEllipse(QRectF(1, 1,
                            width()-2, height()-2));


        double diag = sqrt(width()*width() + height()*height());
        double gap = (diag - width())/2.;
        // cos(45) = 0.52532198881;
        double xGap = 0.52532198881 * gap;
        double yGap = 0.52532198881 * gap;


        QFont font = painter.font();
        font.setPixelSize(height()/5.);
        painter.setFont(font);

        painter.setPen(color1);
        painter.setBrush(Qt::NoBrush);
        QRect rect(xGap, yGap, width()-2.*xGap, height()-2.*yGap);
        painter.drawText(rect, Qt::AlignCenter, text());
    }
}
