#ifndef ROUNDHARDTOGGLEBUTTON_H
#define ROUNDHARDTOGGLEBUTTON_H

#include <QPushButton>

namespace Ui {
class RoundHardToggleButton;
}

class RoundHardToggleButton : public QPushButton
{
    Q_OBJECT

public:
    RoundHardToggleButton(QWidget *parent = nullptr);
    void setFlat(bool makeFlat);
    bool isFlat() const;
protected:
    virtual void mousePressEvent(QMouseEvent* event);
private:
    bool flat;
    void paintEvent(QPaintEvent *);
};

#endif // ROUNDHARDTOGGLEBUTTON_H
