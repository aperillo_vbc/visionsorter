#include "SquareHollowButton.h"
#include <QPainter>
#include <QRect>
#include <QPaintEvent>
#include <QStyleOption>

SquareHollowButton::SquareHollowButton(QWidget *parent) : QPushButton(parent)
{
}

void SquareHollowButton::mousePressEvent(QMouseEvent* event)
{
    if(isFlat())
        setFlat(false);
    else
        setFlat(true);
    emit pressed();
}

void SquareHollowButton::setFlat(bool makeFlat)
{
    QPushButton::setFlat(makeFlat);
    update();
}

void SquareHollowButton::paintEvent(QPaintEvent *)
{
    setText("");
    const double fillFract = 0.70;
    const double margin = (1 -  fillFract)/2.;
    QStyleOption opt;
    opt.init(this);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    if(isFlat())
    {
        QColor color = opt.palette.color(QPalette::WindowText);
        QBrush brush(color, Qt::SolidPattern);
        painter.setBrush(brush);
        painter.setPen(QPen(color));

        if(width() > height())
        {
            double w = width() - (height() * margin * 2.);
            painter.drawRect(QRectF(height() * margin, height() * margin,
                                    w, fillFract * height()));
        }
        else
        {
            double h = height() - (width() * margin * 2.);
            painter.drawRect(QRectF(width() * margin, width() * margin,
                                    fillFract * width(), h));
        }
    }
}
