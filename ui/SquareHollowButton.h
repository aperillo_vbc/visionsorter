#ifndef SQUAREHOLLOWBUTTON_H
#define SQUAREHOLLOWBUTTON_H

#include <QPushButton>

namespace Ui {
class SquareHollowButton;
}

class SquareHollowButton : public QPushButton
{
    Q_OBJECT

public:
    SquareHollowButton(QWidget *parent = nullptr);
    void setFlat(bool makeFlat);
protected:
    virtual void mousePressEvent(QMouseEvent* event);
private:
    void paintEvent(QPaintEvent *);
};

#endif // SQUAREHOLLOWBUTTON_H
