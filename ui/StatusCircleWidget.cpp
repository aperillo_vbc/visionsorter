#include "StatusCircleWidget.h"
#include <QPainter>
#include <QRect>
#include <QPaintEvent>
#include <QStyleOption>

StatusCircleWidget::StatusCircleWidget(QWidget *parent) : QWidget(parent), on(false)
{
}

void StatusCircleWidget::setStatus(bool state)
{
    on = state;
    update();
}

void StatusCircleWidget::paintEvent(QPaintEvent *)
{
    setText("");
    const double innerCircleFraction = 0.6;
    const double fillFract = 0.85;
    QStyleOption opt;
    opt.init(this);
//    QRect rect = event->rect();
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::green);
//    painter.drawText(rect, Qt::AlignCenter, "");
//    painter.drawRect(rect);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
    painter.translate(width() / 2, height() / 2);
    QColor color = opt.palette.color(QPalette::WindowText);
    double diameter = height() * fillFract;

//    int alpha = 255;
    painter.setPen(QPen(color, 5));
    painter.drawEllipse(QRectF(-diameter / 2., -diameter / 2., diameter, diameter));
    if(on)
    {
        color = opt.palette.color(QPalette::AlternateBase);
        QBrush brush(color, Qt::SolidPattern);
        painter.setBrush(brush);
        painter.setPen(QPen(color));
        painter.drawEllipse(QRectF(-diameter / 2. * innerCircleFraction, -diameter / 2. * innerCircleFraction,
                                   diameter * innerCircleFraction, diameter * innerCircleFraction));
    }
}
