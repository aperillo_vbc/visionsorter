#ifndef StatusCircleWidget_H
#define StatusCircleWidget_H

#include <QWidget>
#include <QString>

class StatusCircleWidget : public QWidget
{
    Q_OBJECT
public:
    explicit StatusCircleWidget(QWidget *parent = nullptr);
    inline void setText(const QString &string){}
private:
    bool on;
    void paintEvent(QPaintEvent *);
signals:

public slots:
    void setStatus(bool state);
};

#endif // StatusCircleWidget_H
