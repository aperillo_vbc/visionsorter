#-------------------------------------------------
#
# Project created by QtCreator 2017-07-14T22:33:08
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VisionSorter
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        OperatorWindow.cpp \
    StatusCircleWidget.cpp \
    SquareHollowButton.cpp \
    RoundHardToggleButton.cpp \
    RoundButton.cpp \
    KeyPad.cpp \
    Recipes.cpp \
    unixsignalhandler.cpp

HEADERS += \
        OperatorWindow.h \
    StatusCircleWidget.h \
    SquareHollowButton.h \
    RoundHardToggleButton.h \
    RoundButton.h \
    KeyPad.h \
    timers.h \
    Recipes.h \
    CameraStream.h \
    AstraCamStream.h \
    unixsignalhandler.h

FORMS += \
        OperatorWindow.ui \
    KeyPad.ui
        OperatorWindow.ui

CONFIG += \
c++11 \
link_pkgconfig

PKGCONFIG += opencv python3

QMAKE_CXXFLAGS += -fdiagnostics-color

INCLUDEPATH += \
/usr/local/include \
controller \
controller/daq/include \
controller/camera/include \
controller/classifier/include \
controller/include \
controller/model \
OpenNI2 \ 
/opt/pylon5/include


#PRE_TARGETDEPS += \
#controller/lib/libcontrol.a

LIBS += \
controller/lib/libcontrol.a \
-ldl -lmccusb -lhidapi-libusb -lusb-1.0 -pthread \
-lyaml-cpp \
-L/opt/pylon5/lib64 \
-lpylonbase -lpylonutility -lGenApi_gcc_v3_1_Basler_pylon -lGCBase_gcc_v3_1_Basler_pylon \
-Wl,-rpath,.:/opt/pylon5/lib64 libOpenNI2.so

DESTDIR = .
OBJECTS_DIR = ui
MOC_DIR = ui
RCC_DIR = ui
UI_DIR = ui
