#include "VisionSystem.h"
#include "OperatorWindow.h"
#include "KeyPad.h"
#include "Controller.h"
#include "Model.h"
#include "Recipes.h"
#include <QThread>
#include "timers.h"
#include <QApplication>
#include <QFile>
#include <QMessageBox>
#include <opencv2/opencv.hpp>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <gperftools/profiler.h>
#include "unixsignalhandler.h"

void setModel(ModelPtr model);
// Namespace for using pylon objects.
using namespace Pylon;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QObject::connect(new UnixSignalHandler(SIGTERM, &a), &UnixSignalHandler::raised, qApp, &QCoreApplication::quit);
    QObject::connect(new UnixSignalHandler(SIGINT, &a), &UnixSignalHandler::raised, qApp, &QCoreApplication::quit);

    Pylon::PylonAutoInitTerm autoInitTerm;  // PylonInitialize() will be called now

// ProfilerStart("vs.prof");

    ModelPtr model = ModelPtr(new Model);
    RecipesPtr recipes;
    std::exception_ptr eptr;
    try{
    recipes = RecipesPtr(new Recipes(model));
    model->setCurrentValue<int>("class0Count", 0);
    model->setCurrentValue<int>("class1Count", 0);

    DAQPtr daq = DAQPtr(new DAQ);
    // NozzleMasterPtr noz = NozzleMasterPtr(new NozzleMaster(daq, 24));

    std::vector<baslerCam::CameraPtr> cameras;
    cameras = recipes->getCameras(daq);
    if(cameras.size()==1)
        std::cout << "Only one camera was found" << std::endl;

    ControllerPtr controller = ControllerPtr(new Controller(model, daq,
                                                            cameras));
    controller->setDebugging(false);
    // controller->calibrateBeltSpeed(std::cout, "conveyorSpeedEst1");
    VisionSystemPtr vision = VisionSystemPtr(new VisionSystem(controller));

    OperatorWindow w(0, vision, controller, recipes);

    QMessageBox errorBox;

    QThread *timeThread = new QThread;
    Timers *timer = new Timers();
    timer->moveToThread(timeThread);

    QObject::connect(timer, SIGNAL(updateStatus500()), &w, SLOT(updateAllStatus()));
    QObject::connect(timeThread, SIGNAL(started()), timer, SLOT(startTimers()));
    QObject::connect(timeThread, SIGNAL(finished()), timer, SLOT(stopTimers()));
    // Load the style sheets
    QString styleSheetPathName("style_sheet.css");
    QFile style_sheet(styleSheetPathName);
    if(!style_sheet.exists() || !style_sheet.open(QFile::ReadOnly))
    {
        errorBox.critical(0, "Error", "The style sheet, <b>" + styleSheetPathName + "</b>, could not be loaded. " + style_sheet.errorString() );
        return 0;
    }
    a.setStyleSheet(QString(style_sheet.readAll()));
    style_sheet.close();

    KeyPad k(0, "1234", "1234");
    int state = k.exec();
//    errorBox.information(0,"passcode return", QString::number(state));
    timeThread->start();


//    QStackedWidget stackedWidgets;
//    stackedWidgets.addWidget(&engi);
//    stackedWidgets.addWidget(&w);
//    QVBoxLayout layout;
//    layout.addWidget(&stackedWidgets);
//    setLayout(&layout);
    w.show();
//    KeyPad pad;
//    pad.show();
    // Go full-screen
    w.setWindowState(Qt::WindowFullScreen);

    return a.exec();
    }
    catch(const std::exception &e)
    {
        std::cerr << "Previous copy running\n" << std::endl;
	a.quit();
    }
}
