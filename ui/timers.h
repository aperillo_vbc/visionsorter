#ifndef TIMERS_H
#define TIMERS_H

#include <QObject>
#include <QTimer>
#include "OperatorWindow.h"

class Timers : public QObject {
    Q_OBJECT

public:
    explicit Timers(QObject *parent = 0) : QObject(parent){}
    ~Timers() {}

public slots:
    void startTimers()
    {
//        timer50 = new QTimer(this);
        /* timer100 = new QTimer(this); */
        timer500 = new QTimer(this);
//        timer50->setInterval(50);
        /* timer100->setInterval(100); */
        timer500->setInterval(500);

        connectTimers();

//        timer50->start();
        /* timer100->start(); */
        timer500->start();
    }
    void stopTimers()
    {
//        killTimer(timer50->timerId());
        /* killTimer(timer100->timerId()); */
        killTimer(timer500->timerId());
    }

signals:
   void updateStatus100();
   void updateStatus500();

private slots:
   void updateStatus100Local() {
       emit updateStatus100();
    }
   void updateStatus500Local() {
       emit updateStatus500();
   }

private:

    void connectTimers()
    {
        /* connect(timer100, SIGNAL(timeout()), this, SLOT(updateStatus100Local())); */
        connect(timer500, SIGNAL(timeout()), this, SLOT(updateStatus500Local()));
    }
    QTimer *timer500;
    QTimer *timer100;
//    QTimer *timer50;
};

#endif // TIMERS_H
